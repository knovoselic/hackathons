mod = 10007

def factorial(n):
    global mod

    result = 1
    i = 1
    while i <= n:
        result = (result * i) % mod
        i += 1

    return result

def gcdExtended(a, b):
    if a == 0: return b, 0, 1
    g, x1, y1 = gcdExtended(b % a, a)
    x = y1 - b // a * x1
    y = x1

    return g, x, y

def modInv(b, m):
    g, x, y = gcdExtended(b, m)

    if g != 1: return -1
    return (x % m + m) % m

def moddiv(a, b):
    global mod

    a = a % mod
    inv = modInv(b, mod)
    if inv == -1:
        raise 'AAAAAA'

    return (inv * a + mod) % mod

def modpower(a, n):
    global mod

    if n == 0: return 1

    result = a
    i = 1
    while i < n:
        result = (result * a) % mod
        i += 1

    return result

n = int(input())

c = moddiv(modpower(factorial(n), 2), modpower(6, n))

l = 0
for a in range(n + 1):
    for b in range(n + 1 - a):
        d = (modpower(-1, b) * modpower(2, a) * modpower(3, b)) % mod
        e = factorial(3 * n - 3 * a - 2 * b)
        f = (d * e + mod) % mod

        g = (factorial(a) * factorial(b)) 
        h = modpower(factorial(n - a - b), 2)
        i = modpower(6, n - a - b)
        j = (g * h * i + mod) % mod

        k = moddiv(f, j)
        l = (l + k + mod) % mod

print((c * l) % mod)
