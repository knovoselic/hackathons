import math
import itertools

all_permutations = []
def heap(a, s):
    if s == 1:
        all_permutations.append(''.join(map(str, a)))
        return

    for i in range(s):
        heap(a, s - 1)
        if s % 2 == 1:
            t = a[0]
            a[0] = a[s - 1]
            a[s - 1] = t
        else:
            t = a[i]
            a[i] = a[s - 1]
            a[s - 1] = t

n = int(input())
if n < 3:
    print(0)
    exit(0)

start_row = [1, 1, 1] + [0] * (n - 3)

heap(start_row, n)

valid_rows = list(set(all_permutations))
# for i in range(len(valid_rows)):
#     valid_rows[i] += "\n"

print(valid_rows)
print(len(valid_rows))
print(math.factorial(n) / 6 / math.factorial(n - 3))

# all_permutations = []
# heap(valid_rows, len(valid_rows))
# valid_rows = valid_rows * 3
x = list(itertools.permutations(valid_rows, n))

print(len(x))

mix = list(set(x))
print(len(mix))
valid_count = 0
valids = set()

for p in mix:
    valid = True
    for i in range(n):
        count = 0
        for j in range(n):
            if p[j][i] == '1': count += 1

        if count != 3:
            valid = False
            break

    if not valid: continue
    valids.add(p)
    valid_count += 1
    # print(p)

print(valid_count)
print(len(valids))
