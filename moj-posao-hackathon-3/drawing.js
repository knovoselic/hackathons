
var n = Number(readline());

var m = new Array(2 * n - 1);
for(var i = 0; i < 2 * n - 1; i++) {
	m[i] = Array(2 * n - 1);
}

var cx, cy;

cx = cy = n - 1;

var step = 0;
while(cx >= 0 && cy >= 0) {
	var size = step * 2 + 1;
	for(var i = 0; i < size; i++) {
		m[cx][cy + i] = step + 1;
		m[cx + size - 1][cy + i] = step + 1;
		m[cx + i][cy] = step + 1;
		m[cx + i][cy + size - 1] = step + 1;
	}

	++step;
	--cx;
	--cy;
}

for(var x = 0; x < 2 * n - 1; ++x) {
	for(var y = 0; y < 2 * n - 1; ++y) {
		putstr(m[x][y]);
	}
	print();
}

