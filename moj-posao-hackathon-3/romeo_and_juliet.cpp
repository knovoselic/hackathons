#include <cstdio>
#include <cstring>

// 2 pixel padding all directions +
// 2 chars extra for end line + \0
char field[52][54] = {0};
char backup[52][54] = {0};
int n, m;
int rx, ry;

bool isReachable(int x, int y) {
	if (x < 1 || y < 1 || x > n || y > m ) {
		return false;
	}
	if (field[x][y] == 'J') {
		return true;
	} else if (field[x][y] != '#') {
		return false;
	}

	field[x][y] = '-';

	return isReachable(x, y + 1) ||
		isReachable(x, y - 1) ||
		isReachable(x + 1, y) ||
		isReachable(x - 1, y);
}

void dump() {
#if 0
	for(int i = 1; i <= n; ++i) {
		printf("%s", field[i]);
	}
	printf("\n");
#else
	for(int i = 1; i <= n; ++i) {
		printf("%s", backup[i] + 1);
	}
#endif
	printf("\n");
}

bool runSim() {
	bool changed = false;

	for(int x = 1; x <= n; ++x) {
		for(int y = 1; y <= m; y++) {
			if (field[x][y] != '#') continue;
			if (backup[x - 1][y] == '.' ||
					backup[x + 1][y] == '.' ||
					backup[x][y - 1] == '.' ||
					backup[x][y + 1] == '.') {
				field[x][y] = '.';
				changed = true;
			}
		}
	}
	return changed;
}

int main(void) {
	int years = 0;

	scanf("%d %d", &n, &m);

	// chomp trailing endline
	fgets(field[1] + 1, 50, stdin);

	for(int i = 1; i <= n; ++i) {
		fgets(field[i] + 1, 50, stdin);
	}

	for(int x = 1; x <= n; ++x) {
		for(int y = 1; y <= m; y++) {
			if (field[x][y] == 'R') {
				rx = x;
				ry = y;
				x = n;
				break;
			}
		}
	}

	memcpy(backup, field, sizeof(field));
	field[rx][ry] = '#';
	// dump();
	while(isReachable(rx, ry)) {
		// dump();
		memcpy(field, backup, sizeof(field));
		if (!runSim()) {
			years = -1;
			break;
		}
		memcpy(backup, field, sizeof(field));
		field[rx][ry] = '#';
		++years;

	}
	// dump();

	printf("%d\n", years);
}
