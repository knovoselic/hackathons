<?php

fscanf(STDIN, "%d", $n);

$m = [];


$cx = $n - 1;
$cy = $n - 1;

$step = 0;
while($cx >= 0 && $cy >= 0) {
	$size = $step * 2 + 1;
	for($i = 0; $i < $size; $i++) {
		$m[$cx][$cy + $i] = $step + 1;
		$m[$cx + $size - 1][$cy + $i] = $step + 1;
		$m[$cx + $i][$cy] = $step + 1;
		$m[$cx + $i][$cy + $size - 1] = $step + 1;
	}

	++$step;
	--$cx;
	--$cy;
}

for($x = 0; $x < 2 * $n - 1; ++$x) {
	for($y = 0; $y < 2 * $n - 1; ++$y) {
		echo($m[$x][$y]);
	}
	echo("\n");
}

?>
