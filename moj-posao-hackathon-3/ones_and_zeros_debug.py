mod = 10007
debug = False

def factorial(n):
    global mod

    result = 1
    i = 1
    while i <= n:
        result = (result * i) % mod
        i += 1

    return result

def gcdExtended(a, b):
    if a == 0: return b, 0, 1
    g, x1, y1 = gcdExtended(b % a, a)
    x = y1 - b // a * x1
    y = x1

    return g, x, y

def modInv(b, m):
    g, x, y = gcdExtended(b, m)

    if g != 1: return -1
    return (x % m + m) % m

def moddiv(a, b):
    global mod

    a = a % mod
    inv = modInv(b, mod)
    if inv == -1:
        raise 'AAAAAA'

    return (inv * a + mod) % mod

def modpower(a, n):
    global mod

    if n == 0: return 1

    result = a
    i = 1
    while i < n:
        result = (result * a) % mod
        i += 1

    return result

n = int(input())

c = moddiv(modpower(factorial(n), 2), modpower(6, n))

print("n =", n)
print("c =", c)

l = 0
for a in range(n + 1):
    for b in range(n + 1 - a):
        d = (modpower(-1, b) * modpower(2, a) * modpower(3, b)) % mod
        e = factorial(3 * n - 3 * a - 2 * b)
        f = (d * e + mod) % mod
        if debug: print("a =", a)
        if debug: print("b =", b)
        if debug: print("d =", d)
        if debug: print("dd =", dd)
        if debug: print("e =", e)
        if debug: print("f =", f)

        g = (factorial(a) * factorial(b)) 
        h = modpower(factorial(n - a - b), 2)
        i = modpower(6, n - a - b)
        j = (g * h * i + mod) % mod
        if debug: print("g =", g)
        if debug: print("h =", h)
        if debug: print("i =", i)
        if debug: print("j =", j)

        k = moddiv(f, j)
        l = (l + k + mod) % mod
        if debug: print("k =", k)
        if debug: print("l =", l)
        if debug: print()

print((c * l) % mod)
