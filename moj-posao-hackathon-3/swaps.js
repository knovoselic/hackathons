var n = Number(readline());
var line = readline().trim();


var a = [];
var b = [];
var c = [];

var count = 0;

while (count < n) {
	if (count % 3 == 0) {
		a.push(line[count]);
	} else if (count % 3 == 1) {
		b.push(line[count]);
	} else {
		c.push(line[count]);
	}
	++count;
}

a.sort();
b.sort();
c.sort();

var current_index = 0;

while(a.length > current_index || b.length > current_index || c.length > current_index) {
	if(a.length > current_index) putstr(a[current_index]);
	if(b.length > current_index) putstr(b[current_index]);
	if(c.length > current_index) putstr(c[current_index]);
	++current_index;
}

print();
