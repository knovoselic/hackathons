debug = False
n = int(input())
index = 0
f = list(map(int, input().strip().split()))
f = [(f[i], i) for i in range(len(f))]

f.sort()

if debug: print(n)
if debug: print(f)
solution = [0] * n

if debug: print(solution)
for x in range(n - 1):
    a = min(f[x][1], f[x + 1][1])
    b = max(f[x][1], f[x + 1][1])
    if debug: print(a, '-', b)
    for y in range(a, b + 1):
        solution[y] += 1
print(" ".join(map(str, solution)))

# This is little faster (10s for 100k), but wrong :(
# slices = []
# for x in range(n - 1):
#      a = min(f[x][1], f[x + 1][1])
#      b = max(f[x][1], f[x + 1][1])
#      slices.append((a, b))
# 
# 
# if debug: print(slices)
# 
# slices.sort()
# 
# if debug: print(slices)
# 
# search_start = 0
# new_search_start = 0
# i = 0
# while i < n:
#     search_start = new_search_start
#     x = search_start
#     count = 0
#     while x < n - 1:
#         if slices[x][1] == i: new_search_start = x + 1
#         if slices[x][0] > i: break
#         elif slices[x][1] >= i: count += 1
#         x += 1
#     print(count, end = ' ')
#     i += 1
# print()
