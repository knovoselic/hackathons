n = int(input())
line = input().strip()

a = [];
b = [];
c = [];

count = 0;

while (count < n):
    if (count % 3 == 0):
        a.append(line[count])
    elif count % 3 == 1:
        b.append(line[count])
    else:
        c.append(line[count])
    
    count += 1

a.sort()
b.sort()
c.sort()

current_index = 0;

while(len(a) > current_index or len(b) > current_index or len(c) > current_index):
    if(len(a) > current_index):  print(a[current_index], end = '');
    if(len(b) > current_index): print(b[current_index], end = '');
    if(len(c) > current_index): print(c[current_index], end = '');
    current_index += 1

print();
