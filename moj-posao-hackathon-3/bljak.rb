require 'set'

N = 4
res = Set.new

i = 0
goal = 2**(N*N)
# goal = 2000000
while res.size < goal do
	x = Array.new(N*N) { rand 2 }
	res << x unless res.include? x
	i += 1
	puts "#{res.size}, #{goal - res.size} left" if i % 10000 == 0
end

res.map! { |x| x.each_slice(N).to_a }

correct = res.select do |x|
	valid = true
	N.times do |i|
		count = 0
		N.times { |j| count += 1 if x[i][j] == 1 }
		valid = false && break unless count == 3
		count = 0
		N.times { |j| count += 1 if x[j][i] == 1 }
		valid = false && break unless count == 3
	end
	valid
end
puts correct.count

correct.each do |c|
	puts c.map { |x| x.join ' ' }
	puts '=' * 80
end
