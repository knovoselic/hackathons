import java.io.*;
import java.util.*;

public class Main {
	public static void main(String[] args) throws IOException {
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		StringTokenizer st = new StringTokenizer(br.readLine());

		int n = Integer.parseInt(st.nextToken());

		ArrayList<Character> a = new ArrayList<>();
		ArrayList<Character> b = new ArrayList<>();
		ArrayList<Character> c = new ArrayList<>();


		long i = 0;
		int chr;
		while((chr = br.read()) != -1) {
			if (Character.isWhitespace((char)chr)) continue;
			if (i % 3 == 0) {
				a.add((char)chr);
			} else if (i % 3 == 1) {
				b.add((char)chr);
			} else {
				c.add((char)chr);
			}
			++i;
		}

		Collections.sort(a);
		Collections.sort(b);
		Collections.sort(c);

		StringBuilder sb = new StringBuilder();
		int current_index = 0;
		while(a.size() > current_index || b.size() > current_index || c.size() > current_index) {
			if (a.size() > current_index) sb.append(a.get(current_index));
			if (b.size() > current_index) sb.append(b.get(current_index));
			if (c.size() > current_index) sb.append(c.get(current_index));

			++current_index;
		}

		System.out.println(sb.toString());
	}
}
