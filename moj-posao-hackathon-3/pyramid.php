<?php

fscanf(STDIN, "%d", $n);
fscanf(STDIN, "%d", $level);
fscanf(STDIN, "%d", $index);


$current_level = $level;
$removed = 0;
$next_blocks = array();
array_push($next_blocks, $index);

while($current_level <= $n) {
	$current_blocks = $next_blocks;
	$next_blocks = array();
	$next_level_size = $n - $current_level;

	while(sizeof($current_blocks) > 0) {
		$current = array_pop($current_blocks);
		$left = $current - 1;
		$right = $current;

		if ($left > 0) {
			array_push($next_blocks, $left);
		}
		if ($right <= $next_level_size) {
			array_push($next_blocks, $right);
		}
		$removed++;
	}
	$next_blocks = array_unique($next_blocks);

	++$current_level;
}

fprintf(STDOUT, "%d\n", $removed - 1);
?>
