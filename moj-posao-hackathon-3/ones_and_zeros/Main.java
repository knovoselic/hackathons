import java.io.*;
import java.util.*;

public class Main {
	public static final long MOD = 10007;

	public static long factorial(long n) {
		long result = 1;
		long i = 1;
	
		while (i <= n) {
			result = (result * i) % MOD;
			++i;
		}
	
		return result;
	}

	public static long gcdExtended(long a, long b, long[] x, long[] y) {
		if (a == 0) {
			x[0] = 0;
			y[0] = 1;
			return b;
		}
		long x1[] = new long[1];
		x1[0] = 1;
		long y1[] = new long[1];
		y1[0] = 1;
		long gcd = gcdExtended(b % a, a, x1, y1);
		x[0] = y1[0] - (b / a) * x1[0];
		y[0] = x1[0];
	
		return gcd;
	}
	
	public static long modInv(long b, long m) {
		long x[] = new long[1];
		x[0] = 1;
		long y[] = new long[1];
		y[0] = 1;
		long g = gcdExtended(b, m, x, y);
		if (g != 1) return -1;
		return (x[0] % m + m) % m;
	}
	
	public static long modDiv(long a, long b) {
		a = a % MOD;
		long inv = modInv(b, MOD);
		if (inv == -1) {
			System.out.println("AAAAAAAAAAAAAAAAAAAAAAAAAA");
			return 255;
		}
	
		return (inv * a + MOD) % MOD;
	}
	
	public static long modPower(long a, long n) {
		if (n == 0) return 1;
	
		long result = a;
		long i = 1;
		while (i < n) {
			result = (result * a) % MOD;
			++i;
		}
	
		return result;
	}
	
	public static void main(String[] args) {
		int n;
		Scanner in = new Scanner(System.in);
	
		n = in.nextInt();
		
		long c = modDiv(modPower(factorial(n), 2), modPower(6, n));
		long l = 0;
	
		for(long a = 0; a <= n; ++a) {
			for(long b = 0; b <= n -a; ++b) {
				long d = (modPower(-1, b) * modPower(2, a) * modPower(3, b)) % MOD;
				long e = factorial(3 * n - 3 * a - 2 * b);
				long f = (d * e + MOD) % MOD;
	
				long g = (factorial(a) * factorial(b));
				long h = modPower(factorial(n - a - b), 2);
				long i = modPower(6, n - a - b);
				long j = (g * h * i + MOD) % MOD;
	
				long k = modDiv(f, j);
				l = (l + k + MOD) % MOD;
			}
		}
	
		System.out.println((c * l) % MOD);
	
	}
}
