<?php

$field = [];
$backup = [];

function isReachable($x, $y) {
	global $field, $backup, $n, $m, $rx, $ry;

	if ($x < 1 || $y < 1 || $x > $n || $y > $m ) {
		return false;
	}
	if ($field[$x][$y] == 'J') {
		return true;
	} else if ($field[$x][$y] != '#') {
		return false;
	}

	$field[$x][$y] = '-';

	return isReachable($x, $y + 1) ||
		isReachable($x, $y - 1) ||
		isReachable($x + 1, $y) ||
		isReachable($x - 1, $y);
}

/*
void dump() {
#if 0
	for(int i = 1; i <= n; ++i) {
		printf("%s", field[i]);
	}
	printf("\n");
#else
	for(int i = 1; i <= n; ++i) {
		printf("%s", backup[i] + 1);
	}
#endif
	printf("\n");
}
 */

function runSim() {
	global $field, $backup, $n, $m, $rx, $ry;
	$changed = false;

	for($x = 1; $x <= $n; ++$x) {
		for($y = 1; $y <= $m; $y++) {
			if ($field[$x][$y] != '#') continue;
			if ($backup[$x - 1][$y] == '.' ||
					$backup[$x + 1][$y] == '.' ||
					$backup[$x][$y - 1] == '.' ||
					$backup[$x][$y + 1] == '.') {
				$field[$x][$y] = '.';
				$changed = true;
			}
		}
	}
	return $changed;
}

$years = 0;

fscanf(STDIN, "%d %d", $n, $m);

// fgets(STDIN, 50);

$field[0] = "";
$field[$n + 1] = "";

for($i = 0; $i < $n + 2; ++$i) {
        $field[$i][0] = '+';
        $field[$i][$m + 1] = '+';
}
for($i = 0; $i < $m + 2; ++$i) {
        $field[0][$i] = '+';
        $field[$n + 1][$i] = '+';
}

for($i = 1; $i <= $n; ++$i) {
	$field[$i] = '+'.fgets(STDIN);
}

for($x = 1; $x <= $n; ++$x) {
	for($y = 1; $y <= $m; $y++) {
		if ($field[$x][$y] == 'R') {
			$rx = $x;
			$ry = $y;
			$x = $n;
			break;
		}
	}
}

$backup = $field;
$field[$rx][$ry] = '#';
// dump();
while(isReachable($rx, $ry)) {
	// dump();
	$field = $backup;
	if (!runSim()) {
		$years = -1;
		break;
	}
	$backup = $field;
	$field[$rx][$ry] = '#';
	++$years;

}
// dump();

fprintf(STDOUT, "%d\n", $years);
