[n, m] = readline().trim().split(" ").map(Number);

print((n - m + 1) * 3);
