#include <cstdio>

int main(void) {
	int n;

	scanf("%d", &n);

	int m[2*n-1][2*n-1] = {0};

	int cx, cy;

	cx = cy = n - 1;

	int step = 0;
	while(cx >= 0 && cy >= 0) {
		int size = step * 2 + 1;
		for(int i = 0; i < size; i++) {
			m[cx][cy + i] = step + 1;
			m[cx + size - 1][cy + i] = step + 1;
			m[cx + i][cy] = step + 1;
			m[cx + i][cy + size - 1] = step + 1;
		}

		++step;
		--cx;
		--cy;
	}

	for(int x = 0; x < 2 * n - 1; ++x) {
		for(int y = 0; y < 2 * n - 1; ++y) {
			printf("%d", m[x][y]);
		}
		printf("\n");
	}


	return 0;
}
