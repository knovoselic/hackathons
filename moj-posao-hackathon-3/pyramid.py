n = int(input())
level = int(input())
index = int(input())

current_level = level
removed = 0
next_blocks = set()
next_blocks.add(index)

while current_level <= n:
    current_blocks = set(next_blocks)
    next_blocks.clear()

    next_level_size = n - current_level

    while len(current_blocks) > 0:
        current = current_blocks.pop()
        left = current - 1
        right = current

        if left > 0: next_blocks.add(left)
        if right <= next_level_size: next_blocks.add(right)

        removed += 1

    current_level += 1

print(removed - 1)
