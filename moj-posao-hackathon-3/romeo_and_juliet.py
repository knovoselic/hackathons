import copy

def isReachable(x, y):
    if (x < 1 or y < 1 or x > n or y > m ): return False

    if (field[x][y] == 'J'): return True
    elif (field[x][y] != '#'): return False
    
    field[x][y] = '-'
    
    return isReachable(x, y + 1) or isReachable(x, y - 1) or isReachable(x + 1, y) or isReachable(x - 1, y)

# function dump() {
# 	for(var i = 0; i < n + 2; i++) {
# 		for(var j = 0; j < m + 2; j++) {
# 			if (field[i][j] != 0)
# 				putstr(field[i][j]);
# 			else
# 				putstr(0);
# 		}
# 		print();
# 	}
# }

def runSim():
    changed = False;
   
    for x in range(1, n +1):
        for y in range(1, m + 1):
            if (field[x][y] != '#'): continue;
            if (backup[x - 1][y] == '.' or backup[x + 1][y] == '.' or backup[x][y - 1] == '.' or backup[x][y + 1] == '.'):
                field[x][y] = '.';
                changed = True;
    return changed;

years = 0;
field = [[0 for x in range(52)] for y in range(52)]
backup = [[0 for x in range(52)] for y in range(52)]

n, m = [int(x) for x in input().strip().split(" ")]

for i in range(n + 2):
    field[i][0] = '+';
    field[i][m + 1] = '+';

for i in range(m + 2):
    field[0][i] = '+';
    field[n + 1][i] = '+';

for i in range(1, n + 1):
    line = input();
    for j in range(len(line)):
        field[i][j + 1] = line[j]


for x in range(1, n + 1):
    for y in range(1, m + 1):
        if (field[x][y] == 'R'):
            rx = x
            ry = y
            x = n
            break

backup = copy.deepcopy(field)
field[rx][ry] = '#';

while(isReachable(rx, ry)):
    field = copy.deepcopy(backup)
    if (not runSim()):
    	years = -1;
    	break;
    backup = copy.deepcopy(field)
    field[rx][ry] = '#';
    years += 1


print(years);
