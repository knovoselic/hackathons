<?php
fscanf(STDIN, "%d", $n);

$a = array();
$b = array();
$c = array();

$count = 0;

while ($count < $n) {
	if ($count % 3 == 0) {
		array_push($a, fgetc(STDIN));
	} else if ($count % 3 == 1) {
		array_push($b, fgetc(STDIN));
	} else {
		array_push($c, fgetc(STDIN));
	}
	++$count;
}

sort($a);
sort($b);
sort($c);

$current_index = 0;

while(isset($a[$current_index]) || isset($b[$current_index]) || isset($c[$current_index])) {
	if(isset($a[$current_index])) echo($a[$current_index]);
	if(isset($b[$current_index])) echo($b[$current_index]);
	if(isset($c[$current_index])) echo($c[$current_index]);
	++$current_index;
}

echo "\n";

?>
