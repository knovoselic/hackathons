import math

mod = 10007

def factorial(n):
    # return math.factorial(n)
    mod = 10007
    result = 1
    i = 1
    while i <= n:
        result = (result * i) % mod
        i += 1

    return result

n = int(input())

nf = math.factorial(n)

result = nf * nf 
print("fn*fn:", nf**2)
print("Result:", result)

big_sum = 0
for a in range(n + 1):
    for b in range(n + 1 - a):
        big_sum += ((-1) ** b) * (2 ** a) * (3 ** b) * math.factorial(3 * n - 3 * a - 2 * b) / ((math.factorial(a) * math.factorial(b) * math.factorial(n - a - b) ** 2 * 6 ** (n - a - b)))

result *= big_sum
print("Before div:", result)
print("Before div: mod", result % mod)
print("Div with:", 6**n)
print("Div with mod:", 6**n % mod)
result /= 6**n
print(n)
# print (round(result))
print (round(result) % 10007)


def gcdExtended(a, b):
    if a == 0: return b, 0, 1
    g, x1, y1 = gcdExtended(b % a, a)
    x = y1 - int(b / a) * x1
    y = x1

    return g, x, y

def modInv(b, m):
    g, x, y = gcdExtended(b, m)

    if g != 1: return -1
    return (x % m + m) % m

def moddiv(a, b, m):
    a = a % m
    inv = modInv(b, m)
    if inv == -1:
        raise 'AAAAAA'

    return (inv * a + m) % m

def modpower(a, n):
    mod = 10007
    result = a
    if n == 0: return 1
    i = 1
    while i < n:
        result = (result * a) % mod
        i += 1

    return result


fn = factorial(n) ** 2
result = fn 
print("fn*fn:", fn)
print("Result:", result)

big_sum = 0
big_sum_mod = 0
for a in range(n + 1):
    for b in range(n + 1 - a):
        fact_nnnaaabb = math.factorial(3 * n - 3 * a - 2 * b)
        fa = math.factorial(a)
        fb = math.factorial(b)
        fnab = math.factorial(n - a -b)
        velki = fact_nnnaaabb / (fa * fb * fnab**2 * 6**(n - a - b))

        big_sum += (((-1)**b) *
                   (2**a) *
                   (3**b) *
                   fact_nnnaaabb /
                   (fa * fb * fnab**2 * 6**(n - a - b)))

        fact_nnnaaabb = factorial(3 * n - 3 * a - 2 * b)
        fa = factorial(a)
        fb = factorial(b)
        fnab = factorial(n - a -b)
        xa = modpower(fnab, 2)
        xb = modpower(6, n - a - b)
        velki = ((((-1)**b) * modpower(2, a) * modpower(3, b) * fact_nnnaaabb + mod)) % mod



        big_sum_mod = (big_sum_mod + moddiv(velki, ((fa * fb * xa * xb) + mod) % mod , mod) + mod) % mod
        # print(big_sum)
        # print(big_sum_mod)
        # print()


# result /= 6**n
print("big_sum:", big_sum)
print("big_sum % mod:", big_sum % mod)
print("big_sum_mod:", big_sum_mod)

result_mods = (modpower(factorial(n), 2) * (big_sum_mod)) % mod
result = (result * big_sum) % mod
result = round(result)

print("Before div mods:", result_mods)
print("Before div:", result)
print("Div with:", modpower(6, n))
result = moddiv(result, modpower(6, n), mod)

print(round(result))
print(round(result) % 10007)


result = moddiv(result_mods, modpower(6, n), mod)
print(round(result))
print(round(result) % 10007)

# n!^2/6^n *
# Sum_{a=0..n} Sum_{b=0..n-a} (-1)^b * 2^a * 3^b * (3*n-3*a-2*b)! / (a! * b! * (n-a-b)!^2 * 6^(n-a-b))
