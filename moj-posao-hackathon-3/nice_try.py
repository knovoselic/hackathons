import math

n = int(input())
if n < 3:
    print(0)
    exit(0)
elif n == 3:
    print(1)
    exit(0)

n_rows = math.factorial(n) / 6 / math.factorial(n - 3)

solution = int(math.factorial(n_rows) / math.factorial(n_rows - n))

# print(solution)
print(solution % 10007)
