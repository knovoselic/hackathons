var MOD = 10007;

function factorial(n) {
	var result = 1;
	var i = 1;

	while (i <= n) {
		result = (result * i) % MOD;
		++i;
	}

	return result;
}

function gcdExtended(a, b) {
	if (a == 0) {
		return [b, 0, 1];
	}
	[gcd, x1, y1] = gcdExtended(b % a, a);
	x = y1 - Math.floor(b / a) * x1;
	y = x1;

	return [gcd, x, y];
}

function modInv(b, m) {
	[g, x, y] = gcdExtended(b, m);
	if (g != 1) return -1;
	return (x % m + m) % m;
}

function modDiv(a, b) {
	a = a % MOD;
	inv = modInv(b, MOD);
	if (inv == -1) {
		return 255;
	}

	return (inv * a + MOD) % MOD;
}

function modPower(a, n) {
	if (n == 0) return 1;

	var result = a;
	var i = 1;
	while (i < n) {
		result = (result * a) % MOD;
		++i;
	}

	return result;
}
var n = Number(readline())
	
var c = modDiv(modPower(factorial(n), 2), modPower(6, n));
var l = 0;

for(var a = 0; a <= n; ++a) {
	for(var b = 0; b <= n -a; ++b) {
		var d = (modPower(-1, b) * modPower(2, a) * modPower(3, b)) % MOD;
		var e = factorial(3 * n - 3 * a - 2 * b);
		var f = (d * e + MOD) % MOD;

		var g = (factorial(a) * factorial(b));
		var h = modPower(factorial(n - a - b), 2);
		var i = modPower(6, n - a - b);
		var j = (g * h * i + MOD) % MOD;

		var k = modDiv(f, j);
		l = (l + k + MOD) % MOD;
	}
}

print((c * l) % MOD);

