[n, m] = [int(x) for x in input().strip().split(" ")]

print((n - m + 1) * 3);
