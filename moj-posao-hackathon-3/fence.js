n = Number(readline())

var just_values = readline().trim().split(" ").map(Number);

var values = [];

for(var i = 0; i < n; i++) {
	values.push({a: just_values[i], b: i});
}

values.sort(function(a, b) {
	if (a.a == b.a) {
		return a.b - b.b;
	} else {
		return a.a - b.a;
	}
});


var a = [];
var b = [];


for(var i = 0; i < n - 1; i++) {
	var x = values[i].b;
	var y = values[i + 1].b;
	if (x < y) {
		a.push(x);
		b.push(y);
	} else {
		a.push(y);
		b.push(x);
	}
}

a.sort(function(x, y) { return x - y; });
b.sort(function(x, y) { return x - y; });

var a_skips = [];
var b_skips = [];
var i = 0;
var ai = 0;
var count = n - 1;

while (i < n) {
	while (ai < n - 1 && a[ai] == i) {
		--count;
		++ai;
	}
	a_skips[i] = count;
	++i;
}

i = n - 1;
var bi = n - 2;
count = n - 1;
while (i >= 0) {
	while (bi >= 0 && b[bi] == i) {
		--count;
		--bi;
	}
	b_skips[i] = count;
	--i;
}

i = 0;

var output = [];
while (i < n - 1) {
	output.push(n - 1 - a_skips[i] - b_skips[i]) + " ";
	++i;
}
output.push(n - 1 - a_skips[i] - b_skips[i]);

print(output.join(' '));

