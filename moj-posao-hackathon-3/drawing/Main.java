import java.io.*;
import java.util.*;

public class Main {
	public static void main(String[] args) {
		Scanner in = new Scanner(System.in);

		int n = in.nextInt();
		int m[][] = new int[2*n-1][2*n-1];

		int cx, cy;

		cx = cy = n - 1;

		int step = 0;
		while(cx >= 0 && cy >= 0) {
			int size = step * 2 + 1;
			for(int i = 0; i < size; i++) {
				m[cx][cy + i] = step + 1;
				m[cx + size - 1][cy + i] = step + 1;
				m[cx + i][cy] = step + 1;
				m[cx + i][cy + size - 1] = step + 1;
			}

			++step;
			--cx;
			--cy;
		}

		for(int x = 0; x < 2 * n - 1; ++x) {
			for(int y = 0; y < 2 * n - 1; ++y) {
				System.out.print(m[x][y]);
			}
			System.out.println();
		}

	}
}
