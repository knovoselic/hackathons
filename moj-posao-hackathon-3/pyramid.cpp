#include <cstdio>
#include <set>

int main(void) {
	int n, level, index;

	scanf("%d", &n);
	scanf("%d", &level);
	scanf("%d", &index);

	// printf("%d %d %d\n", n, level, index);

	int current_level = level;
	int removed = 0;
	std::set<int> next_blocks;
	next_blocks.insert(index);

	while (current_level <= n) {
		std::set<int> current_blocks(next_blocks);
		next_blocks.clear();
		int next_level_size = n - current_level;
		// printf("\nCurrent level: %d\n", current_level);

		while(!current_blocks.empty()) {
			int current = *current_blocks.begin();
			current_blocks.erase(current);
			int left = current - 1;
			int right = current;
			// printf("Current block: %d\n", current);

			if (left > 0) {
				next_blocks.insert(left);
			}
			if (right <= next_level_size) {
				next_blocks.insert(right);
			}
			removed++;
		}

		++current_level;

	}

	printf("%d\n", removed - 1);

	return 0;
}
