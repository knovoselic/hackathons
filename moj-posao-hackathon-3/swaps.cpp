#include <stdio.h>
#include <stdlib.h>
#include <string.h>

static int cmpstringp(const void *p1, const void *p2) {
	char a = *(const char *)p1;
	char b = *(const char *)p2;
	return a - b;
}

int main(void) {
	int n;
	char a[40000] = {0};
	char b[40000] = {0};
	char c[40000] = {0};

	scanf("%d", &n);
	getchar();

	int count = 0;
	int index_a = 0;
	int index_b = 0;
	int index_c = 0;

	while (count < n) {
		a[index_a++] = fgetc(stdin);
		++count;
		if (count >= n) break; 

		b[index_b++] = fgetc(stdin);
		++count;
		if (count >= n) break; 

		c[index_c++] = fgetc(stdin);
		++count;
		if (count >= n) break; 
	}
	
#ifdef DEBUG
	printf("%s\n", a);
	printf("%s\n", b);
	printf("%s\n", c);
#endif

	qsort(a, index_a, sizeof(char), cmpstringp);
	qsort(b, index_b, sizeof(a[0]), cmpstringp);
	qsort(c, index_c, sizeof(a[0]), cmpstringp);

#ifdef DEBUG
	printf("%s\n", a);
	printf("%s\n", b);
	printf("%s\n", c);
#endif

	int current_index = 0;
	while(a[current_index] || b[current_index] || c[current_index]) {
		if(a[current_index]) putchar(a[current_index]);
		if(b[current_index]) putchar(b[current_index]);
		if(c[current_index]) putchar(c[current_index]);
		++current_index;
	}
	puts("");

	return 0;
}
