import java.io.*;
import java.util.*;

public class Main {
	public static class Tuple implements Comparable<Tuple> {
		private long a;
		private long b;

		public Tuple(long a, long b) {
			this.a = a;
			this.b = b;
		}

		@Override
		public int compareTo(Tuple o) {
			if (this.a == o.a) {
				return (int)(this.b - o.b);
			} else {
				return (int)(this.a - o.a);
			}
		}

		public long getA() {
			return a;
		}

		public long getB() {
			return b;
		}

	}

	public static void main(String[] args) {
		Scanner in = new Scanner(System.in);
		int n = in.nextInt();
		long just_values[] = new long[n];

		for(int i = 0; i < n; i++) {
			long temp = in.nextLong();
			just_values[i] = temp;
		}

		ArrayList<Tuple> values = new ArrayList<Tuple>();
		for(int i = 0; i < n; i++) {
			values.add(new Tuple(just_values[i], i));
		}

		Collections.sort(values);
		ArrayList<Long> a = new ArrayList<Long>();
		ArrayList<Long> b = new ArrayList<Long>();


		for(int i = 0; i < n - 1; i++) {
			long x = values.get(i).getB();
			long y = values.get(i + 1).getB();
			if (x < y) {
				a.add(x);
				b.add(y);
			} else {
				a.add(y);
				b.add(x);
			}
		}
		Collections.sort(a);
		Collections.sort(b);

		long a_skips[] = new long[n];
		long b_skips[] = new long[n];
		int i = 0;
		int ai = 0;
		long count = n - 1;

		while (i < n) {
			while (ai < n - 1 && a.get(ai) == i) {
				--count;
				++ai;
			}
			a_skips[i] = count;
			++i;
		}
	
	
		i = n - 1;
		int bi = n - 2;
		count = n - 1;
		while (i >= 0) {
			while (bi >= 0 && b.get(bi) == i) {
				--count;
				--bi;
			}
			b_skips[i] = count;
			--i;
		}
		i = 0;

		StringBuilder sb = new StringBuilder();
		while (i < n - 1) {
			sb.append(n - 1 - a_skips[i] - b_skips[i]);
			sb.append(" ");
			++i;
		}
		sb.append(n - 1 - a_skips[i] - b_skips[i]);
		System.out.println(sb.toString());
	}
}
