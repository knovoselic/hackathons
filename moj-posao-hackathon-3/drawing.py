n = int(input())

m = [[0 for x in range(2 * n - 1)] for y in range(2 * n -1)] 

cx = cy = n - 1

step = 0
while(cx >= 0 and cy >= 0):
    size = step * 2 + 1;
    for i in range(size):
        m[cx][cy + i] = step + 1
        m[cx + size - 1][cy + i] = step + 1
        m[cx + i][cy] = step + 1
        m[cx + i][cy + size - 1] = step + 1

    step += 1
    cx -= 1
    cy -= 1

for x in range(2 * n - 1):
    for y in range(2 * n - 1):
        print(m[x][y], end = '')
    print()
