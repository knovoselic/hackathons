<?php

define("MOD", 10007);

function factorial($n) {
	$result = 1;
	$i = 1;

	while ($i <= $n) {
		$result = ($result * $i) % MOD;
		++$i;
	}

	return $result;
}

function gcdExtended($a, $b, &$x, &$y) {
	if ($a == 0) {
		$x = 0;
		$y = 1;
		return $b;
	}
	$x1 = 1;
	$y1 = 1;
	$gcd = gcdExtended($b % $a, $a, $x1, $y1);
	$x = $y1 - intdiv($b, $a) * $x1;
	$y = $x1;

	return $gcd;
}

function modInv($b, $m) {
	$x = 1;
	$y = 1;
	$g = gcdExtended($b, $m, $x, $y);
	if ($g != 1) return -1;
	return ($x % $m + $m) % $m;
}

function modDiv($a, $b) {
	$a = $a % MOD;
	$inv = modInv($b, MOD);
	if ($inv == -1) {
		return 255;
	}

	return ($inv * $a + MOD) % MOD;
}

function modPower($a, $n) {
	if ($n == 0) return 1;

	$result = $a;
	$i = 1;
	while ($i < $n) {
		$result = ($result * $a) % MOD;
		++$i;
	}

	return $result;
}

fscanf(STDIN, "%d", $n);

$c = modDiv(modPower(factorial($n), 2), modPower(6, $n));
$l = 0;

for($a = 0; $a <= $n; ++$a) {
	for($b = 0; $b <= $n -$a; ++$b) {
		$d = (modPower(-1, $b) * modPower(2, $a) * modPower(3, $b)) % MOD;
		$e = factorial(3 * $n - 3 * $a - 2 * $b);
		$f = ($d * $e + MOD) % MOD;

		$g = (factorial($a) * factorial($b));
		$h = modPower(factorial($n - $a - $b), 2);
		$i = modPower(6, $n - $a - $b);
		$j = ($g * $h * $i + MOD) % MOD;

		$k = modDiv($f, $j);
		$l = ($l + $k + MOD) % MOD;
	}
}

printf("%d\n", ($c * $l) % MOD);

?>
