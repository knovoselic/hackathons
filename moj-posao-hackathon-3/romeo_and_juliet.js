function isReachable(x, y) {
	if (x < 1 || y < 1 || x > n || y > m ) {
		return false;
	}
	if (field[x][y] == 'J') {
		return true;
	} else if (field[x][y] != '#') {
		return false;
	}

	field[x][y] = '-';

	return isReachable(x, y + 1) ||
		isReachable(x, y - 1) ||
		isReachable(x + 1, y) ||
		isReachable(x - 1, y);
}

function dump() {
	for(var i = 0; i < n + 2; i++) {
		for(var j = 0; j < m + 2; j++) {
			if (field[i][j] != 0)
				putstr(field[i][j]);
			else
				putstr(0);
		}
		print();
	}
}

function runSim() {
	 var changed = false;

	 for(var x = 1; x <= n; ++x) {
	 	for(var y = 1; y <= m; y++) {
	 		if (field[x][y] != '#') continue;
	 		if (backup[x - 1][y] == '.' ||
	 				backup[x + 1][y] == '.' ||
	 				backup[x][y - 1] == '.' ||
	 				backup[x][y + 1] == '.') {
	 			field[x][y] = '.';
	 			changed = true;
	 		}
	 	}
	 }
	 return changed;
}

var years = 0;
var field = Array(52);
var backup = Array(52);

for(var i = 0; i < 52; i++) {
        field[i] = Array(54);
        backup[i] = Array(54);
}

var n, m;
var rx, ry;

[n, m] = readline().trim().split(" ").map(Number);

for(var i = 0; i < n + 2; ++i) {
	field[i][0] = '+';
	field[i][m + 1] = '+';
}
for(var i = 0; i < m + 2; ++i) {
	field[0][i] = '+';
	field[n + 1][i] = '+';
}
for(var i = 1; i <= n; ++i) {
	var line = readline();
	// print(line);
	for (var j = 0; j < line.length; j++) {
		field[i][j + 1] = line.charAt(j);
	}
}

// dump();
//print(field);

for(var x = 1; x <= n; ++x) {
	for(var y = 1; y <= m; y++) {
		if (field[x][y] == 'R') {
			rx = x;
			ry = y;
			x = n;
			break;
		}
	}
}

backup = JSON.parse(JSON.stringify(field));
field[rx][ry] = '#';
// dump();
while(isReachable(rx, ry)) {
	// dump();
	field = JSON.parse(JSON.stringify(backup));
	if (!runSim()) {
		years = -1;
		break;
	}
	backup = JSON.parse(JSON.stringify(field));
	field[rx][ry] = '#';
	++years;

}
// dump();

print(years);
