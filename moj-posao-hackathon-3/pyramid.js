var n, level, index;

n = Number(readline())
level = Number(readline())
index = Number(readline())

var current_level = level;
var removed = 0;
var next_blocks = [];

next_blocks.push(index);

while (current_level <= n) {
	var current_blocks = next_blocks;
	next_blocks = [];
	var next_level_size = n - current_level;

	while(current_blocks.length > 0) {
		var current = current_blocks.pop();
		var left = current - 1;
		var right = current;

		if (left > 0 && next_blocks.indexOf(left) === -1) {
			next_blocks.push(left);
		}
		if (right <= next_level_size && next_blocks.indexOf(right) === -1) {
			next_blocks.push(right);
		}
		removed++;
	}

	++current_level;

}

print(removed - 1);

