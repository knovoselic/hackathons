<?php

fscanf(STDIN, "%d", $n);
$just_values = array_map('intval', explode(" ", trim(fgets(STDIN, 10240000))));

$values = [];


for($i = 0; $i < $n; $i++) {
	$values[$just_values[$i]] = $i;
}

ksort($values);

$a = [];
$b = [];

$keys = array_keys($values);
for($i = 0; $i < $n - 1; $i++) {
	$x = $values[$keys[$i]];
	$y = $values[$keys[$i + 1]];

	if ($x < $y) {
		array_push($a, $x);
		array_push($b, $y);
	} else {
		array_push($a, $y);
		array_push($b, $x);
	}
}

sort($a);
sort($b);

$a_skips = array_fill(0, $n, 0);
$b_skips = array_fill(0, $n, 0);

$i = 0;
$ai = 0;
$count = $n - 1;


while ($i < $n) {
	while ($ai < $n - 1 && $a[$ai] == $i) {
		--$count;
		++$ai;
	}
	$a_skips[$i] = $count;
	++$i;
}

$i = $n - 1;
$bi = $n - 2;
$count = $n - 1;
while ($i >= 0) {
	while ($bi >= 0 && $b[$bi] == $i) {
		--$count;
		--$bi;
	}
	$b_skips[$i] = $count;
	--$i;
}

$i = 0;

while ($i < $n - 1) {
	printf("%d ", $n - 1 - $a_skips[$i] - $b_skips[$i]);
	++$i;
}
printf("%d\n", $n - 1 - $a_skips[$i] - $b_skips[$i]);


return 0;
?>
