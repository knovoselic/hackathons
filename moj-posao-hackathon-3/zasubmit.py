n = int(input())
values = list(map(int, input().strip().split()))
values = [(values[i], i) for i in range(len(values))]

values.sort()

a = []
b = []

for i in range(n - 1):
    x = min(values[i][1], values[i + 1][1])
    y = max(values[i][1], values[i + 1][1])
    a.append(x)
    b.append(y)

a.sort()
b.sort()

a_skips = [0] * n
i = 0
ai = 0
count = n - 1

while i < n:
    while ai < n - 1 and a[ai] == i:
        count -= 1
        ai += 1
    a_skips[i] = count
    i += 1

b_skips = [0] * n
i = n - 1
bi = n - 2
count = n - 1
while i >= 0:
    while bi >= 0 and b[bi] == i:
        count -= 1
        bi -= 1
    b_skips[i] = count
    i -= 1

i = 0
while i < n - 1:
    print(n - 1 - a_skips[i] - b_skips[i], end = ' ')
    i += 1

print(n - 1 - a_skips[i] - b_skips[i])

