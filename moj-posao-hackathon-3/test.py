mod = 10007
def gcdExtended(a, b):
    if a == 0: return b, 0, 1
    g, x1, y1 = gcdExtended(b % a, a)
    x = y1 - b // a * x1
    y = x1

    return g, x, y

def modInv(b, m):
    g, x, y = gcdExtended(b, m)

    if g != 1: return -1
    return (x % m + m) % m

def ninv(b, m):
    b = b % m
    for x in range(1, m):
        if (b * x) % m == 1: return x
    return -1

def moddiv(a, b, m):
    a = a % m
    inv = modInv(b, m)
    print('-', inv)
    print('-', ninv(b, m))
    if inv == -1:
        raise 'AAAAAA'

    return (inv * a + m) % m

def modpower(a, n):
    mod = 10007
    result = a
    i = 1
    while i < n:
        result = (result * a) % mod
        i += 1

    return result


print(moddiv(3678, 6628, mod))

print(moddiv(85, 9747, mod))



print(moddiv(4390, 8447, mod))
