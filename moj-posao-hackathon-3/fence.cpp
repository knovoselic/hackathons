#include <cstdio>
#include <vector>
#include <tuple>
#include <algorithm>

int main(void) {
	int n;

	scanf("%d", &n);
	std::vector<int> just_values(n);

	for(int i = 0; i < n; i++) {
		int temp;
		scanf("%d", &temp);
		just_values[i] = temp;
	}

	std::vector<std::tuple<int, int> > values(n);

	for(int i = 0; i < n; i++) {
		values[i] = std::make_tuple(just_values[i], i);
	}

	std::sort(values.begin(), values.end());

	std::vector<int> a, b;

	for(int i = 0; i < n - 1; i++) {
		int x = std::get<1>(values[i]);
		int y = std::get<1>(values[i + 1]);
		if (x < y) {
			a.push_back(x);
			b.push_back(y);
		} else {
			a.push_back(y);
			b.push_back(x);
		}

	}

	std::sort(a.begin(), a.end());
	std::sort(b.begin(), b.end());

	std::vector<int> a_skips(n);

	int i = 0;
	int ai = 0;
	int count = n - 1;

	while (i < n) {
		while (ai < n - 1 && a[ai] == i) {
			--count;
			++ai;
		}
		a_skips[i] = count;
		++i;
	}


	std::vector<int> b_skips(n);

	i = n - 1;
	int bi = n - 2;
	count = n - 1;
	while (i >= 0) {
		while (bi >= 0 && b[bi] == i) {
			--count;
			--bi;
		}
		b_skips[i] = count;
		--i;
	}

	i = 0;

	while (i < n - 1) {
		printf("%d ", n - 1 - a_skips[i] - b_skips[i]);
		++i;
	}
	printf("%d\n", n - 1 - a_skips[i] - b_skips[i]);


	return 0;
}
