import java.io.*;
import java.util.*;

public class Main {
	public static void main(String[] args) {
		Scanner in = new Scanner(System.in);
		int n, level, index;

		n = in.nextInt();
		level = in.nextInt();
		index = in.nextInt();

		int current_level = level;
		int removed = 0;
		HashSet<Integer> next_blocks = new HashSet<Integer>();
		next_blocks.add(index);

		while (current_level <= n) {
			HashSet<Integer> current_blocks = (HashSet<Integer>)next_blocks.clone();
			next_blocks.clear();
			int next_level_size = n - current_level;

			while(!current_blocks.isEmpty()) {
				int current = current_blocks.iterator().next();
				current_blocks.remove(current);
				int left = current - 1;
				int right = current;

				if (left > 0) {
					next_blocks.add(left);
				}
				if (right <= next_level_size) {
					next_blocks.add(right);
				}
				removed++;
			}

			++current_level;
		}

		System.out.println(removed - 1);
	}
}
