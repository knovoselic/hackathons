import java.io.*;
import java.util.*;

public class Main {
	public static char[][] field = new char[52][54];
	public static char[][] backup = new char[52][54];
	public static int n, m;
	public static int rx, ry;

	public static Boolean isReachable(int x, int y) {
		if (x < 1 || y < 1 || x > n || y > m ) {
			return false;
		}
		if (field[x][y] == 'J') {
			return true;
		} else if (field[x][y] != '#') {
			return false;
		}

		field[x][y] = '-';

		return isReachable(x, y + 1) ||
			isReachable(x, y - 1) ||
			isReachable(x + 1, y) ||
			isReachable(x - 1, y);
	}

	public static void dump() {
		for(int i = 0; i < n + 2; i++) {
			for(int j = 0; j < m + 2; j++) {
				if (field[i][j] != 0)
					System.out.print(field[i][j]);
				else
					System.out.print(0);
			}
			System.out.println();
		}
	}

	public static Boolean runSim() {
		 Boolean changed = false;

		 for(int x = 1; x <= n; ++x) {
		 	for(int y = 1; y <= m; y++) {
		 		if (field[x][y] != '#') continue;
		 		if (backup[x - 1][y] == '.' ||
		 				backup[x + 1][y] == '.' ||
		 				backup[x][y - 1] == '.' ||
		 				backup[x][y + 1] == '.') {
		 			field[x][y] = '.';
		 			changed = true;
		 		}
		 	}
		 }
		 return changed;
	}

	public static void main(String[] args) {
		int years = 0;
		Scanner in = new Scanner(System.in);

		n = in.nextInt();
		m = in.nextInt();

		in.nextLine();
		for(int i = 0; i < n + 2; ++i) {
			field[i][0] = '+';
			field[i][m + 1] = '+';
		}
		for(int i = 0; i < m + 2; ++i) {
			field[0][i] = '+';
			field[n + 1][i] = '+';
		}
		for(int i = 1; i <= n; ++i) {
			String line = in.nextLine();
			for (int j = 0; j < line.length(); j++) {
				field[i][j + 1] = line.charAt(j);
			}
		}

		for(int x = 1; x <= n; ++x) {
			for(int y = 1; y <= m; y++) {
				if (field[x][y] == 'R') {
					rx = x;
					ry = y;
					x = n;
					break;
				}
			}
		}
	
		backup = field.clone();
		for(int i = 0; i < 52; i++) {
			backup[i] = field[i].clone();
		}
		field[rx][ry] = '#';
		// dump();
		while(isReachable(rx, ry)) {
			// dump();
			field = backup.clone();
			for(int i = 0; i < 52; i++) {
				field[i] = backup[i].clone();
			}
			if (!runSim()) {
				years = -1;
				break;
			}
			backup = field.clone();
			for(int i = 0; i < 52; i++) {
				backup[i] = field[i].clone();
			}
			field[rx][ry] = '#';
			++years;
	
		}
		// dump();
	
		System.out.println(years);
	}
}

/*


int main(void) {
}
*/
