#include <stdio.h>
#define MOD 10007

long long factorial(long long n) {
	long long result = 1;
	long long i = 1;

	while (i <= n) {
		result = (result * i) % MOD;
		++i;
	}

	return result;
}

long long gcdExtended(long long a, long long b, long long *x, long long *y) {
	if (a == 0) {
		*x = 0;
		*y = 1;
		return b;
	}
	long long x1, y1;
	long long gcd = gcdExtended(b % a, a, &x1, &y1);
	*x = y1 - (b / a) * x1;
	*y = x1;

	return gcd;
}

long long modInv(long long b, long long m) {
	long long x, y;
	long long g = gcdExtended(b, m, &x, &y);
	if (g != 1) return -1;
	return (x % m + m) % m;
}

long long modDiv(long long a, long long b) {
	a = a % MOD;
	long long inv = modInv(b, MOD);
	if (inv == -1) {
		return 255;
	}

	return (inv * a + MOD) % MOD;
}

long long modPower(long long a, long long n) {
	if (n == 0) return 1;

	long long result = a;
	long long i = 1;
	while (i < n) {
		result = (result * a) % MOD;
		++i;
	}

	return result;
}

int main(void) {
	int n;

	scanf("%d", &n);
	
	long long c = modDiv(modPower(factorial(n), 2), modPower(6, n));
	long long l = 0;

	for(long long a = 0; a <= n; ++a) {
		for(long long b = 0; b <= n -a; ++b) {
			long long d = (modPower(-1, b) * modPower(2, a) * modPower(3, b)) % MOD;
			long long e = factorial(3 * n - 3 * a - 2 * b);
			long long f = (d * e + MOD) % MOD;

			long long g = (factorial(a) * factorial(b));
			long long h = modPower(factorial(n - a - b), 2);
			long long i = modPower(6, n - a - b);
			long long j = (g * h * i + MOD) % MOD;

			long long k = modDiv(f, j);
			l = (l + k + MOD) % MOD;
		}
	}

	printf("%lld\n", (c * l) % MOD);

	return 0;
}
