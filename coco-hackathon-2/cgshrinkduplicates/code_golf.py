from dmoj.graders.standard import StandardGrader
from dmoj.result import Result, CheckerResult
import math

# 70 -> 100%
# 200 -> 25%
# 250 -> 10%
# 300 -> 0%

class Grader(StandardGrader):
    def check_result(self, case, result):
        passed = StandardGrader.check_result(self, case, result);
        if passed:
            points = min(2.28 * math.e **(-0.0116 * len(self.source)) * case.points, case.points)
        else:
            points = 0

        return CheckerResult(passed, points);
