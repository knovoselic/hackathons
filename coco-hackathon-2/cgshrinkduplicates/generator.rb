#!/usr/bin/env ruby
sizes = [2, 10, 44, 467, 4325, 10000, 24567, 43568, 87989, 99999]

sizes.each_with_index do |size, i|
  last = rand 1..10
  data = Array.new(size) do
    repeat = [true, false].sample
    last = rand 1..1000 unless repeat
    last
  end
  File.write "secret.#{i}.in", "#{data.join(' ')}\n"
end
