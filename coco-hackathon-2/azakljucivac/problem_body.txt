Nakon godina mukotrpnog prikupljanja podataka i anketiranja građana, Državni zavod za statistiku napokon ima dovoljno podataka o željenim temama kako bi mogao sastaviti završni izvještaj. No kako živimo gdje živimo, umjesto rezultata imamo burek. Što se točno dogodilo i kako vi možete pomoći? Saznat ćete uskoro, no prvo da objasnimo o čemu se točno radi.

Svako istraživanje sastoji se od jednog pitanja, te odgovora građana. Na primjer, jedno  vrlo važno istraživanje postavljalo je pitanje "Koje je Vaše najdraže jelo?" na koje su građani nudili odgovore tipa "pizza", "mahune", "janjetina" i slično. Nakon što je završila faza prikupljanja odgovora, vrlo napredan software pisan u JavaScriptu obrađuje odgovore i ispisuje izvještaj u kojemu odgovore rangira od onih najviše odabranih, do onih koje su građani najmanje odabrali.

No nije ovdje kraj priče. Vrhunski developer Nick napravio je mali propust u softwareu (kaže JavaScript je kriv) te je uspio pobrisati sve prikupljene rezultate i generirane izvještaje. Nakon mukotrpnog spašavanja podataka, rezultati su, onak', ne dobri. Odgovori građana su zauvijek izgubljeni. Jedino što su uspjeli spasiti su parcijalni izvještaji u ovom obliku:

- Odgovori "janjetina", "pizza" nalaze se na jednom od prva 2 mjesta
- Odgovor "mahune" nalaze se na jednom od prva 3 mjesta

Gornje izjave su ručno zapisane u YAML datoteke u ovom formatu:

    statements:
      - {items: [janjetina, pizza], position: 2}
      - {items: [mahune], position: 3}

Spasi DZS od sramote i porezne obaveznike od dodatnih troškova! Napiši program koji će odrediti sve odgovore čiji se točan položaj može odrediti na temelju spašenih podataka (izjava). Za gornji primjer moguće je odrediti da se mahune nalaze na 3. mjestu jer janjetina i pizza moraju biti na jednom od prva dva mjesta.

Ulaz
----
Sa standardnog ulaza potrebno je učitati YAML datoteku koji sadrži sve spašene izjave. Veličina YAML datoteke je nepoznata. YAML datoteka neće sadržavati međusobno kontradiktorne izjave. Maksimalan broj izjava u jednoj YAML datoteci je 500.

Izlaz
-----
Ispiši sve odgovore čija se pozicija u izvještaju može točno odrediti u formatu: "{pozicija} {odgovor}". Postojat će barem jedna izjava čija se pozicija može točno odrediti. Ispis sortiraj po rednom broju pozicije.

Test slučajevi
------------

 - broj izjava bit će manji od 100 unutar 40% test slučajeva
 - broj izjava bit će manji od 500 unutar 60% test slučajeva

##INSERT_SAMPLE_TEST_CASES_HERE##
