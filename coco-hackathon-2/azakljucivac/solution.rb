require 'yaml'

items_hash = {}

statements = YAML.load(ARGF.read).fetch('statements')

statements.each do |statement|
  statement['items'].each do |item|
    items_hash[item] = [statement['position'], items_hash.fetch(item, 1e9)].min
  end
end

items_sorted = items_hash.sort_by(&:last)
items_sorted.each_with_index do |(name, position), index|
  if index == 0 && position == 1
    puts "#{position} #{name}"
  elsif items_sorted[index - 1].last != position
    puts "#{position} #{name}" if position - 1 == index
  end
end
