#!/usr/bin/env ruby
sizes = [20, 500, 4325, 10000, 24567, 43568, 87989, 294587, 746152, 943158]

sizes.each_with_index do |size, i|
  data = Array.new(size) { rand size..1_000_000 }
  File.write "secret.#{i}.in", "#{size}\n#{data.join(' ')}\n"
  File.write "secret.#{i}.out", "#{data.max - data.min}\n"
end
