solution = 0
i = 0
gets.chomp.chars.each do |c|
  if c.upcase == c && i % 4 != 0
    nops = 4 - i % 4
    solution += nops
    i += nops
  end
  i += 1
end

puts solution
