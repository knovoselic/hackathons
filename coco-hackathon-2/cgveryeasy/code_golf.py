from dmoj.graders.standard import StandardGrader
from dmoj.result import Result, CheckerResult
import math

# 40  -> 100%
# 70  -> 25%
# 100 -> 1%
# 200 -> 0%

class Grader(StandardGrader):
    def check_result(self, case, result):
        passed = StandardGrader.check_result(self, case, result);
        if passed:
            points = min(7.78 * math.e **(-0.0509 * len(self.source)) * case.points, case.points)
        else:
            points = 0

        return CheckerResult(passed, points);
