Napiši program koji ispisuje string 'COCO RULZ' te nakon njega znak za novu liniju LF (line feed).

Ulaz
----
Nema.

Izlaz
-----
Potrebno je ispisati string 'COCO RULZ' s LF znakom na kraju.

##INSERT_SAMPLE_TEST_CASES_HERE##
