import java.util.Scanner;

public class Main {

     public static void main(String []args){
        Scanner in = new Scanner(System.in);
        // read integer into variable n
        int n = in.nextInt();
        long[] height = new long[n];

        for(int i = 0; i < n; i++) {
            height[i] = in.nextLong();
        }

        long result = 0;
        int start = 0;
        int end = height.length - 1;
        while (start < end) {
            if (height[start] <= height[end]) {
                long current = height[start];
                while (height[++start] < current) {
                    result += current - height[start];
                }
            } else {
                long current = height[end];
                while(height[--end] < current) {
                    result += current - height[end];
                }
            }
        }

        System.out.println(result);
     }
}