#include <cstdio>
#include <vector>
#include <stack>
#include <algorithm>

using namespace std;
// long long trap(std::vector<long long>& height)
// {
//     long long ans = 0;
//     long long size = height.size();
//     for (long long i = 1; i < size - 1; i++) {
//         long long max_left = 0, max_right = 0;
//         for (long long j = i; j >= 0; j--) { //Search the left part for max bar size
//             max_left = std::max(max_left, height[j]);
//         }
//         for (long long j = i; j < size; j++) { //Search the right part for max bar size
//             max_right = std::max(max_right, height[j]);
//         }
//         ans += std::min(max_left, max_right) - height[i];
//     }
//     return ans;
// }

// long long trap(vector<long long>& height)
// {
//     long long ans = 0;
//     long long size = height.size();
//     vector<long long> left_max(size), right_max(size);
//     left_max[0] = height[0];
//     for (int i = 1; i < size; i++) {
//         left_max[i] = max(height[i], left_max[i - 1]);
//     }
//     right_max[size - 1] = height[size - 1];
//     for (int i = size - 2; i >= 0; i--) {
//         right_max[i] = max(height[i], right_max[i + 1]);
//     }
//     for (int i = 1; i < size - 1; i++) {
//         ans += min(left_max[i], right_max[i]) - height[i];
//     }
//     return ans;
// }

// long long trap(vector<long long>& height)
// {
//     long long ans = 0, current = 0;
//     stack<long long> st;
//     while (current < height.size()) {
//         while (!st.empty() && height[current] > height[st.top()]) {
//             long long top = st.top();
//             st.pop();
//             if (st.empty())
//                 break;
//             long long distance = current - st.top() - 1;
//             long long bounded_height = min(height[current], height[st.top()]) - height[top];
//             ans += distance * bounded_height;
//         }
//         st.push(current++);
//     }
//     return ans;
// }

long long trap(vector<long long>& height)
{
    long long left = 0, right = height.size() - 1;
    long long ans = 0;
    long long left_max = 0, right_max = 0;
    while (left < right) {
        if (height[left] < height[right]) {
            height[left] >= left_max ? (left_max = height[left]) : ans += (left_max - height[left]);
            ++left;
        }
        else {
            height[right] >= right_max ? (right_max = height[right]) : ans += (right_max - height[right]);
            --right;
        }
    }
    return ans;
}

int main(void)
{
    long long n, temp;
    vector<long long> A;

    scanf("%lld", &n);

    while(n) {
        scanf("%lld", &temp);
        A.push_back(temp);
        --n;
    }

    printf("%lld\n", trap(A));
}