N = int(input())
height = list(map(int, input().strip().split()))

# max_R_pt = height.index(max(height))
# print(max_R_pt)
# max_L_pt = 0
# max_Lh = height[0]
# rain_trap = 0

# for i in range(N-1):
#     if i == max_R_pt and i<N-1:
#         right_height = height[i+1:]
#         max_R_pt = i+1+right_height.index(max(right_height))

#     if i>0 and height[i-1] > max_Lh:
#         max_Lh = height[i-1]
#         max_L_pt = i-1

#     rain_trap += max(0, min(height[max_R_pt], height[max_L_pt])-height[i])
# print(rain_trap)

result = start = 0
end = len(height) - 1
while start < end:
    if height[start] <= height[end]:
        current = height[start]
        start += 1
        while height[start] < current:
            result += current - height[start]
            start += 1
    else:
        current = height[end]
        end -= 1
        while height[end] < current:
            result += current - height[end]
            end -= 1

print(result)