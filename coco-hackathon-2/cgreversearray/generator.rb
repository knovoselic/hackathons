#!/usr/bin/env ruby
sizes = [20, 500, 4325, 10000, 24567, 43568, 87989, 125740, 648766, 945376]

sizes.each_with_index do |size, i|
  data = Array.new(size) { rand 0..1000 }
  File.write "secret.#{i}.in", "#{data.join(' ')}\n"
  File.write "secret.#{i}.out", "#{data.reverse.join(' ')}\n"
end
