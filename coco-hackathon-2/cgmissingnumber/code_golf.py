from dmoj.graders.standard import StandardGrader
from dmoj.result import Result, CheckerResult
import math

# 55  -> 100%
# 95  -> 70%
# 180 -> 25%
# 300 -> 10%

class Grader(StandardGrader):
    def check_result(self, case, result):
        passed = StandardGrader.check_result(self, case, result);
        if passed:
            points = min(1.74 * math.e **(-0.00998 * len(self.source)) * case.points, case.points)
        else:
            points = 0

        return CheckerResult(passed, points);
