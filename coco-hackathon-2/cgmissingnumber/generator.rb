#!/usr/bin/env ruby
MAX=10e6
sizes = [2, 10, 44, 467, 4325, 10000, 24567, 43568, 87989, 99999]

sizes.each_with_index do |size, i|
  sum = 0
  data = Array.new(size) do
    element = rand 1..((MAX-sum)/size).to_i
    sum += element
    element
  end
  missing = data.sample
  data.delete_at data.index(missing)
  File.write "secret.#{i}.in", "#{size} #{sum}\n#{data.join(' ')}\n"
  File.write "secret.#{i}.out", "#{missing}\n"
end
