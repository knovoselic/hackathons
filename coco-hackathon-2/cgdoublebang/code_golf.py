from dmoj.graders.standard import StandardGrader
from dmoj.result import Result, CheckerResult
import math

# 85  -> 100%
# 120 -> 70%
# 220 -> 25%
# 300 -> 10%

class Grader(StandardGrader):
    def check_result(self, case, result):
        passed = StandardGrader.check_result(self, case, result);
        if passed:
            points = min(2.46 * math.e **(-0.0105 * len(self.source)) * case.points, case.points)
        else:
            points = 0

        return CheckerResult(passed, points);
