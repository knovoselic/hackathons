#!/usr/bin/env ruby
alphabet = ('a'..'z').to_a
sizes = [2, 5, 10, 12, 24, 28, 44, 58, 67, 99]

sizes.each_with_index do |size, i|
  first = Array.new(size) { alphabet.sample }
  second = Array.new(size) { alphabet.sample }
  File.write "secret.#{i}.in", "#{first.join}\n#{second.join}\n"
end
