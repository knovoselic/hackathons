
/*
  Zupanijsko natjecanje 2006 - Srednjoskolska skupina - I. podskupina
  Zadatak KASKADE
  Rjesenje napisali: Ante Derek, Luka Kalinovcic, Lovro Puzar

  Opis algoritma:

  Zadatak rjesavamo u dva koraka. U prvom koraku na temelju pocetnog
  prikaza ekrana odredimo sirinu, visinu te naziv svakog prozora.
  U drugom koraku slazemo prozore na ekran kako je opisanu u zadatku.

  Na pocetnom ekranu gornji lijevi kut prozora mozemo pronaci koristeci
  sljedeci kriterij: na odredjenoj poziciji se nalazi znak '+', neposredno
  dolje se nalazi znak '|' i neposredno desno se nalazi znak '-', te takodjer
  vrijedi da desno od tog '+'-a, prvi znak koji nije '-' je nuzno
  '|'. Visinu, odnosno sirinu, prozora odredimo tako da nadjemo sljedeci
  '+' dolje, odnosno desno.

  Nakon sortiranja po imenu, prozore direktno crtamo na ekran od
  najudaljenijeg (u prvom redu) prema najblizem. Moramo paziti i da
  popunimo unutrasnjost svakog prozora znakovima '.' kako bi prekrili
  prozore ispod.
*/

#include <iostream>
#include <string>
#include <vector>
#include <algorithm>

using namespace std;

const int MAX=100;

struct prozor
{
        int vis;
        int sir;
        string ime;
        bool operator< (const prozor &a) const
        {
                return ime<a.ime;
        }
};

int n, m;
char e[MAX][MAX+1];
vector<prozor> p;

void citaj_ulaz (void)
{
        cin >> n >> m;
        for (int i=0; i<n; i++)
                cin >> e[i];
}

void odredi_prozor (int x, int y)
{
        // odredi da li je ovo gornji lijevi rub prozora
        if (e[x][y]!='+') return ;
        if (e[x][y+1]!='-') return ;
        if (e[x+1][y]!='|') return ;

        int i=1;
        while (e[x][y+i]=='-')
                i++;
        if (e[x][y+i]!='|') return ;

        prozor a;

        // odredi sirinu
        a.sir=1;
        while (e[x][y+a.sir]!='+')
                a.sir++;

        // odredi visinu
        a.vis=1;
        while (e[x+a.vis][y]!='+')
                a.vis++;

        // odredi naziv
        i=y+1;
        while (e[x][i]!='|')
                i++;
        i++;
        while (e[x][i]!='|')
                a.ime+=e[x][i++];

        p.push_back(a);
}


void nadji_sve_prozore (void)
{
        for (int i=0; i<n-1; i++)
                for (int j=0; j<m-1; j++)
                        odredi_prozor(i, j);
}

void ispisi_rjesenje (void)
{
        // sortiraj prozore
        sort(p.begin(), p.end());

        // isprazni ekran
        for (int i=0; i<n; i++)
                for (int j=0; j<m; j++)
                        e[i][j]='.';

        // nacrtaj prozore
        for (int i=0; i<(int)p.size(); i++)
        {
                // uglovi
                e[i][i]=e[i+p[i].vis][i]=e[i][i+p[i].sir]=e[i+p[i].vis][i+p[i].sir]='+';

                // vodoravni rubovi
                for (int j=1; j<p[i].sir; j++)
                        e[i][i+j]=e[i+p[i].vis][i+j]='-';

                // okomiti rubovi
                for (int j=1; j<p[i].vis; j++)
                        e[i+j][i]=e[i+j][i+p[i].sir]='|';

                // naziv
                int start=(p[i].sir-(int)p[i].ime.size()+1)/2;
                e[i][i+start-1]=e[i][i+start+p[i].ime.size()]='|';
                for (int j=0; j<(int)p[i].ime.size(); j++)
                        e[i][i+start+j]=p[i].ime[j];

                // unutrasnjost
                for (int j=1; j<p[i].vis; j++)
                        for (int k=1; k<p[i].sir; k++)
                                e[i+j][i+k]='.';
        }

        // ispisi ekran
        for (int i=0; i<n; i++)
                cout << e[i] << endl;
}

int main (void)
{
        citaj_ulaz();
        nadji_sve_prozore();
        ispisi_rjesenje();

        return 0;
}
