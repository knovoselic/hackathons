map = ARGF.readlines.each(&:chomp!)
map.shift

find_map = map.dup
windows = []
while find_map.any? { |l| l.include? '+' }
	y = find_map.index { |l| l.include? '+' }
	x = find_map[y].index '+'
	h = find_map.drop(y + 1).index { |l| l[x] == '+' } + 2
	w = find_map[y].index('+', x + 1) - x + 1
	title = find_map[y].match(/\|(\w+)\|/)[1]
	h.times do |j|
		w.times do |i|
			find_map[y + j][x + i] = '.'
		end
	end
	windows << {
		y: y,
		x: x,
		h: h,
		w: w,
		title: title
	}
end
x = 0
y = 0
windows.sort_by { |w| w[:title] }.each do |window|
	find_map[y][x] = '+'
	find_map[y + window[:h] - 1][x] = '+'
	find_map[y][x + window[:w] - 1] = '+'
	find_map[y + window[:h] - 1][x + window[:w] - 1] = '+'
	(1..window[:h]-2).each do |j|
		find_map[y + j][x] = '|'
		find_map[y + j][x + window[:w] - 1] = '|'
	end
	(1..window[:w]-2).each do |i|
		find_map[y][x + i] = '-'
		find_map[y + window[:h] - 1][x + i] = '-'
	end
	window[:title] = "|#{window[:title]}|"
	title_x = (window[:w] - window[:title].size) / 2 + x
	find_map[y][(title_x)...(title_x + window[:title].size)] = window[:title]
	(1..window[:h]-2).each do |j|
		(1..window[:w]-2).each do |i|
			find_map[y + j][x + i] = '.'
		end
	end

	x += 1
	y += 1
end
puts find_map.join "\n"
