n, r, l = list(map(int, input().strip().split()))
layout = input().strip()

solution = 99999
stack = [(layout.find('F'), 0)]

layout = list(layout)

while stack:
    current, jumps = stack.pop()
    layout[current] = '+'

    if current - l < 0:
        solution = min(solution, jumps)
    elif layout[current - l] == '#':
        stack.append((current - l, jumps + 1))

    if current + r >= n:
        solution = min(solution, jumps)
    elif layout[current + r] == '#':
        stack.append((current + r, jumps +1))

if solution == 99999:
    print('NO')
else:
    print(solution + 1)
