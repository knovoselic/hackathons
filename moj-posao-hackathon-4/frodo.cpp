#include <cstdio>
#include <cstring>
#include <stack>

struct State {
    int current;
    int jumps;

    State(int c, int j) {
        current = c;
        jumps = j;
    }
};

char layout[200000] = {0};

int main(void) {
    int n, r, l;
    int solution = 99999;

    scanf("%d %d %d %s", &n, &r, &l, layout);

    std::stack<State> stack;
    stack.push(State(strchr(layout, 'F') - layout, 0));

    while(!stack.empty()) {
        State state = stack.top();
        stack.pop();
        layout[state.current] = '+';
        if (state.current - l < 0) {
            solution = std::min(solution, state.jumps);
        } else if (layout[state.current - l] == '#') {
            stack.push(State(state.current - l, state.jumps + 1));
        }

        if (state.current + r >= n) {
            solution = std::min(solution, state.jumps);
        } else if (layout[state.current + r] == '#') {
            stack.push(State(state.current + r, state.jumps + 1));
        }
    }


    if (solution == 99999) {
        printf("NO\n");
    } else {
        printf("%d\n", solution + 1);
    }
    return 0;
}
