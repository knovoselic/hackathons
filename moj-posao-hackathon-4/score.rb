require 'open-uri'
require 'json'

result = open 'https://hackathon.dankarijera.com/hacklanje/api/leaderBoard/1000/1/all/1/all', 'Cookie' => 'session=ee4e3d731b13dece856626b345fb0a2d'
result = JSON.parse result.read
users = result['leaderBoard'].map do |user|
  {
    name: user['nickname'],
    score: user['score'].to_i
  }
end

users.sort_by! { |x| -x[:score] }
users.each_with_index do |user, index|
  puts "#{index + 1}. #{user[:name]} #{user[:score]}"
end
