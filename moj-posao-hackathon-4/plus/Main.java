import java.io.*;
import java.util.*;

public class Main {
  public static void main(String[] args) {
    int n, m;
    Scanner in = new Scanner(System.in);

    n = in.nextInt();
    m = in.nextInt();

    char[][] mat = new char[n][n];
    for (char[] row: mat) {
      Arrays.fill(row, '.');
    }

    for(int i = 0; i < m; ++i) {
      int x = in.nextInt() - 1;
      int y = in.nextInt() - 1;
      char c = in.next().charAt(0);
      for(int j = 0; j < n; ++j) {
        if (mat[j][y] < c) {
          mat[j][y] = c;
        }
        if (mat[x][j] < c) {
          mat[x][j] = c;
        }
      }
    }

    for(int x = 0; x < n ; ++x) {
      for(int y = 0; y < n; ++y) {
        System.out.print(mat[x][y]);
      }
      System.out.println();
    }
  }
}
