<?php

$mod = 1000000007;
fscanf(STDIN, "%d", $n);

$boxes = array_map('intval', explode(" ", trim(fgets(STDIN, 10240000))));
sort($boxes);

$result = "1";
for($i = 0; $i < $n; ++$i) {
  $result = bcmod(bcmul($result, $boxes[$i] - $i), $mod);
}

fprintf(STDOUT, "%d\n", $result);
