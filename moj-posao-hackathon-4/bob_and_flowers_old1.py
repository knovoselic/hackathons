DEBUG = True
# DEBUG = False
MAT = True

x1, y1, x2, y2 = list(map(int, input().strip().split()))
solution = 0

if DEBUG:
  print(x1, y1, x2, y2)

# find square inside of given rectangle and calculate number
# of bee hives inside it
a = max(x1, y1)
while a % 5 != 1: a += 1
if DEBUG: print(a)

b = min(x2, y2)
while b % 5 != 1: b -= 1
if DEBUG: print(b)

size = b - a - 1
if size < 3:
  solution = 0
else:
  solution = int(size**2/5)
if DEBUG: print('size', size, 'solution', solution)

if MAT:
  n = 40
  mat = [['.' for x in range(n)] for y in range(n)]

  for i in range(n):
    for j in range(n):
      mat[i][j] = str(min(i + 1, j + 1))

  for i in range(y1 - 1, y2):
    for j in range(x1 - 1, x2):
      mat[i][j] = '#'

  for i in range(a - 1, b):
    for j in range(a - 1, b):
      mat[i][j] = '+'

  for i in range(y1 - 1, y2):
    for j in range(x1 - 1, a - 1):
      mat[i][j] = '-'

  for i in range(y1 - 1, a - 1):
    for j in range(a - 1, x2):
      mat[i][j] = '.'

  for i in range(a - 1, y2):
    for j in range(b, x2):
      mat[i][j] = '='

  for i in range(b, y2):
    for j in range(a - 1, b):
      mat[i][j] = ','


# for i in range(y1 - 1, y2):
#   for j in range(x1 - 1, a - 1):
#     if min(i, j) % 5 == 4: solution += 1

# for i in range(y1 - 1, a - 1):
#   for j in range(a - 1, x2):
#     if min(i, j) % 5 == 4: solution += 1

# for i in range(a - 1, y2):
#   for j in range(b, x2):
#     if min(i, j) % 5 == 4: solution += 1

# for i in range(b, y2):
#   for j in range(a - 1, b):
#     if min(i, j) % 5 == 4: solution += 1

# if x1 == y1 and x1 % 5 == 0:
#   solution += x2 - x1 + 1
#   y1 += 1

# while True:
#   is_five = min(x1, y1) % 5 == 0;

#   if x1 == x2 and y1 == y2:
#     if is_five: solution += 1
#     break

#   if x1 < x2 and y1 < y2:
#     if x1 < y1:
#       dir = 'r'
#     else:
#       dir = 'd'
#   elif x1 < x2:
#     dir = 'r'
#   elif y1 < y2:
#     dir = 'd'


#   if dir == 'r':
#     if is_five: solution += x2 - x1;
#     if DEBUG: mat[y1-1][x1-1] = '>'
#     x1 += 1
#   else:
#     if is_five: solution += y2 - y1;
#     if DEBUG: mat[y1-1][x1-1] = 'v'
#     y1 += 1

  # if DEBUG:
  #   print(solution)
  #   print()
  #   print(x, y, x2, y2)


if MAT:
  for i in range(n):
    for j in range(n):
      print(mat[i][j].rjust(3, ' '), end = '')
    print()


print(solution)