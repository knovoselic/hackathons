x1, y1, x2, y2 = list(map(int, input().strip().split()))

def solve_rect(width, row):
  if width < 5: return 0
  precalc = [(width - 4) * int(i / 5) + i % 5 for i in range(5, 10)]
  valid_until = width
  while valid_until % 5 != 4: valid_until += 1

  if row <= valid_until:
    return precalc[row % 5] * int(row / 5)
  else:
    return precalc[valid_until % 5] * int(valid_until / 5) + (row - valid_until) * int(width / 5)

# herc
# def hives(x, y):
#   mxy = min(x,y)/5
#   return (x+y+1 - (mxy+1)*5)*mxy

print(solve_rect(x2, y2) - solve_rect(x2, y1 - 1) - solve_rect(x1 - 1, y2) + solve_rect(x1 - 1, y1 - 1))
