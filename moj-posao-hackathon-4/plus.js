var n = Number(readline().trim())
var m = Number(readline().trim())

var mat = Array(n);
for(var i = 0; i < n; ++i) {
  mat[i] = Array(n);
  for(var j = 0; j < n; ++j) {
    mat[i][j] = '.';
  }
}

for(var i = 0; i < m; ++i) {
  [x, y, c] = readline().trim().split(" ");
  x = Number(x) - 1;
  y = Number(y) - 1;
  for(var j = 0; j < n; ++j) {
    if (mat[j][y] < c) {
      mat[j][y] = c;
    }
    if (mat[x][j] < c) {
      mat[x][j] = c;
    }
  }
}

for(var x = 0; x < n; ++x) {
  for(var y = 0; y < n; ++y) {
    putstr(mat[x][y]);
  }
  print();
}
