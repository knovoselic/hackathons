import java.io.*;
import java.util.*;

public class Main {
  public static final long mod = 1000000007;

  public static void main(String[] args) {
    int n;
    Scanner in = new Scanner(System.in);

    n = in.nextInt();

    List<Long> boxes = new ArrayList<Long>();

    for(int i = 0; i < n; ++i) {
      boxes.add(in.nextLong());
    }

    Collections.sort(boxes);

    Long result = 1l;
    for(int i = 0; i < n; ++i) {
      result = ((result % mod) * ((boxes.get(i) - i) % mod)) % mod;
    }

    System.out.println(result);
  }
}
