n = int(input())
str = input().strip()

sum = 0
for c in str:
    sum += ord(c) - ord('a')

print(sum)