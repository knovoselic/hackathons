for(var createArray=function(n){for(var r=new Array(n),t=-1;++t<n;)r[t]=0;return r},epsilon=2/9007199254740992;1+epsilon/2!=1;)epsilon/=2;for(var BASE=2/epsilon,s=134217728;s*s<2/epsilon;)s*=2;var SPLIT=s+1,fma=function(n,r,t){var e=SPLIT*n,i=e-(e-n),a=n-i,g=SPLIT*r,u=g-(g-r),f=r-u;return i*u+t+i*f+a*u+a*f},fastTrunc=function(n){var r=n-BASE+BASE;return r>n?r-1:r},performMultiplication=function(n,r,t){var e=r*t,i=fma(r,t,-e),a=fastTrunc(e/BASE),g=e-a*BASE+i;return g<0&&(g+=BASE,a-=1),(g+=n-BASE)<0?g+=BASE:a+=1,{lo:g,hi:a}},performDivision=function(n,r,t){if(n>=t)throw new RangeError;var e=n*BASE,i=fastTrunc(e/t),a=0-fma(i,t,-e);a<0&&(i-=1,a+=t),(a+=r-t)<0?a+=t:i+=1;var g=fastTrunc(a/t);return{q:i+=g,r:a-=g*t}};function BigIntegerInternal(n,r,t){this.sign=n,this.magnitude=r,this.length=t}var createBigInteger=function(n,r,t){return new BigIntegerInternal(n,r,t)},fromNumber=function(n){if(n>=BASE||0-n>=BASE)throw new RangeError;var r=createArray(1);return r[0]=n<0?0-n:0+n,createBigInteger(n<0?1:0,r,0===n?0:1)};BigIntegerInternal.BigInt=function(n){if("number"==typeof n)return fromNumber(n);throw new RangeError},BigIntegerInternal.toNumber=function(n){if(0===n.length)return 0;if(1===n.length)return 1===n.sign?0-n.magnitude[0]:n.magnitude[0];if(BASE+1!==BASE)throw new RangeError;for(var r=n.magnitude[n.length-1],t=n.magnitude[n.length-2],e=n.length-3;e>=0&&0===n.magnitude[e];)e-=1;e>=0&&t%2==1&&(t+=1);var i=(r*BASE+t)*Math.pow(BASE,n.length-2);return 1===n.sign?0-i:i};var compareMagnitude=function(n,r){if(n.length!==r.length)return n.length<r.length?-1:1;for(var t=n.length;--t>=0;)if(n.magnitude[t]!==r.magnitude[t])return n.magnitude[t]<r.magnitude[t]?-1:1;return 0},addAndSubtract=function(n,r,t){var e=compareMagnitude(n,r),i=e<0?0!==t?1-r.sign:r.sign:n.sign,a=e<0?n:r,g=e<0?r:n;if(0===a.length)return createBigInteger(i,g.magnitude,g.length);var u=0,f=g.length;if(n.sign!==(0!==t?1-r.sign:r.sign)){if(u=1,a.length===f)for(;f>0&&a.magnitude[f-1]===g.magnitude[f-1];)f-=1;if(0===f)return createBigInteger(0,createArray(0),0)}for(var o=createArray(f+(1-u)),l=-1,c=0;++l<f;){var v=l<a.length?a.magnitude[l]:0;(c+=g.magnitude[l]+(0!==u?0-v:v-BASE))<0?(o[l]=BASE+c,c=0-u):(o[l]=c,c=1-u)}if(0===u)o[f]=c,f+=0!==c?1:0;else for(;f>0&&0===o[f-1];)f-=1;return createBigInteger(i,o,f)};BigIntegerInternal.add=function(n,r){return addAndSubtract(n,r,0)},BigIntegerInternal.subtract=function(n,r){return addAndSubtract(n,r,1)},BigIntegerInternal.multiply=function(n,r){if(0===n.length||0===r.length)return createBigInteger(0,createArray(0),0);if(1===n.length&&1===n.magnitude[0])return createBigInteger(1===n.sign?1-r.sign:r.sign,r.magnitude,r.length);if(1===r.length&&1===r.magnitude[0])return createBigInteger(1===n.sign?1-r.sign:r.sign,n.magnitude,n.length);for(var t=1===n.sign?1-r.sign:r.sign,e=n.length+r.length,i=createArray(e),a=-1;++a<r.length;)if(0!==r.magnitude[a]){for(var g=0,u=-1;++u<n.length;){var f=0;(g+=i[u+a]-BASE)>=0?f=1:g+=BASE;var o=performMultiplication(g,n.magnitude[u],r.magnitude[a]),l=o.lo,c=o.hi;i[u+a]=l,g=c+f}i[n.length+a]=g}for(;e>0&&0===i[e-1];)e-=1;return createBigInteger(t,i,e)},BigIntegerInternal.prototype.toString=function(n){if(null==n&&(n=10),10!==n&&(n<2||n>36||n!==Math.floor(n)))throw new RangeError("radix argument must be an integer between 2 and 36");var r=this,t=1===r.sign?"-":"",e=r.length;if(0===e)return"0";if(1===e)return t+=r.magnitude[0].toString(n);for(var i=0,a=1,g=fastTrunc(BASE/n);a<=g;)i+=1,a*=n;if(a*n<=BASE)throw new RangeError;for(var u=e+Math.floor((e-1)/i)+1,f=createArray(u),o=-1;++o<e;)f[o]=r.magnitude[o];for(var l=u;0!==e;){for(var c=0,v=e;--v>=0;){var B=performDivision(c,f[v],a),I=B.q,s=B.r;f[v]=I,c=s}for(;e>0&&0===f[e-1];)e-=1;f[l-=1]=c}for(t+=f[l].toString(n);++l<u;){for(var d=f[l].toString(n),m=i-d.length;--m>=0;)t+="0";t+=d}return t};var n=function(n){return function(r,t){return n(r,t)}},Internal="undefined"!=typeof BigInt&&BigInt(9007199254740991)+BigInt(2)-BigInt(2)===BigInt(9007199254740991)?BigIntWrapper:BigIntegerInternal,valueOf=function(n){return"number"==typeof n?Internal.BigInt(n):n},toResult=function(n){var r=Internal.toNumber(n);return r>=-9007199254740991&&r<=9007199254740991?r:n},add=n(function(n,r){var t=valueOf(n),e=valueOf(r);return toResult(Internal.add(t,e))}),subtract=n(function(n,r){var t=valueOf(n),e=valueOf(r);return toResult(Internal.subtract(t,e))}),multiply=n(function(n,r){if(n===r){var t=valueOf(n);return Internal.multiply(t,t)}var e=valueOf(n),i=valueOf(r);return toResult(Internal.multiply(e,i))}),divide=n(function(n,r){var t=valueOf(n),e=valueOf(r);return toResult(Internal.divide(t,e))});function BigInteger(){}BigInteger.BigInt=function(n){if("number"==typeof n)return n;var r=0+Number(n);return r>=-9007199254740991&&r<=9007199254740991?r:Internal.BigInt(n)};

[x1, y1, x2, y2] = readline().trim().split(" ").map(Number);

function solve_rect(width, row) {
  if (width < 5) return 0;

  precalc = [];
  for(var i = 5; i < 10; i++) {
    precalc[i - 5] = (width - 4) * Math.floor(i / 5) + i % 5;
  }

  var valid_until = width;
  while (valid_until % 5 != 4) valid_until += 1;

  if (row <= valid_until) {
    var a = BigInteger.BigInt(precalc[row % 5]);
    var b = BigInteger.BigInt(Math.floor(row / 5));
    return  multiply(a, b);
  } else {
    var a = BigInteger.BigInt(precalc[valid_until % 5]);
    var b = BigInteger.BigInt(Math.floor(valid_until / 5));
    var c = multiply(a, b);

    var d = BigInteger.BigInt(row - valid_until);
    var e = BigInteger.BigInt(Math.floor(width / 5));
    var f = multiply(d, e);

    return add(c, f);
  }
}

var a = subtract(solve_rect(x2, y2), solve_rect(x2, y1 - 1));
var b = subtract(a, solve_rect(x1 - 1, y2));

print(add(b, solve_rect(x1 - 1, y1 - 1)));
