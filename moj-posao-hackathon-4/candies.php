<?php

fscanf(STDIN, "%d", $n);
fscanf(STDIN, "%d", $m);

$a = array_fill(0, $n + 2, 0);

for($j = 0; $j < $m; ++$j) {
  fscanf(STDIN, "%d %d %d", $x, $y, $z);
  $a[$x] += $z;
  $a[$y + 1] -= $z;
}

$last = $a[1];
print($a[1]);
print(" ");

for($i = 2; $i < $n; ++$i) {
  $last += $a[$i];
  print($last);
  print(" ");
}
$last += $a[$n];
print($last);
print("\n");

?>