<?php
fscanf(STDIN, "%d %d %d", $n, $r, $l);

$layout = trim(fgets(STDIN, 10240000));

$solution = 99999;
$stack = [[strpos($layout, 'F'), 0]];

// layout = layout.split('');

while(count($stack) > 0) {
    $temp = array_pop($stack);
    $current = $temp[0];
    $jumps = $temp[1];
    $layout[$current] = '+';

    if ($current - $l < 0) {
        $solution = min($solution, $jumps);
    } else if ($layout[$current - $l] == '#') {
        array_push($stack, [$current - $l, $jumps + 1]);
    }

    if ($current + $r >= $n) {
        $solution = min($solution, $jumps);
    } else if ($layout[$current + $r] == '#') {
        array_push($stack, [$current + $r, $jumps + 1]);
    }
}

if ($solution == 99999) {
    print('NO');
} else {
    print($solution + 1);
}
print("\n");

?>