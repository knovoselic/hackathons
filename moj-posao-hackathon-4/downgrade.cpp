#include <cstdio>

int main(void) {
  int n;
  char c;
  int sum = 0;

  scanf("%d\n", &n);


  for(int i = 0; i < n; ++i) {
    scanf("%c", &c);
    sum += c - 97;
  }

  printf("%d\n", sum);
  return 0;
}
