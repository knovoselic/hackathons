n = Number(readline())
mat = []
memo = []

for(var i = 0; i < n; ++i) {
  mat[i] = readline().trim().split(" ").map(Number);
  memo[i] = [];
  for(var j = 0; j < n; ++j) {
    memo[i][j] = -10000;
  }
}

memo[n - 1][0] = mat[n - 1][0]

function solve(x, y) {
  if (x >= n || y < 0) {
    return -100;
  }

  if (memo[x][y] != -10000) {
    return memo[x][y];
  }

  result = Math.max(
    mat[x][y] + solve(x + 1, y),
    mat[x][y] + solve(x + 2, y),
    mat[x][y] + solve(x, y - 1),
    mat[x][y] + solve(x, y - 2)
  )

  memo[x][y] = result
  return result
}


solve(0, n - 1)
var solution = -10000;
for(var i = 0; i < n; ++i) {
  for(var j = 0; j < n; ++j) {
    solution = Math.max(solution, memo[i][j]);
  }
}
print(solution);
