n = int(input())
m = int(input())

a = [0] * (n + 2)

ranges = []

for j in range(m):
  x, y, z = list(map(int, input().strip().split()))
  a[x] += z
  a[y+1] -= z

last = a[1]
print(a[1], end = ' ')

for i in range(2, n):
  last += a[i]
  print(last, end = ' ')

last += a[n]
print(last)