import java.io.*;
import java.util.*;

public class Main {
  public static void main(String[] args) {
    Scanner in = new Scanner(System.in);

    int n = in.nextInt();
    int m = in.nextInt();

    int[] a = new int[n + 2];

    for(int i = 0; i < n + 2; ++i) {
      a[i] = 0;
    }

    for(int j = 0; j < m; ++j) {
      int x = in.nextInt();
      int y = in.nextInt();
      int z = in.nextInt();
      a[x] += z;
      a[y + 1] -= z;
    }

    for(int i = 0; i < n + 2; ++i) {
      System.out.print(a[i]);
      System.out.print(" ");
    }
      System.out.println(" ");

    int last = a[1];
    System.out.print(last);
    System.out.print(" ");
    for(int i = 2; i < n; ++i) {
      last += a[i];
      System.out.print(last);
      System.out.print(" ");
    }
    last += a[n];
    System.out.println(last);
  }
}
