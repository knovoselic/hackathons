import java.io.*;
import java.util.*;

public class Main {
  public static void main(String[] args) {
    Scanner in = new Scanner(System.in);
    StringBuilder sb = new StringBuilder();

    int n = in.nextInt();
    int m = in.nextInt();

    int a[] = new int[n + 2];

    for(int i = 0; i < m; ++i) {
      int x = in.nextInt();
      int y = in.nextInt();
      int z = in.nextInt();
      a[x] += z;
      a[y + 1] -= z;
    }

    int last = 0;
    for(int i = 1; i < n; ++i) {
      last += a[i];
      sb.append(last);
      sb.append(" ");
    }

    last += a[n];
    sb.append(last);
    System.out.println(sb.toString());
  }
}
