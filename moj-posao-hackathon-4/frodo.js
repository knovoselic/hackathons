[n, r, l] = readline().trim().split(" ").map(Number);
var layout = readline().trim();

var solution = 99999;
var stack = [[layout.indexOf('F'), 0]];

layout = layout.split('');

while(stack.length > 0) {
    [current, jumps] = stack.pop();
    layout[current] = '+';

    if (current - l < 0) {
        solution = Math.min(solution, jumps);
    } else if (layout[current - l] == '#') {
        stack.push([current - l, jumps + 1]);
    }

    if (current + r >= n) {
        solution = Math.min(solution, jumps);
    } else if (layout[current + r] == '#') {
        stack.push([current + r, jumps +1]);
    }
}

if (solution == 99999) {
    print('NO');
} else {
    print(solution + 1);
}
