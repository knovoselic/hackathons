import java.io.*;
import java.util.*;

public class Main {
  public static long solve_rect(long width, long row) {
    if (width < 5) return 0;

    long[] precalc = new long[5];
    for(int i = 5; i < 10; i++) {
      precalc[i - 5] = (width - 4) * (i / 5) + i % 5;
    }

    long valid_until = width;
    while (valid_until % 5 != 4) valid_until += 1;

    if (row <= valid_until) {
      return precalc[(int)(row % 5)] * (row / 5);
    } else {
      return precalc[(int)(valid_until % 5)] * (valid_until / 5) + (row - valid_until) * (width / 5);
    }
  }

  public static void main(String[] args) {
    long x1, y1, x2, y2;
    Scanner in = new Scanner(System.in);

    x1 = in.nextLong();
    y1 = in.nextLong();
    x2 = in.nextLong();
    y2 = in.nextLong();

    System.out.println(solve_rect(x2, y2) - solve_rect(x2, y1 - 1) - solve_rect(x1 - 1, y2) + solve_rect(x1 - 1, y1 - 1));
  }
}
