#include <cstdio>
#include <cstring>

int main(void) {
  int n, m;
  scanf("%d %d", &n, &m);

  char mat[n][n];

  memset(mat, '.', sizeof(mat));

  for(int i = 0; i < m; ++i) {
    int x, y;
    char c;

    scanf("%d %d %c", &x, &y, &c);

    --x;
    --y;

    for(int j = 0; j < n; ++j) {
      if (mat[j][y] < c) {
        mat[j][y] = c;
      }
      if (mat[x][j] < c) {
        mat[x][j] = c;
      }
    }
  }

  for(int x = 0; x < n ; ++x) {
    for(int y = 0; y < n; ++y) {
      printf("%c", mat[x][y]);
    }
    printf("\n");
  }

  return 0;
}
