#include <cstdio>

unsigned long long solve_rect(long long width, long long row) {
  if (width < 5) return 0;

  unsigned long long precalc[5] = {0};
  for(int i = 5; i < 10; i++) {
    precalc[i - 5] = (width - 4) * (i / 5) + i % 5;
  }

  long valid_until = width;
  while (valid_until % 5 != 4) valid_until += 1;

  if (row <= valid_until) {
    return precalc[(int)(row % 5)] * (row / 5);
  } else {
    return precalc[(int)(valid_until % 5)] * (valid_until / 5) + (row - valid_until) * (width / 5);
  }
}

int main(void) {
  unsigned long long x1, y1, x2, y2;
  scanf("%llu %llu %llu %llu", &x1, &y1, &x2, &y2);

  printf("%llu\n", solve_rect(x2, y2) - solve_rect(x2, y1 - 1) - solve_rect(x1 - 1, y2) + solve_rect(x1 - 1, y1 - 1));

  return 0;
}
