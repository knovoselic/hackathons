<?php

fscanf(STDIN, "%d", $n);

$mat = [];
$memo = [];

for($i = 0; $i < $n; ++$i) {
  $mat[$i] = array_map('intval', explode(" ", trim(fgets(STDIN, 10240000))));
  $memo[$i] = array_fill(0, $n, -10000);
}

$memo[$n - 1][0] = $mat[$n - 1][0];

function solve($x, $y) {
  global $n, $memo, $mat;

  if ($x >= $n || $y < 0) {
    return -100;
  }

  if ($memo[$x][$y] != -10000) {
    return $memo[$x][$y];
  }

  $result = max(
    $mat[$x][$y] + solve($x + 1, $y),
    $mat[$x][$y] + solve($x + 2, $y),
    $mat[$x][$y] + solve($x, $y - 1),
    $mat[$x][$y] + solve($x, $y - 2)
  );

  $memo[$x][$y] = $result;
  return $result;
}


solve(0, $n - 1);
$solution = -10000;
for($i = 0; $i < $n; ++$i) {
  for($j = 0; $j < $n; ++$j) {
    $solution = max($solution, $memo[$i][$j]);
  }
}
print($solution);
print("\n");
?>