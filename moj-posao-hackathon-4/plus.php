<?php
fscanf(STDIN, "%d", $n);
fscanf(STDIN, "%d", $m);

$mat = [];
for($i = 0; $i < $n; ++$i) {
  $mat[$i] = array_fill(0, $n, '.');
}

for($i = 0; $i < $m; ++$i) {
  fscanf(STDIN, "%d %d %s", $x, $y, $c);
  --$x;
  --$y;
  for($j = 0; $j < $n; ++$j) {
    if ($mat[$j][$y] < $c) {
      $mat[$j][$y] = $c;
    }
    if ($mat[$x][$j] < $c) {
      $mat[$x][$j] = $c;
    }
  }
}

for($x = 0; $x < $n; ++$x) {
  for($y = 0; $y < $n; ++$y) {
    print($mat[$x][$y]);
  }
  print("\n");
}
?>