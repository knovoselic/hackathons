import java.io.*;
import java.util.*;

public class Main {
  public static int mat[][];
  public static int memo[][];
  public static int n;

  public static int solve(int x, int y) {
    if (x >= n || y < 0) {
      return -100;
    }

    if (memo[x][y] != -10000) {
      return memo[x][y];
    }

    int result = Math.max(
      Math.max(mat[x][y] + solve(x + 1, y), mat[x][y] + solve(x + 2, y)),
      Math.max(mat[x][y] + solve(x, y - 1), mat[x][y] + solve(x, y - 2))
    );

    memo[x][y] = result;
    return result;
  }

  public static void main(String[] args) {
    Scanner in = new Scanner(System.in);

    n = in.nextInt();

    mat = new int[n][n];
    memo = new int[n][n];

    for(int i = 0; i < n; ++i) {
      for(int j = 0; j < n; ++j) {
        mat[i][j] = in.nextInt();
        memo[i][j] = -10000;
      }
    }

    memo[n - 1][0] = mat[n - 1][0];

    solve(0, n - 1);
    int solution = -10000;
    for(int i = 0; i < n; ++i) {
      for(int j = 0; j < n; ++j) {
        solution = Math.max(solution, memo[i][j]);
      }
    }
    System.out.println(solution);

  }
}
