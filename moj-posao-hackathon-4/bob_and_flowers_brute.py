c = 1
if c == 1:
  x1, y1, x2, y2 = list(map(int, input().strip().split()))
  # x1, y1, x2, y2 = 11, 11, 16, 16
  solution = 0


  for y in range(y1, y2 + 1):
    for x in range(x1, x2 + 1):
      if min(x, y) % 5 == 0: solution += 1


  print(x1, y1, x2, y2, ' = ', solution)
  # x1, y1, x2, y2 = x1 - 5, y1 - 5, x2 - 5, y2 - 5
  # solution = 0


  # for y in range(y1, y2 + 1):
  #   for x in range(x1, x2 + 1):
  #     if min(x, y) % 5 == 0: solution += 1


  # if solution != 0:  print(x1, y1, x2, y2, ' = ', solution)
elif c == 2:
  x, y, x2, y2 = 1, 1, 50, 50

  n = 20

  mat = [['.' for x in range(x2)] for y in range(y2)]

  for i in range(y2):
    for j in range(x2):
      mat[i][j] = min(i + 1, j + 1)
      if mat[i][j] % 5 == 0: mat[i][j] = '#'


  for i in range(y2):
    for j in range(x2):
      print(str(mat[i][j]).rjust(3, ' '), end = '')
    print()
elif c == 3 :
  x1, y1, x2, y2 = 1, 1, 10, 10
  for i in range(30):
    x2, y2 = i, i
    solution = 0

    for y in range(y1, y2 + 1):
      for x in range(x1, x2 + 1):
        if min(x, y) % 5 == 0: solution += 1

    print(x1, y1, x2, y2, ' = ', solution)
