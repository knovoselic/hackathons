<?php

fscanf(STDIN, "%d", $n);

$str = trim(fgets(STDIN, 10240000));

$sum = 0;
for($i = 0; $i < strlen($str); ++$i) {
  $sum += ord($str[$i]) - 97;
}

print($sum);
print("\n");
?>