n = int(input())
m = int(input())

mat = [['.' for x in range(n)] for y in range(n)]

for i in range(m):
  x, y, c = list(input().strip().split())
  x = int(x) - 1
  y = int(y) - 1

  for j in range(0, n):
    if mat[j][y] < c:
      mat[j][y] = c
    if mat[x][j] < c:
      mat[x][j] = c


for x in range(n):
  for y in range(n):
    print(mat[x][y], end = '')
  print()