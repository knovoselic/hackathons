DEBUG = True
# DEBUG = False
MAT = False

x1, y1, x2, y2 = list(map(int, input().strip().split()))
solution = 0

if DEBUG:
  print(x1, y1, x2, y2)

n = 11
if MAT:
  mat = [['.' for x in range(n)] for y in range(n)]

  for i in range(n):
    for j in range(n):
      mat[i][j] = min(i + 1, j + 1)
      if mat[i][j] % 5 == 0: mat[i][j] = '#'

  mat[y1 - 2][x1 - 2] = '+'
  mat[y1 - 2][x2] = '+'
  mat[y2][x1 - 2] = '+'
  mat[y2][x2] = '+'
  for j in range(x1 - 1, x2):
    mat[y1 - 2][j] = '-'
    mat[y2][j] = '-'

  for i in range(y1 - 1, y2):
    mat[i][x1 - 2] = '|'
    mat[i][x2] = '|'

# for i in range(1, n + 1):
#   count = 0
#   for j in range(1, n + 1):
#     if min(i, j) % 5 == 0: count += 1
#   print("Row", i, "count", count)
precalc = [(n - 4) * int(i / 5) + i % 5 for i in range(5, 10)]
print(precalc)
count = 0
for i in range(1, 22):
  for j in range(1, n + 1):
    if min(i, j) % 5 == 0: count += 1
  instacount = precalc[i % 5] * int(i / 5)
  print("Row", i, "rowcount", count, "instacount", instacount)
  # for i in range(y1 - 1, y2):
  #   for j in range(x1 - 1, x2):
  #     mat[i][j] = '#'

  # for i in range(a - 1, b):
  #   for j in range(a - 1, b):
  #     mat[i][j] = '+'

  # for i in range(y1 - 1, y2):
  #   for j in range(x1 - 1, a - 1):
  #     mat[i][j] = '-'

  # for i in range(y1 - 1, a - 1):
  #   for j in range(a - 1, x2):
  #     mat[i][j] = '.'

  # for i in range(a - 1, y2):
  #   for j in range(b, x2):
  #     mat[i][j] = '='

  # for i in range(b, y2):
  #   for j in range(a - 1, b):
  #     mat[i][j] = ','


if MAT:
  for i in range(n):
    for j in range(n):
      print(str(mat[i][j]).rjust(3, ' '), end = '')
    print()


print(solution)