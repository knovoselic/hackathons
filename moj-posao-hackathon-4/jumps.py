MOD = 1000000007

n = int(input())
boxes = list(map(int, input().strip().split()))

list.sort(boxes)

result = 1
for i, n in enumerate(boxes):
  result = (result * (n - i)) % MOD

print(result % MOD)
