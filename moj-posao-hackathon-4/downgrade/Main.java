import java.io.*;
import java.util.*;

public class Main {
  public static void main(String[] args) {
    Scanner in = new Scanner(System.in);

    int n = in.nextInt();
    String str = in.next().trim();

    int sum = 0;
    for(int i = 0; i < n; ++i) {
      sum += (int)str.charAt(i) - 97;
    }


    System.out.println(sum);
  }
}
