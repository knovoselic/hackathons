import java.io.*;
import java.util.*;

public class Main {
  public static class State {
    public int current, jumps;

    public State(int current, int jumps) {
        this.current = current;
        this.jumps = jumps;
    }
  }

  public static void main(String[] args) {
    Scanner in = new Scanner(System.in);

    int n = in.nextInt();
    int r = in.nextInt();
    int l = in.nextInt();
    int solution = 99999;

    String layoutStr = in.next().trim();

    Stack<State> stack = new Stack<State>();

    stack.push(new State(layoutStr.indexOf("F"), 0));

    char[] layout = layoutStr.toCharArray();

    while(!stack.empty()) {
        State state = stack.pop();
        layout[state.current] = '+';
        if (state.current - l < 0) {
            solution = Math.min(solution, state.jumps);
        } else if (layout[state.current - l] == '#') {
            stack.push(new State(state.current - l, state.jumps + 1));
        }

        if (state.current + r >= n) {
            solution = Math.min(solution, state.jumps);
        } else if (layout[state.current + r] == '#') {
            stack.push(new State(state.current + r, state.jumps + 1));
        }
    }

    if (solution == 99999) {
      System.out.println("NO");
    } else {
        System.out.println(solution + 1);
    }
  }
}
