#include <cstdio>

int main(void) {
  int n, m;
  scanf("%d %d", &n, &m);

  int a[n + 2] = {0};

  for(int j = 0; j < m; ++j) {
    int x, y, z;
    scanf("%d %d %d", &x, &y, &z);
    a[x] += z;
    a[y + 1] -= z;
  }

  unsigned long long last = a[1];
  printf("%lld ", last);
  for(int i = 2; i < n; ++i) {
    last += a[i];
    printf("%lld ", last);
  }
  last += a[n];
  printf("%lld\n", last);
  return 0;
}
