# DEBUG = True
DEBUG = False

x, y, x2, y2 = list(map(int, input().strip().split()))
solution = 0

if DEBUG:
  print(x, y, x2, y2)

n = 20

mat = [['.' for x in range(n)] for y in range(n)]

for i in range(15):
  for j in range(15):
    mat[i][j] = str(min(i + 1, j + 1))

if x == y and x % 5 == 0:
  solution += x2 - x + 1
  y += 1

while True:
  is_five = min(x, y) % 5 == 0;

  if x == x2 and y == y2:
    if is_five: solution += 1
    break

  if x < x2 and y < y2:
    if x < y:
      dir = 'r'
    else:
      dir = 'd'
  elif x < x2:
    dir = 'r'
  elif y < y2:
    dir = 'd'


  if dir == 'r':
    if is_five: solution += x2 - x;
    if DEBUG: mat[y-1][x-1] = '>'
    x += 1
  else:
    if is_five: solution += y2 - y;
    if DEBUG: mat[y-1][x-1] = 'v'
    y += 1

  if DEBUG:
    print(solution)
    print()
    print(x, y, x2, y2)


if DEBUG:
  for i in range(15):
    for j in range(15):
      print(mat[i][j].rjust(3, ' '), end = '')
    print()


print(solution)