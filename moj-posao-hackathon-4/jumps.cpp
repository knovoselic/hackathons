#include <cstdio>
#include <vector>
#include <algorithm>
#define mod 1000000007

int main(void) {
  unsigned long long int n, temp;
  scanf("%lld", &n);

  std::vector<int> boxes(n);
  for(unsigned int i = 0; i < n; ++i) {
    scanf("%lld", &temp);
    boxes[i] = temp;
  }

  std::sort(boxes.begin(), boxes.end());

  unsigned long long int result = 1;
  for(unsigned int i = 0; i < n; ++i) {
    result = ((result % mod) * ((boxes[i] - i) % mod)) % mod;
  }

  printf("%lld\n", result);

  return 0;
}
