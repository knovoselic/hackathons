n = 10**5
m = 10**5
a = Array.new(n) do
  x = rand 1..(n-10)
  y = rand x..n
  z = rand 1..10
  [x, y, z].join ' '
end

File.write 'candies_input12', "#{n}\n#{m}\n#{a.join("\n")}"