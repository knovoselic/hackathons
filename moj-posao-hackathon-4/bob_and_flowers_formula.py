width = 20
rows = 40

p = True
def solve_rect(width, row):
  global p
  if width < 5: return 0
  precalc = [(width - 4) * int(i / 5) + i % 5 for i in range(5, 10)]
  valid_until = width
  while valid_until % 5 != 4: valid_until += 1

  if p:
    print(valid_until)
    p = False

  if row <= valid_until:
    return precalc[row % 5] * int(row / 5)
  else:
    return precalc[valid_until % 5] * int(valid_until / 5) + (row - valid_until) * int(width / 5)



precalc = [(width - 4) * int(i / 5) + i % 5 for i in range(5, 10)]

print(precalc)

count = 0
for row in range(1, rows + 1):
  for j in range(1, width + 1):
    if min(row, j) % 5 == 0: count += 1
  print("Row", row, "rowcount", count, "solve_rect", solve_rect(j, row))

