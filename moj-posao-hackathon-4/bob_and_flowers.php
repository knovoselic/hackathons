<?php
fscanf(STDIN, "%d %d %d %d", $x1, $y1, $x2, $y2);

function solve_rect($width, $row) {
  if ($width < 5) return 0;

  $precalc = [];
  for($i = 5; $i < 10; $i++) {
    $precalc[$i - 5] = ($width - 4) * floor($i / 5) + $i % 5;
  }

  $valid_until = $width;
  while ($valid_until % 5 != 4) $valid_until += 1;

  if ($row <= $valid_until) {
    return bcmul($precalc[$row % 5], floor($row / 5));
  } else {
    return bcadd(
      bcmul($precalc[$valid_until % 5], floor($valid_until / 5)),
      bcmul(($row - $valid_until),floor($width / 5))
    );
  }
}

$temp = bcsub(solve_rect($x2, $y2), solve_rect($x2, $y1 - 1));
$temp = bcsub($temp, solve_rect($x1 - 1, $y2));
$temp = bcadd($temp, solve_rect($x1 - 1, $y1 - 1));

echo($temp);
echo("\n");
?>
