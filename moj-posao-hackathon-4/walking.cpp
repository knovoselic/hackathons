#include <cstdio>
#include <algorithm>

int mat[20][20] = {0};
int memo[20][20] = {0};
int n;

int solve(int x, int y) {
  if (x >= n || y < 0) {
    return -100;
  }

  if (memo[x][y] != -10000) {
    return memo[x][y];
  }

  int result = std::max(
    std::max(mat[x][y] + solve(x + 1, y), mat[x][y] + solve(x + 2, y)),
    std::max(mat[x][y] + solve(x, y - 1), mat[x][y] + solve(x, y - 2))
  );

  memo[x][y] = result;
  return result;
}

int main(void) {
  scanf("%d", &n);

  for(int i = 0; i < n; ++i) {
    for(int j = 0; j < n; ++j) {
      scanf("%d", &mat[i][j]);
      memo[i][j] = -10000;
    }
  }

  memo[n - 1][0] = mat[n - 1][0];

  solve(0, n - 1);
  int solution = -10000;
  for(int i = 0; i < n; ++i) {
    for(int j = 0; j < n; ++j) {
      solution = std::max(solution, memo[i][j]);
    }
  }
  printf("%d\n", solution);

  return 0;
}
