n = int(input())
mat = []

for i in range(n):
  mat.append(list(map(int, input().strip().split())))

memo = {}
memo[(n - 1, 0)] = mat[n - 1][0]

def solve(x, y):
  if x >= n or y < 0:
    return -100

  if (x,y) in memo:
    return memo[(x,y)]

  result = max(
    mat[x][y] + solve(x + 1, y),
    mat[x][y] + solve(x + 2, y),
    mat[x][y] + solve(x, y - 1),
    mat[x][y] + solve(x, y - 2)
  )

  memo[(x,y)] = result
  return result


solve(0, n - 1)
print(max(memo.values()))
