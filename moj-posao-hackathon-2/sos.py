numpad = [
    ['1', '2', '3'],
    ['4', '5', '6'],
    ['7', '8', '9'],
    ['\0', '0', 'C']
]

ci = 0
cj = 0
ni = 0
nj = 0

def find_next(needle):
    global ci
    global cj
    global ni
    global nj
    for i in range(4):
        for j in range(3):
            if numpad[i][j] == needle:
                ni = i
                nj = j
                return


number = input().strip() + 'C'
number_length = len(number)
solution = 0

find_next('0')
ci = ni
cj = nj

for c in range(number_length):
    find_next(number[c])
    distance = abs(ci - ni) + abs(cj - nj) + 1
    solution += distance
    ci = ni
    cj = nj


print(solution)
