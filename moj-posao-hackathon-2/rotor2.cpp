#include <cstdio>
#include <cstring>
#include <algorithm>
#include <vector>
#define DEBUG(x, ...)
// #define DEBUG(x, ...) printf(x, ##__VA_ARGS__)

using namespace std;

// 2n+1 because of padding with $
char board[1010][1010]; // # + 3 - 1 char for padding, 1 for endl (just to be sure) and 1 for NULL
int n, m;

int ispc = 0;

bool is_pal(int i, int j, int p, int q) {
  ispc++;
  DEBUG("i = %d, j = %d, p = %d, q = %d\n", i, j, p, q);
  int xmax = p - i + 1;
  int ymax = xmax / 2 + (xmax % 2);

  DEBUG("Xmax = %d, Ymax = %d\n", xmax, ymax);
  // for(int x = 0; x < xmax; ++x) {
  //   for(int y = 0; y < ymax; ++y) {
  //     DEBUG("Comparing %c == %c\n", board[i + y][j + x], board[p - y][q - x]);
  //     if (board[i + y][j + x] != board[p - y][q - x]) return false;
  //   }
  // }
  for(int r = 0; r < xmax; ++r) {
    DEBUG("Comparing %c == %c\n", board[i + r][j], board[p - r][q]);
    if (board[i + r][j] != board[p - r][q]) return false;
  }

  for(int c = 1; c < xmax - 1; ++c) {
    DEBUG("Comparing %c == %c\n", board[i][j + c], board[p][q - c]);
    if (board[i][j + c] != board[p][q - c]) return false;
  }

  return true;
}

int main(void) {
  // scanf("%d %d", &n, &m);
  // memset(board, '$', sizeof(board));
  // getc(stdin); // chomp new line
  // for(int i = 0; i < n; ++i) {
  //   for(int j = 0; j < m + 1; ++j) {
  //     board[i * 2 + 1][j * 2 + 1] = getc(stdin);
  //   }
  // }
  // for(int i = 0; i < n * 2 + 1; ++i) {
  //   for(int j = 0; j < m * 2 + 1; ++j) {
  //     printf("%c ", board[i][j]);
  //     // printf("%c ", board[i][j] == '$' ? ' ' : board[i][j]);
  //   }
  //   printf("\n");
  // }
  // n = (n << 1) + 1;
  // m =(m << 1) + 1;

  scanf("%d %d", &n, &m);
  memset(board, '$', sizeof(board));
  getc(stdin); // chomp new line
  for(int r = 1; r <= n; r++) {
    fscanf(stdin, "%s", (board[r] + 1));
    board[r][1 + m] = '$';
  }
  // for(int i = 0; i < n + 2; ++i) {
  //   for(int j = 0; j < m + 2; ++j) {
  //     printf("%c ", board[i][j]);
  //   }
  //   printf("\n");
  // }


  int max_pal = 0;
  int i;

  for(int d = n - 1; d >= 0; --d) {
    DEBUG("Processing d = %d\n", d);
    int max_center = 1;
    int pals_size = n - abs(d) + 1;
    vector<int> pals(pals_size, 0);
    pals[1] = 1;

    for(int j = 2; j < pals_size; ++j) {
      i = j + d;

      DEBUG("Processing (%d, %d) = %c\n", i, j, board[i][j]);
      if (max_center + pals[max_center] <= j) {
        DEBUG("Setting current pals to 1\n");
        pals[j] = 1;
      } else {
        pals[j] = std::min(pals[2 * max_center - j], max_center + pals[max_center] - j);
        DEBUG("Copying current pals to %d\n", pals[j]);
      }
      while((j + pals[j] <= n) &&
            (j - pals[j] >= 1) &&
            is_pal(i - pals[j], j - pals[j], i + pals[j], j + pals[j])
          ) {
        ++pals[j];
        DEBUG("Increased pals to %d\n", pals[j]);
      }

      if (j + pals[j] > max_center + pals[max_center]) {
        DEBUG("Max center was %d, now is %d\n", max_center, j);
        max_center = j;
      }

      DEBUG("Pals: ");
      for(unsigned int i = 0; i < pals.size(); ++i) {
        DEBUG("%d ", pals[i]);
      }
      DEBUG("\n");
      DEBUG("====================================================\n");
    }
    DEBUG("\n");
    // printf("d = %02d. Pals: ", d);
    for(unsigned int i = 0; i < pals.size(); ++i) {
    //   printf("%d ", pals[i]);
      if (pals[i] > max_pal) max_pal = pals[i];
    }
    // printf("\n");
  }

  for(int d = -1; d >= -m + 1; --d) {
    DEBUG("Processing d = %d\n", d);
    int max_center = 1;
    int pals_size = m - abs(d) + 1;
    vector<int> pals(pals_size, 0);
    pals[1] = 1;

    for(int j = 2; j < pals_size; ++j) {
      i = j - d;

      DEBUG("Processing (%d, %d) = %c\n", j, i, board[j][i]);
      if (max_center + pals[max_center] <= j) {
        DEBUG("Setting current pals to 1\n");
        pals[j] = 1;
      } else {
        pals[j] = std::min(pals[2 * max_center - j], max_center + pals[max_center] - j);
        DEBUG("Copying current pals to %d\n", pals[j]);
      }
      while((j + pals[j] <= n) &&
            (j - pals[j] >= 1) &&
            is_pal(j - pals[j], i - pals[j], j + pals[j], i + pals[j])
          ) {
        ++pals[j];
        DEBUG("Increased pals to %d\n", pals[j]);
      }

      if (j + pals[j] > max_center + pals[max_center]) {
        DEBUG("Max center was %d, now is %d\n", max_center, j);
        max_center = j;
      }

      DEBUG("Pals: ");
      for(unsigned int i = 0; i < pals.size(); ++i) {
        DEBUG("%d ", pals[i]);
      }
      DEBUG("\n");
      DEBUG("====================================================\n");
    }
    DEBUG("\n");
    // printf("d = %02d. Pals: ", d);
    for(unsigned int i = 0; i < pals.size(); ++i) {
      // printf("%d ", pals[i]);
      if (pals[i] > max_pal) max_pal = pals[i];
    }
    // printf("\n");
  }

  printf("%d\n", max_pal * 2 -1);
  // printf("%d\n", max_pal);
  // printf("%d\n", ispc);

  return 0;
}

// #include <cstdio>
// #include <cstring>
// #include <algorithm>
// #include <vector>
//
// using namespace std;
//
// char board[501][503]; // # + 3 - 1 char for padding, 1 for endl (just to be sure) and 1 for NULL
// int n, m;
//
// int lcp1(int i, int j, int p, int q) {
//   printf("i = %d, j = %d, p = %d, q = %d\n", i, j, p, q);
//   int ii = i;
//   int jj = j;
//   int pp = p;
//   int qq = q;
//   int lcp = 0;
//
//   while (pp >= 1 && jj <= n) {
//     if (board[pp][q] != board[i][jj]) break;
//
//     ++lcp;
//     --pp;
//     ++jj;
//   }
//
//   int old = lcp;
//   lcp = 0;
//
//   while (qq >= 1 && ii <= n) {
//     if (board[ii][j] != board[p][qq]) break;
//
//     ++lcp;
//     ++ii;
//     --qq;
//   }
//
//   printf("LCP1 is %d\n", std::min(old, lcp));
//   return std::min(old, lcp);
// }
//
// int lcp2(int i, int j, int p, int q) {
//   printf("i = %d, j = %d, p = %d, q = %d\n", i, j, p, q);
//   int ii = i;
//   int jj = j;
//   int pp = p;
//   int qq = q;
//   int lcp = 0;
//
//   while (pp <= n && jj <= n) {
//     if (board[pp][q] != board[i][jj]) break;
//
//     ++lcp;
//     ++pp;
//     ++jj;
//   }
//
//   int old = lcp;
//   lcp = 0;
//
//   while (ii >= 1 && q >= 1) {
//     if (board[ii][j] != board[p][qq]) break;
//
//     ++lcp;
//     --ii;
//     --qq;
//   }
//
//   printf("LCP2 is %d\n", std::min(old, lcp));
//   return std::min(old, lcp);
// }
//
// int main(void) {
//   scanf("%d %d", &n, &m);
//
//   memset(board, '$', sizeof(board));
//
//   getc(stdin); // chomp new line
//   for(int r = 1; r <= n; r++) {
//     fscanf(stdin, "%s", (board[r] + 1));
//   }
//
//   for(int i = 0; i < n + 1; ++i) {
//     for(int j = 0; j < m + 1; ++j) {
//       printf("%c ", board[i][j]);
//     }
//     printf("\n");
//   }
//
//   for(int d = n - 1; d >= -n + 1; --d) {
//     d = 0;
//     printf("Processing d = %d\n", d);
//     int max_center = 1;
//     int pals_size = n - abs(d) + 1;
//     vector<int> pals(pals_size, 0);
//     pals[1] = 1;
//
//     for(int j = 2; j < pals_size; ++j) {
//       int i = d + j;
//       printf("Processing (%d, %d) = %c\n", i, j, board[i][j]);
//       if (max_center + pals[max_center] <= j) {
//         printf("Setting current pals to 1\n");
//         pals[j] = 1;
//       } else {
//         pals[j] = std::min(pals.at(2 * max_center - j), max_center + pals[max_center] - j);
//         printf("Setting current pals to %d\n", pals[j]);
//       }
//       while((j + pals[i] < n) &&
//             (j - pals[i] > 1) &&
//             lcp1(i - pals[i] - 1, j - pals[i] - 1, i + pals[i] + 1, j + pals[i] + 1) >= 2 * pals[i] &&
//             lcp2(i + pals[i] + 1, j - pals[i] - 1, i - pals[i] - 1, j + pals[i] + 1) >= 2 * pals[i]
//           ) {
//         ++pals[j];
//       }
//
//       if (j + pals[i] > max_center + pals[max_center]) {
//         printf("Max center was %d, now is %d\n", max_center, j);
//         max_center = j;
//       }
//
//       printf("Pals: ");
//       for(unsigned int i = 0; i < pals.size(); ++i) {
//         printf("%d ", pals[i]);
//       }
//       printf("\n");
//       // printf("(%d, %d) ", i, j);
//     }
//     // printf("\n");
//     printf("Pals: ");
//     for(unsigned int i = 0; i < pals.size(); ++i) {
//       printf("%d ", pals[i]);
//     }
//     printf("\n");
//     break;
//
//   }
//
//   return 0;
// }
