import java.io.*;
import java.util.*;

public class Main {
  public static HashMap<Integer, Long> primes = new HashMap<Integer, Long>();

  public static void prime_factors(Long n) {
    while(n % 2 == 0) {
      primes.put(2, primes.getOrDefault(2, 0L) + 1);
      n /= 2;
    }

    for(int i = 3; i <= Math.sqrt(n); i += 2) {
      while (n%i == 0) {
        primes.put(i, primes.getOrDefault(i, 0L) + 1);
        n /= i;
      }
    }

    if (n > 1) primes.put(n.intValue(), primes.getOrDefault(n, 0L) + 1);;
  }

  public static void main(String[] args) {
    Scanner in = new Scanner(System.in);
    Long n, count = 1L;

    n = in.nextLong();

    prime_factors(n);
    Iterator it = primes.entrySet().iterator();
    while(it.hasNext()) {
      Map.Entry pair = (Map.Entry)it.next();
      count *= ((Long)pair.getValue() + 1);
    }

    System.out.println(count);
  }
}
