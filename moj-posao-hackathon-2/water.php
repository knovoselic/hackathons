<?php

$bottles = array();
$solution = 0;

fscanf(STDIN, "%d %d", $n, $x);
$bottles = array_map('intval', explode(" ", trim(fgets(STDIN, 102400))));

sort($bottles);
$i = 0;
while ($x > 0 && $i < $n) {
    $x -= $bottles[$i];
    if ($x < 0 ) break;
    ++$solution;
    ++$i;
}
fprintf(STDOUT, "%d\n", $solution);
?>
