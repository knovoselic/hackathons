import java.io.*;
import java.util.*;

public class Main {
  public static char input_string[];
  public static int n;

  public static int find_solution(char[] pattern) {
    int mem[] = {0, 0, 0, 0, 0, 0};

    for(int i = 0; i < n; i++) {
      char current = pattern[i % 3];
      if (input_string[i] == 'A' && current == 'B') {
        ++mem[0];
      } else if (input_string[i] == 'A' && current == 'C') {
        ++mem[1];
      } else if (input_string[i] == 'B' && current == 'A') {
        ++mem[2];
      } else if (input_string[i] == 'B' && current == 'C') {
        ++mem[3];
      } else if (input_string[i] == 'C' && current == 'A') {
        ++mem[4];
      } else if (input_string[i] == 'C' && current == 'B') {
        ++mem[5];
      }
    }

    return Math.min(mem[0], mem[2]) +
           Math.min(mem[1], mem[4]) +
           Math.min(mem[3], mem[5]) +
           Math.abs(mem[0] - mem[2]) * 2;
  }

  public static void main(String[] args) {
    Scanner in = new Scanner(System.in);
    int minimum;

    n = in.nextInt() * 3;
    input_string = in.next().toCharArray();
    minimum = n + 10;

    minimum = Math.min(minimum, find_solution("ABC".toCharArray()));
    minimum = Math.min(minimum, find_solution("ACB".toCharArray()));
    minimum = Math.min(minimum, find_solution("BAC".toCharArray()));
    minimum = Math.min(minimum, find_solution("BCA".toCharArray()));
    minimum = Math.min(minimum, find_solution("CAB".toCharArray()));
    minimum = Math.min(minimum, find_solution("CBA".toCharArray()));

    System.out.println(minimum);


  }
}
