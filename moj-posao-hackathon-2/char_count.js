var NUMBER_OF_LETTERS = 26;
var input;
var counts = [];
for(i = 0; i < NUMBER_OF_LETTERS; i++) counts[i] = 0;

while (input = readline()) {
  input = input.toLowerCase();
  for(i = 0; i < input.length; i++) {
    let index = input.charCodeAt(i) - 97;
    if (index < 0 || index > 25) continue;
    counts[index]++;
  }

  let firstPrint = true;
  for(i = 0; i < NUMBER_OF_LETTERS; i++) {
    if (counts[i] == 0) continue;
    if (!firstPrint) print();
    putstr(String.fromCharCode((i + 97)));
    putstr(":");
    putstr(counts[i]);
    firstPrint = false;
  }
}
