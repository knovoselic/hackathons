<?php

function find_solution($pattern) {
  global $n;
  global $input_string;

  $mem = [0, 0, 0, 0, 0, 0];

  for($i = 0; $i < $n; $i++) {
    $current = $pattern[$i % 3];
    if ($input_string[$i] == 'A' && $current == 'B') {
      ++$mem[0];
    } else if ($input_string[$i] == 'A' && $current == 'C') {
      ++$mem[1];
    } else if ($input_string[$i] == 'B' && $current == 'A') {
      ++$mem[2];
    } else if ($input_string[$i] == 'B' && $current == 'C') {
      ++$mem[3];
    } else if ($input_string[$i] == 'C' && $current == 'A') {
      ++$mem[4];
    } else if ($input_string[$i] == 'C' && $current == 'B') {
      ++$mem[5];
    }
  }

  return min($mem[0], $mem[2]) +
         min($mem[1], $mem[4]) +
         min($mem[3], $mem[5]) +
         abs($mem[0] - $mem[2]) * 2;
}

fscanf(STDIN, "%d", $n);
$input_string = str_split(trim(fgets(STDIN, 3000010)));

$n *= 3;
$minimum = $n + 10;

$minimum = min($minimum, find_solution("ABC"));
$minimum = min($minimum, find_solution("ACB"));
$minimum = min($minimum, find_solution("BAC"));
$minimum = min($minimum, find_solution("BCA"));
$minimum = min($minimum, find_solution("CAB"));
$minimum = min($minimum, find_solution("CBA"));

fprintf(STDOUT, "%d\n", $minimum);
