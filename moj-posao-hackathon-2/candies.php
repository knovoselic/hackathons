<?php

$primes = array();

function prime_factors($n) {
  global $primes;

  while(bcmod($n, 2) == 0) {
    if (!array_key_exists(2, $primes)) $primes[2] = 0;
    $primes[2]++;
    $n = bcdiv($n, 2);
  }

  for($i = 3; bccomp($i, bcsqrt($n)) <= 0; $i += 2) {
    while (bcmod($n, $i) == 0) {
      if (!array_key_exists($i, $primes)) $primes[$i] = 0;
      $primes[$i]++;
      $n = bcdiv($n, $i);
    }
  }

  if (bccomp($n, 2) == 1) {
    if (!array_key_exists($n, $primes)) $primes[$n] = 0;
    $primes[$n]++;
  }
}

$count = 1;

$n = trim(fgets(STDIN, 102400));

prime_factors($n);

foreach($primes as $value) {
  $count *= ($value + 1);
}

fprintf(STDOUT, "%d\n", $count);
?>
