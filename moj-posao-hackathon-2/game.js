function prime_factors(n) {
  var count = 0;

  while(n % 2 == 0) {
    ++count;
    n /= 2;
  }

  for(var i = 3; i <= Math.sqrt(n); i += 2) {
    while (n%i == 0) {
      ++count;
      n /= i;
    }
  }

  if (n > 2) ++count;

  return count;
}

var n;
var count = 0;
var number;

n = Number(readline());

var numbers = readline().trim().split(" ").map(Number);
for(var i = 0; i < n; i++) {
  count += prime_factors(numbers[i]);
}

if (count % 2 == 0) {
  print("Slavko");
} else {
  print("Mirko");
}
print(count);
