#include <iostream>
#include <string>
#include <queue>

using namespace std;

void print_padded(const string &line, const string &alignment, const int &w) {
  if (alignment == "right") {
    cout << string(w - line.length(), '.') << line << endl;
  } else if (alignment == "left") {
    cout << line << string(w - line.length(), '.') << endl;
  } else {
    int diff = w - line.length();
    int spaces = diff / 2;
    cout << string(spaces, '.') << line << string(spaces, '.');
    if (diff % 2 == 1) cout << ".";
    cout << endl;
  }
}

int main(void) {
  int w;
  string alignment;
  string temp;
  queue<string> words;

  cin >> w >> alignment;
  while (cin >> temp) {
    words.push(temp);
  }

  int current_width = 0;
  string line = "";

  while(words.size() > 0) {
    if (line == "") {
      line = words.front();
      words.pop();
    } else if (line.length() + words.front().length() + 1 <= w) {
      line = line + "." + words.front();
      words.pop();
    } else {
      print_padded(line, alignment, w);
      line = "";
    }
  }

  if (line != "") print_padded(line, alignment, w);
  return 0;
}
