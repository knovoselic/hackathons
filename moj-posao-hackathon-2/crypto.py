encoded = list(input().strip())

n = len(encoded)
start = 0
index = 0
step = 2
decoded = [' '] * (n + 1)

while start < n:
    i = start;
    while i < n:
        decoded[i] = encoded[index]
        index += 1
        i += step
    while decoded[start] != ' ': start += 1
    step = step << 1

print(''.join(decoded[:-1]))
