import java.io.*;
import java.util.*;

public class Main {

  public static void main(String[] args) {
    Scanner in = new Scanner(System.in);
    byte mem[][] = new byte[10000][10000];
    int n;
    int x1;
    int x2;
    int y1;
    int y2;
    long area = 0;


    n = in.nextInt();
    for(int i = 0; i < n; i++) {
      x1 = in.nextInt();
      y1 = in.nextInt();
      x2 = in.nextInt();
      y2 = in.nextInt();
      for(int x = x1; x < x2; x++) {
        for(int y = y1; y < y2; y++) {
          mem[x][y] = 1;
        }
      }
    }

    n = in.nextInt();
    for(int i = 0; i < n; i++) {
      x1 = in.nextInt();
      y1 = in.nextInt();
      x2 = in.nextInt();
      y2 = in.nextInt();
      for(int x = x1; x < x2; x++) {
        for(int y = y1; y < y2; y++) {
          mem[x][y] = 1;
        }
      }
    }

    for(int x = 0; x < 10000; x++) {
      for(int y = 0; y < 10000; y++) {
        if (mem[x][y] == 1) ++area;
      }
    }

    System.out.println(area);
  }
}
