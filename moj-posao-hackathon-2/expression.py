def burek(x):
    if x < 0:
        print('-', end='')
    else:
        print('+', end='')

a, b, c, d = [int(x) for x in input().strip().split(" ")]

burek(b);
burek(c);
burek(d);
print();
