def find_solution(pattern):
    global input_string
    global n

    mem = [0] * 6

    for i in range(n):
        current = pattern[i % 3]
        if (input_string[i] == 'A' and current == 'B'):
            mem[0] += 1
        elif (input_string[i] == 'A' and current == 'C'):
            mem[1] += 1;
        elif (input_string[i] == 'B' and current == 'A'):
            mem[2] += 1;
        elif (input_string[i] == 'B' and current == 'C'):
            mem[3] += 1;
        elif (input_string[i] == 'C' and current == 'A'):
            mem[4] += 1;
        elif (input_string[i] == 'C' and current == 'B'):
            mem[5] += 1;

    return min(mem[0], mem[2]) + min(mem[1], mem[4]) + min(mem[3], mem[5]) + abs(mem[0] - mem[2]) * 2

n = int(input().strip())
input_string = list(input().strip())

n *= 3
minimum = n + 10

minimum = min(minimum, find_solution("ABC"))
minimum = min(minimum, find_solution("ACB"))
minimum = min(minimum, find_solution("BAC"))
minimum = min(minimum, find_solution("BCA"))
minimum = min(minimum, find_solution("CAB"))
minimum = min(minimum, find_solution("CBA"))

print(minimum)
