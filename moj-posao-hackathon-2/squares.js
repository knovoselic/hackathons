  var world = readline().trim().split(" ").map(Number);
  var min_amount = 32768;


  for(var i = 0; i < 6; i++) {
    var min = 5;
    for(var j = 0; j < 5; j++) {
      if (world[i + j] < min) min = world[i + j];
    }
    var amount = 0;
    for(var j = 0; j < 5; j++) {
      amount += world[i + j] - min;
    }
    if (amount < min_amount) min_amount = amount;
  }
  print(min_amount);
