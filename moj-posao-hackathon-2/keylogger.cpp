#include <cstdio>
#include <cstring>

int main(void) {
  char buffer[200] = {0};
  char input[10];
  int lines;
  int i;
  int buffer_position = 0;
  bool caps_lock = false;

  scanf("%d\n", &lines);

  for(i = 0; i < lines; i++) {
    scanf(" %[^\n]s", input);
    if (strcmp("backspace", input) == 0) {
      if (buffer_position > 0) buffer[--buffer_position] = 0;
    } else if (strcmp("caps", input) == 0) {
      caps_lock = !caps_lock;
    } else {
      buffer[buffer_position++] = caps_lock ? input[0] - 32  : input[0];
    }
  }

  printf("%s\n", buffer);

  return 0;
}
