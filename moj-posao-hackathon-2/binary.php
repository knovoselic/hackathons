<?php
$MAX = 10000;
$MOD = 1000000007;

$solution = 0;

function calculate_one($va) {
  global $solution;

  $sum = 0;
  $squares = 0;
  $temp;
  $count = sizeof($va);
  $size = $count;

  for($i = 0; $i < $size; ++$i) {
    $temp = $va[$i];
    $sum = ($sum + $temp);
    $squares = ($squares + $temp*$temp);
  }

  for($i = 0; $i < $size; ++$i) {
    $temp = $va[$i];
    --$count;
    $sum -= $temp;
    $squares -= $temp*$temp;
    $solution = ($solution + $count * $temp * $temp - ($temp<<1)*$sum + $squares);
  }
}

function calculate($va, $vb) {
  global $solution;

  if (sizeof($va) > sizeof($vb)) {
    $bigger = $va;
    $smaller = $vb;
  } else {
    $bigger = $vb;
    $smaller = $va;
  }
  $sum = 0;
  $squares = 0;
  $temp;
  $count = sizeof($bigger);

  $current_size = $count;
  for($i = 0; $i < $current_size; ++$i) {
    $temp = $bigger[$i];
    $sum = $sum + $temp;
    $squares = $squares + $temp*$temp;
  }

  $current_size = sizeof($smaller);
  for($i = 0; $i < $current_size; ++$i) {
    $temp = $smaller[$i];
    $solution = ($solution + $count * $temp * $temp - ($temp<<1)*$sum + $squares);
  }
}


fscanf(STDIN, "%d", $n);

$positions = [];
$numbers = array_map('intval', explode(" ", trim(fgets(STDIN, 1024000))));
$minv = 999999;
$maxv = 0;

for($i = 0; $i < $n; $i++) {
  $temp = $numbers[$i];
  if (!array_key_exists($temp, $positions)) $positions[$temp] = [];
  array_push($positions[$temp], $i);
  if ($temp > $maxv) $maxv = $temp;
  if ($temp < $minv) $minv = $temp;
}

for($i = $minv; $i <= $maxv; $i++) {
  for($j = $minv; $j < $i; $j++) {
    if (($i & $j) != $j) continue;
    if(!array_key_exists($i, $positions)) continue;
    if(!array_key_exists($j, $positions)) continue;
    calculate($positions[$i], $positions[$j]);
  }
}

for($i = $minv; $i <= $maxv; ++$i) {
  if (!array_key_exists($i, $positions) || sizeof($positions[$i]) == 1) continue;
  // ovo traje jako kratko, naravno da se optimizirat kak treba ako bude potrebno
  calculate_one($positions[$i]);
  calculate_one($positions[$i]);
}

fprintf(STDOUT, "%d\n", $solution % $MOD);

?>
