#include <cstdio>
#include <cstdlib>
#include <cstring>

int main(void) {
    char encoded[1000001];
    char decoded[1000001] = {0};

    scanf("%s", encoded);
    int n = strlen(encoded);
    int start = 0;
    int index = 0;
    int step = 2;

    while(start < n) {
        // printf("Start = %d, index = %d, step = %d\n", start, index, step);
        for(int i = start; i < n; i += step) {
            decoded[i] = encoded[index];
            encoded[index++] = '.';
        }
        // for(int i = 0; i < 20; i++) {
        //     printf("%c ", decoded[i]);
        // }
        // printf("\n");
        while(decoded[start] != 0) ++start;
        step = step << 1;
    }
    printf("%s\n", decoded);
    return 0;
}
