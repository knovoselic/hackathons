while (input = readline()) {
  for(i = 0; i < input.length; i++) {
    if (input[i].match(/[a-zA-Z]/)) {
      putstr('-');
    } else if (input[i].match(/\d/)) {
      putstr('*');
    } else {
      putstr('?');
    }
  }
  print();
}
