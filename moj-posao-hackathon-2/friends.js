var graph = [];
for(var i = 0; i < 4001; ++i) {
  graph[i] = [];
}

var max_distance = 0;
var max_vertex = 0;

function bfs(x) {
  q = [];
  q.push(x);
  q.push(0);
  nseen = 0;
  seen = [];
  for(var i = 0; i < 4001; ++i) {
    seen[i] = false;
  }

  while (q.length > 0) {
    vertex = q.shift();
    distance = q.shift();

    if(seen[vertex]) continue;

    ++nseen;
    seen[vertex] = true;

    if (distance > max_distance) {
      max_distance = distance;
      max_vertex = vertex;
    }

    for(var i = 0; i < graph[vertex].length; i++) {
      if(!seen[graph[vertex][i]]) {
        q.push(graph[vertex][i]);
        q.push(distance + 1);
      }
    }
  }

  return nseen;
}

function add_edge(a, b) {
  graph[a].push(b);
  graph[b].push(a);
}

function sequence(x) {
  if (x < 2) return 0;
  x -= 2;

  current_value = 1;
  current_step = 1;

  for(var i = 0; i < x; i += current_step) {
    ++current_value;
    current_step <<= 1;
  }

  return current_value;
}

[n, m] = readline().trim().split(" ").map(Number);

for(var i = 0; i < m; i++) {
  [a, b] = readline().trim().split(" ").map(Number);
  add_edge(a - 1, b - 1);
}

if (bfs(0) == n) {
  bfs(max_vertex);
  print(sequence(max_distance));
} else {
  print("-1");
}
