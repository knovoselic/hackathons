#include <cstdio>
#include <cstring>
#include <ctype.h>

#define NUMBER_OF_LETTERS 26
int main(void) {
  char input[10240];
  int counts[NUMBER_OF_LETTERS];
  int i;

  fgets(input, 10240, stdin);

  memset(counts, 0, NUMBER_OF_LETTERS * sizeof(counts[0]));
  for(i = 0; i < strlen(input); i++) {
    counts[tolower(input[i]) - 'a']++;
  }

  bool firstPrint = true;
  for(i = 0; i < NUMBER_OF_LETTERS; i++) {
    if (counts[i] == 0) continue;
    if (!firstPrint) printf("\n");
    printf("%c:%d", (i + 'a'), counts[i]);
    firstPrint = false;
  }

  return 0;
}
