#include <cstdio>
#include <cstdlib>
#include <cstring>

char numpad[4][3] = {
    {'1', '2', '3'},
    {'4', '5', '6'},
    {'7', '8', '9'},
    {'\0', '0', 'C'}
};

int ci, cj, ni, nj;

void find_next(char needle) {
    for(int i = 0; i < 4; i++) {
        for(int j = 0; j < 3; j++) {
            if (numpad[i][j] == needle) {
                ni = i;
                nj = j;
                return;
            }
        }
    }
}

int main(void) {
    char number[102];

    scanf("%s", number);
    char *burek = number;
    while(*burek) ++burek;
    *burek++ = 'C';
    *burek = '\0';

    // for(int i = 0; i < 4; i++) {
    //     for(int j = 0; j < 3; j++) {
    //         printf("%d ", numpad[i][j]);
    //     }
    //     printf("\n");
    // }
    int number_length = strlen(number);
    int solution = 0;

    find_next('0');
    ci = ni; cj = nj;
    // printf("%d %d\n", ci, cj);
    for(int c = 0; c < number_length; ++c) {
        find_next(number[c]);
        int distance = abs(ci - ni) + abs(cj - nj) + 1;
        solution += distance;
        // printf("%d\n", distance);
        ci = ni; cj = nj;
    }


    printf("%d\n", solution);
    return 0;
}
