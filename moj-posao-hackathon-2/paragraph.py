def print_padded(line, alignment, w):
    if alignment == 'right':
        print('.' * (w - len(line)), end = '')
        print(line)
    elif alignment == 'left':
        print(line, end = '')
        print('.' * (w - len(line)))
    else:
        diff = w - len(line)
        print('.' * (diff // 2), end = '')
        print(line, end = '')
        print('.' * (diff // 2), end = '')
        if diff % 2 == 1: print('.', end='')
        print()


w = int(input())
alignment = input().strip()
words = input().strip().split(" ")

current_width = 0
line = ''
while len(words) > 0:
    if line == '':
        line = words.pop(0)
    elif (len(line) + len(words[0]) + 1) <= w:
        line = line + '.' + words.pop(0)
    else:
        print_padded(line, alignment, w)
        line = ''

if line != '': print_padded(line, alignment, w)
