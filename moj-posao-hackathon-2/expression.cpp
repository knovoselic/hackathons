#include <cstdio>
#include <cstdlib>

void burek(int x) {
    if (x < 0) {
        printf("-");
    } else {
        printf("+");
    }
}
int main(void) {
    int a, b, c, d;

    scanf("%d %d %d %d", &a, &b, &c, &d);
    burek(b);
    burek(c);
    burek(d);
    printf("\n");
}
