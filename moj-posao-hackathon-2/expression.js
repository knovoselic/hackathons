
function burek(x) {
    if (x < 0) {
        putstr("-");
    } else {
        putstr("+");
    }
}

[a, b, c, d] = readline().trim().split(" ").map(Number);
burek(b);
burek(c);
burek(d);
print();
