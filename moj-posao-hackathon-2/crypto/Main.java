import java.io.*;
import java.util.*;

public class Main {
  public static void main(String[] args) {
    Scanner in = new Scanner(System.in);
    char[] encoded = in.next().trim().toCharArray();
    int n = encoded.length;
    int start = 0;
    int index = 0;
    int step = 2;
    char[] decoded = new char[n+1];

    Arrays.fill(decoded, '\0');

    while(start < n) {
      for(int i = start; i < n; i += step) {
        decoded[i] = encoded[index];
        encoded[index++] = '.';
      }
      while(decoded[start] != 0) ++start;
      step = step << 1;
    }

    System.out.println(String.valueOf(decoded).replace("\0", ""));
  }
}
