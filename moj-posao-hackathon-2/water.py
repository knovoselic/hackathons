n, x = [int(x) for x in input().strip().split(" ")]
bottles = [int(x) for x in input().strip().split(" ")]

bottles.sort()

i = 0
solution = 0
while (x > 0 and i < n):
    x -= bottles[i]
    if (x < 0 ): break
    solution += 1;
    i += 1;

print(solution);
