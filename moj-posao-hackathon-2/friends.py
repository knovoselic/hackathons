from collections import deque

graph = {}

max_distance = 0
max_vertex = 0

def bfs(x):
    global max_distance
    global max_vertex

    q = deque()
    q.append(x)
    q.append(0)
    nseen = 0
    seen = [False] * 4001

    while (q):
        vertex = q.popleft()
        distance = q.popleft()

        if(seen[vertex]): continue

        nseen += 1
        seen[vertex] = True

        if distance > max_distance:
            max_distance = distance
            max_vertex = vertex

        if vertex not in graph: continue
        for v in graph[vertex]:
            if seen[v]: continue
            q.append(v)
            q.append(distance + 1)

    return nseen

def add_edge(a, b):
    if a not in graph: graph[a] = []
    if b not in graph: graph[b] = []
    graph[a].append(b)
    graph[b].append(a)

def sequence(x):
    if (x < 2): return 0
    x -= 2;

    current_value = 1
    current_step = 1

    i = 0
    while i < x:
        current_value += 1
        current_step <<= 1
        i += current_step

    return current_value

n, m = [int(x) for x in input().strip().split(" ")]

for i in range(m):
    a, b = [int(x) for x in input().strip().split(" ")]
    add_edge(a - 1, b - 1);

if bfs(0) == n:
    bfs(max_vertex)
    print(sequence(max_distance))
else:
    print("-1")
