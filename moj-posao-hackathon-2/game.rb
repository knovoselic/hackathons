#!/usr/bin/env ruby
MAX_N = 100
MAX_NUMBER = 1_000_000_000
File.open('game_input9', 'w') do |f|
  n = rand(MAX_N..MAX_N)
  f.puts n
  n.times do
    f.puts "#{rand(MAX_NUMBER)}"
  end
end
