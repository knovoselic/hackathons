import fileinput

for line in fileinput.input():
    world = [int(x) for x in line.strip().split(" ")]
    min_amount = 32768;

    for i in range(0, 6):
        min = 5;
        for j in range(0, 5):
            if (world[i + j] < min): min = world[i + j]

        amount = 0;
        for j in range(0, 5):
            amount += world[i + j] - min;
        if (amount < min_amount): min_amount = amount
    print(min_amount);
