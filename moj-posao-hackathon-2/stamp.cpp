#include <cstdio>
#include <cstring>
#include <ctype.h>

char mem[10000][10000];

int main(void) {
  int n;
  int x1;
  int x2;
  int y1;
  int y2;
  long area = 0;

  memset(mem, 0, 10000 * 10000 * sizeof(mem[0][0]));

  scanf("%d", &n);
  for(int i = 0; i < n; i++) {
    scanf("%d %d %d %d", &x1, &y1, &x2, &y2);
    for(int x = x1; x < x2; x++) {
      for(int y = y1; y < y2; y++) {
        mem[x][y] = 1;
      }
    }
  }

  scanf("%d", &n);
  for(int i = 0; i < n; i++) {
    scanf("%d %d %d %d", &x1, &y1, &x2, &y2);
    for(int x = x1; x < x2; x++) {
      for(int y = y1; y < y2; y++) {
        mem[x][y] = 1;
      }
    }
  }

  for(int x = 0; x < 10000; x++) {
    for(int y = 0; y < 10000; y++) {
      if (mem[x][y] == 1) ++area;
    }
  }

  printf("%ld\n", area);

  return 0;
}
