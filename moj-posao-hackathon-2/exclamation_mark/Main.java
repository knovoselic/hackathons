import java.io.*;
import java.util.*;

public class Main {
  public static void main(String[] args) {
    Scanner in = new Scanner(System.in);
    String w1 = in.next();
    String w2 = in.next();

    HashSet<String> set = new HashSet<String>(Arrays.asList(w1.split("")));
    set.retainAll(new HashSet<String>(Arrays.asList(w2.split(""))));

    for(String s : set) {
      w1 = w1.replace(s, "!");
      w2 = w2.replace(s, "!");
    }

    System.out.println(w1);
    System.out.println(w2);
  }
}
