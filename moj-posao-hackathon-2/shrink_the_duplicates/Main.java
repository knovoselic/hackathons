import java.io.*;
import java.util.*;

public class Main {

  public static void main(String[] args) {
    Scanner in = new Scanner(System.in);
    ArrayList<Integer> input = new ArrayList<Integer>();

    while(in.hasNextInt())
    {
      input.add(in.nextInt());
    }

    for(int i = 0; i < input.size();) {
      Integer current = input.get(i);
      System.out.print(current);
      System.out.print(" ");
      i++;
      for(; i < input.size() && input.get(i) == current; i++) {}
    }
    System.out.println();
  }
}
