import fileinput

for line in fileinput.input():
    line = line.lower()
    for c in line:
        if(c.isalpha()):
            print("-", end = "")
        elif (c.isdigit()):
            print("*", end = "")
        else:
            print("?", end = "")
    print()
