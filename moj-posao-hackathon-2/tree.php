<?php
$MAX = 1001;

function branch($x, $y, $dx, $dy, $level) {
  global $lines, $n, $a;

  for($i = 0; $i < $a[$level]; ++$i) {
    $x += $dx;
    $y += $dy;
    $lines[$x][$y] = '*';
  }

  ++$level;
  if ($level >= $n) return;

  if ($dx != 0 && $dy != 0) {
    branch($x, $y, $dx, 0, $level);
    branch($x, $y, 0, $dy, $level);
  } else if ($dx == 0) {
    branch($x, $y, -1, $dy, $level);
    branch($x, $y, 1, $dy, $level);
  } else {
    branch($x, $y, $dx, -1, $level);
    branch($x, $y, $dx, 1, $level);
  }
}

fscanf(STDIN, "%d", $n);
$a = array_map('intval', explode(" ", trim(fgets(STDIN, 102400))));
$lines = array();

for($i = 0; $i < $MAX; ++$i) {
  array_push($lines, str_repeat(".", $MAX));
}

$x = $MAX;
$y = $MAX / 2;

for($i = 0; $i < $a[0]; ++$i) {
  --$x;
  $lines[$x][$y] = '*';
}

branch($x, $y, -1, -1, 1);
branch($x, $y, -1, 1, 1);

$minx = $MAX;
$miny = $MAX;
$maxx = 0;
$maxy = 0;

for($i = 0; $i < $MAX; ++$i) {
  for($j = 0; $j < $MAX; ++$j) {
    if ($lines[$i][$j] == '.') continue;
    $minx = min($minx, $i);
    $miny = min($miny, $j);
    $maxx = max($maxx, $i);
    $maxy = max($maxy, $j);
  }
}

for($i = $minx; $i <= $maxx; ++$i) {
  for($j = $miny; $j <= $maxy; ++$j) {
    if ($lines[$i][$j] == '.') {
      echo('.');
    } else {
      echo('*');
    }
  }
  echo("\n");
}
