import java.io.*;
import java.util.*;

public class Main {

    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        while(in.hasNextLine())
        {
            String inputLine = in.nextLine();
            System.out.print(inputLine.charAt(0));
            for(int i = 1; i < inputLine.length(); i++) {
              if(inputLine.charAt(i) == inputLine.charAt(0)) {
                System.out.print("*");
              } else {
                System.out.print(inputLine.charAt(i));
              }
            }
            System.out.println();
        }
    }
}
