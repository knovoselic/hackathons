function is_pal(board, x, y, k) {
  for (var i = 0; i < k; ++i) {
    var yi = y + i;
    var yii = y - i - 1 + k;
    var xk = x + k - 1;
    for (var j = 0; j < k; ++j) {
      if (board[yi][x + j] != board[yii][xk- j]) return false;
    }
  }

  return true;
}

function solve(board, m, n) {
  var max_k = Math.min(m, n);
  var current_k = max_k;

  while(true) {
    var mk = m - current_k;
    var nk = n - current_k;
    for (var x = 0; x <= mk; ++x) {
      for (var y = 0; y <= nk; ++y) {
        if (is_pal(board, x, y, current_k)) return current_k;
      }
    }

    --current_k;
  }

  return 1;
}

[n, m] = readline().trim().split(" ").map(Number);
board = [];

for(var r = 0; r < n; ++r) {
  board[r] = readline().trim();
}

print(solve(board, m, n));
