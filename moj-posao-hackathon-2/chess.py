import fileinput

input = iter(fileinput.input())
n, k = [int(x) for x in next(input).strip().split(" ")]
board = [['.' for i in range(15)] for j in range(15)]
solution = 0

for i in range(k):
    piece_name, row, col = next(input).strip().split(" ")
    row, col = [int(x) for x in [row, col]]
    board[row - 1][col - 1] = piece_name[0]

for row in range(n):
    for col in range (n):
        if board[row][col] == 'r':
            for r in range(row - 1, -1, -1):
                if board[r][col] == 'b' or board[r][col] == 'r': break
                board[r][col] = '#'
            for r in range(row + 1, n):
                if board[r][col] == 'b' or board[r][col] == 'r': break
                board[r][col] = '#'
            for c in range(col - 1, -1, -1):
                if board[row][c] == 'b' or board[row][c] == 'r': break
                board[row][c] = '#'
            for c in range(col + 1, n):
                if board[row][c] == 'b' or board[row][c] == 'r': break
                board[row][c] = '#'
        elif board[row][col] == 'b':
            r = row - 1
            c = col - 1

            while(r >= 0 and c >= 0):
                if board[r][c] == 'b' or board[r][c] == 'r': break
                board[r][c] = '#'
                r -= 1
                c -= 1

            r = row - 1
            c = col + 1
            while(r >= 0 and c < n):
                if board[r][c] == 'b' or board[r][c] == 'r': break
                board[r][c] = '#'
                r -= 1
                c += 1

            r = row + 1
            c = col - 1
            while(r < n and c >= 0):
                if board[r][c] == 'b' or board[r][c] == 'r': break
                board[r][c] = '#'
                r += 1
                c -= 1

            r = row + 1
            c = col + 1
            while(r < n and c < n):
                if board[r][c] == 'b' or board[r][c] == 'r': break
                board[r][c] = '#'
                r += 1
                c += 1
for row in range(n):
    for col in range (n):
        width = 0
        height = 0
        max_width = 0
        max_height = 0

        while(col + max_width < n and board[row][col + max_width] == '.'): max_width += 1
        while(row + max_height < n and board[row + max_height][col] == '.'): max_height += 1

        abort = False;
        for height in range(1, max_height):
            for i in range(0, max_width):
                if board[row + height][col + i] != '.':
                    abort = True
                    break
            if abort: break
        if not abort: height += 1
        solution = max(solution, max_width * height);

        abort = False;
        for width in range(1, max_width):
            for i in range(0, max_height):
                if board[row + i][col + width] != '.':
                    abort = True
                    break
            if abort: break
        if not abort: width += 1
        solution = max(solution, width * max_height);

for i in range(n):
    for j in range(n):
        print(board[i][j] + ' ', end='')
    print()

print(solution)
