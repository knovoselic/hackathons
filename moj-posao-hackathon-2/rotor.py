def is_pal(board, x, y, k):
    i = 0
    while i < k:
        yi = y + i
        yii = y - i - 1 + k
        xk = x + k - 1
        j = 0
        while j < k:
            if board[yi][x + j] != board[yii][xk- j]: return False
            j += 1
        i += 1

    return True

def solve(board, m, n):
    max_k = min(m, n)
    current_k = max_k

    while(True):
        mk = m - current_k + 1
        nk = n - current_k + 1
        x = 0
        while x < mk:
            y = 0
            while y < nk:
                if is_pal(board, x, y, current_k): return current_k
                y += 1
            x += 1

        current_k -= 1

    return 1

n, m = [int(x) for x in input().strip().split(" ")]
board = []

for r in range(n):
    board.append(input().strip())

print(solve(board, m, n))
