#include <cstdio>
#include <cstring>

int main(void) {
  int letters[26] = {0};
  char w1[255];
  char w2[255];

  scanf("%s", w1);
  scanf("%s", w2);

  for(unsigned int i = 0; i < strlen(w1); ++i) {
    letters[w1[i] - 'a'] |= 1;
  }
  for(unsigned int i = 0; i < strlen(w2); ++i) {
    letters[w2[i] - 'a'] |= 2;
  }

  for(unsigned int i = 0; i < strlen(w1); ++i) {
    if (letters[w1[i] - 'a'] == 3) {
      printf("!");
    } else {
      printf("%c", w1[i]);
    }
  }
  printf("\n");
  for(unsigned int i = 0; i < strlen(w2); ++i) {
    if (letters[w2[i] - 'a'] == 3) {
      printf("!");
    } else {
      printf("%c", w2[i]);
    }
  }
  printf("\n");

  return 0;
}
