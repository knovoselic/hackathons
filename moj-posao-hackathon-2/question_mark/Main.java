import java.io.*;
import java.util.*;

public class Main {
  public static int n;
  public static int k;
  public static int[] indexes = {-1, -1, -1, -1, -1};
  public static int[] values = {0, 0, 0, 0, 0};
  public static int qm = 0;
  public static int[] number = new int[101];

  public static Boolean number_divisible_by_k() {
      int i = 0;
      int temp = number[i];

      while(i < n - 1) {
          temp = (temp % k) * 10 + number[++i];
      }

      temp %= k;

      return temp == 0;
  }

  public static Boolean has_more_values() {
      for(int i = 0; i < qm; i++) {
          if (values[i] != 9) return true;
      }
      return false;
  }

  public static void next_value() {
      ++values[0];
      for(int i = 0; i < qm; i++) {
          if (values[i] > 9) {
              values[i] -= 10;
              ++values[i + 1];
          }
      }
  }

  public static void create_new_number() {
      for(int i = 0; i < qm; i++) {
          number[indexes[qm - i - 1]] = values[i];
      }
  }

  public static void main(String[] args) {
    Scanner in = new Scanner(System.in);


    n = in.nextInt();
    k = in.nextInt();

    char[] temp = in.next().toCharArray();

    int j = 0;
    for(int i = 0; i < n; i++) {
        if (temp[i] == '?') {
            ++qm;
            indexes[j++] = i;
            number[i] = -1;
        } else {
            number[i] = temp[i] - '0';
        }
    }


    // print_number();

    values[0] = -1;
    int solution = 0;

    if (number[0] == -1) {
      values[qm-1] = qm == 1 ? 0 : 1;
    }

    while(has_more_values()) {
        next_value();
        create_new_number();
        // print_number();
        if (number_divisible_by_k()) ++solution;
    }

    System.out.println(solution);
  }
}
