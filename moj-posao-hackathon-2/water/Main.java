import java.io.*;
import java.util.*;

public class Main {
  public static void main(String[] args) {
    Scanner in = new Scanner(System.in);
    ArrayList<Integer> bottles = new ArrayList<Integer>();
    Integer n;
    Integer x;
    Integer i;
    Integer solution = 0;

    n = in.nextInt();
    x = in.nextInt();

    for(i = 0; i < n; i++) {
      bottles.add(in.nextInt());
    }

    Collections.sort(bottles);
    i = 0;
    while (x > 0 && i < n) {
        x -= bottles.get(i);
        if (x < 0 ) break;
        ++solution;
        ++i;
    }

    System.out.println(solution);
  }
}
