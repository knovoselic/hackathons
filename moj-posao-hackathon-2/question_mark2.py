import fileinput
debug = False
# debug = True

input = fileinput.input()
n, k = map(int, input.readline().strip().split(" "))
temp = input.readline().strip()
number = int(temp.replace('?', '0'))

if debug: print(temp)
if debug: print(number)
indexes = [i for i, char in enumerate(temp[::-1]) if char == '?']
increment = [10**i for i in indexes]
qm = len(indexes)
if debug: print(indexes)
if debug: print(increment)
values = [0] * qm
values[0] = -1
max_sum = qm * 9
solution = 0

if debug: print(values)

number -= increment[0]
while sum(values) < max_sum:
    updated_indexes = [0]
    values[0] += 1
    number += increment[0]
    for i in range(qm):
        if values[i] > 9:
            values[i] -= 10;
            number -= increment[i] * 10;
            number += increment[i + 1]
            values[i + 1] += 1
            updated_indexes.append(i + 1)
    if debug: print(updated_indexes)
    if debug: print(number)
    if number % k == 0: solution += 1

print(solution)
