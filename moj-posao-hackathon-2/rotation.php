<?php

function rotate($N, $x, $y) {
  global $mat;

  for ($i = 0; $i < $N / 2; ++$i) {
    for ($j = $i; $j < $N - $i - 1; ++$j) {
        $temp = $mat[$i + $x][$j + $y];

        $mat[$i + $x][$j + $y] = $mat[$j + $x][$N - 1 - $i + $y];
        $mat[$j + $x][$N - 1 - $i + $y] = $mat[$N - 1 - $i + $x][$N - 1 - $j + $y];
        $mat[$N - 1 - $i + $x][$N - 1 - $j + $y] = $mat[$N - 1 - $j + $x][$i + $y];
        $mat[$N - 1 - $j + $x][$i + $y] = $temp;
    }
  }
}

$d = array();
$mat = array();

fscanf(STDIN, "%d", $n);

for($i = 0; $i < $n; $i++) {
  $mat[$i] = array_map('intval', explode(" ", trim(fgets(STDIN, 102400))));
}

fscanf(STDIN, "%d", $q);

for($i = 0; $i < $q; $i++) {
  $d[$i] = array_map('intval', explode(" ", trim(fgets(STDIN, 102400))));
}

for($i = $q - 1; $i >= 0; --$i) {
  $x = $d[$i][0] - 1;
  $y = $d[$i][1] - 1;
  $a = $d[$i][2];
  $k = $d[$i][3] % 4;

  while($k--) rotate($a, $x, $y);
}

for($i = 0; $i < $n; $i++) {
  for ($j = 0; $j < $n; $j++) {
    echo $mat[$i][$j];
    if ($j < $n - 1) echo ' ';
  }
  echo("\n");
}
echo("\n");
