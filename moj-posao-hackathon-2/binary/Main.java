import java.io.*;
import java.util.*;

public class Main {
  public static final int MAX = 10000;
  public static long solution = 0;
  public static long MOD = 1000000007;

  public static void calculate(ArrayList<Integer> va) {
    long sum = 0;
    long squares = 0;
    long temp;
    long count = va.size();

    int current_size = va.size();
    for(int i = 0; i < current_size; ++i) {
      temp = va.get(i);
      sum = sum + temp;
      squares = squares + temp*temp;
    }

    for(int i = 0; i < current_size; ++i) {
      temp = va.get(i);
      --count;
      sum -= temp;
      squares -= temp*temp;
      solution = (solution + count * temp * temp - (temp<<1)*sum + squares);
    }
  }

  public static void calculate(ArrayList<Integer> va, ArrayList<Integer>vb) {
    ArrayList<Integer> bigger;
    ArrayList<Integer> smaller;
    if (va.size() > vb.size()) {
      bigger = va;
      smaller = vb;
    } else {
      bigger = vb;
      smaller = va;
    }
    long sum = 0;
    long squares = 0;
    long temp;
    long count = bigger.size();

    int current_size = bigger.size();
    for(int i = 0; i < current_size; ++i) {
      temp = bigger.get(i);
      sum = sum + temp;
      squares = squares + temp*temp;
    }

    current_size = smaller.size();
    for(int i = 0; i < current_size; ++i) {
      temp = smaller.get(i);
      solution = (solution + count * temp * temp - (temp<<1)*sum + squares);
    }
  }

  public static void main(String[] args) throws IOException {
    // Scanner in = new Scanner(System.in);
    BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
    StringTokenizer st = new StringTokenizer(br.readLine());

    int n, temp;
    HashMap<Integer, ArrayList<Integer>> positions = new HashMap<Integer, ArrayList<Integer>>();

    n = Integer.parseInt(st.nextToken());

    st = new StringTokenizer(br.readLine());
    for(int i = 0; i < n; i++) {
      temp = Integer.parseInt(st.nextToken());
      if (!positions.containsKey(temp)) {
        positions.put(temp, new ArrayList<Integer>(100));
      }
      positions.get(temp).add(i);
    }
    for(int i = 1; i <= MAX; i++) {
      for(int j = 1; j < i; j++) {
        if ((i & j) != j) continue;
        // DEBUG("Checking %d, %d\n", i, j);
        if(!positions.containsKey(i)) continue;
        if(!positions.containsKey(j)) continue;
        ArrayList<Integer> vi = positions.get(i);
        ArrayList<Integer> vj = positions.get(j);
        calculate(vi, vj);
      }
    }

    for(int i = 1; i <= MAX; ++i) {
      if (!positions.containsKey(i) || positions.get(i).size() == 1) continue;
      // ovo traje jako kratko, naravno da se optimizirat kak treba ako bude potrebno
      calculate(positions.get(i));
      calculate(positions.get(i));
    }

    System.out.println(Long.remainderUnsigned(solution, MOD));
  }
}
