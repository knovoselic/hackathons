<?php

$input = fgets(STDIN, 10240);

for($i = 0; $i < strlen($input); $i++) {
  if(ctype_alpha($input[$i])) {
    fprintf(STDOUT, "-");
  } else if (ctype_digit($input[$i])) {
    fprintf(STDOUT, "*");
  } else {
    fprintf(STDOUT, "?");
  }
}
fprintf(STDOUT, "\n");
?>
