import java.io.*;
import java.util.*;

public class Main {
  public static char[][] numpad = {
    {'1', '2', '3'},
    {'4', '5', '6'},
    {'7', '8', '9'},
    {'\0', '0', 'C'}
  };

  public static int ci;
  public static int cj;
  public static int ni;
  public static int nj;

  public static void find_next(char needle) {
      for(int i = 0; i < 4; i++) {
          for(int j = 0; j < 3; j++) {
              if (numpad[i][j] == needle) {
                  ni = i;
                  nj = j;
                  return;
              }
          }
      }
  }

  public static void main(String[] args) {
    Scanner in = new Scanner(System.in);
    String number;
    ArrayList<Integer> bottles = new ArrayList<Integer>();

    number = in.next() + "C";

    int number_length = number.length();
    int solution = 0;

    find_next('0');
    ci = ni; cj = nj;
    for(int c = 0; c < number_length; ++c) {
        find_next(number.charAt(c));
        int distance = Math.abs(ci - ni) + Math.abs(cj - nj) + 1;
        solution += distance;
        ci = ni; cj = nj;
    }

    System.out.println(solution);
  }
}
