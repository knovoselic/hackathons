import fileinput
import re

for line in fileinput.input():
    print(re.sub(r"not.*?bad", "good", line), end="")
