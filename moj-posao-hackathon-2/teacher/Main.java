import java.io.*;
import java.util.*;

public class Main {
  public static void main(String[] args) {
    Scanner in = new Scanner(System.in);
    int n, x;

    n = in.nextInt();
    x = in.nextInt();

    while(--n > 0) {
      x -= in.nextInt();
    }

    System.out.println(x);
  }
}
