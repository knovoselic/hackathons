<?php

$input = array_map('intval', explode(" ", trim(fgets(STDIN, 10240))));
$len = count($input);

for($i = 0; $i < $len; ) {
  $current = $input[$i];
  fprintf(STDOUT, "%d ", $input[$i]);
  $i++;
  for(; $i < $len && $input[$i] == $current; $i++) {}
}
fprintf(STDOUT, "\n");
?>
