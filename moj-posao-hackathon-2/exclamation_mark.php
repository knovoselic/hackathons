<?php

$w1 = trim(fgets(STDIN, 102400));
$w2 = trim(fgets(STDIN, 102400));

$s1 = str_split($w1);
$s2 = str_split($w2);

$s1 = array_unique($s1);
$s2 = array_unique($s2);

$s1 = array_intersect($s1, $s2);

foreach ($s1 as $c) {
  $w1 = str_replace($c, '!', $w1);
  $w2 = str_replace($c, '!', $w2);
}

fprintf(STDOUT, "%s\n%s\n", $w1, $w2);
?>
