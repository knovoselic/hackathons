w1 = input()
w2 = input()

for c in set(w1) & set(w2):
    w1 = w1.replace(c, '!')
    w2 = w2.replace(c, '!')

print(w1)
print(w2)
