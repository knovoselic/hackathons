import java.io.*;
import java.util.*;

public class Main {

  public static void main(String[] args) {
    Scanner in = new Scanner(System.in);

    int aa = in.nextInt();
    int bb = in.nextInt();

    int a = Math.min(aa, bb);
    int b = Math.max(aa, bb);

    int diff = b - a;
    if (a % 2 == 0 && b % 2 == 1) diff += 2;
    if (diff % 2 == 0) {
      System.out.println(diff / 2);
    } else {
      System.out.println((diff + 1) / 2);
    }
  }
}
