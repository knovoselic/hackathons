n, x = [int(x) for x in input().strip().split(" ")]
heights = [int(x) for x in input().strip().split(" ")]

print(x - sum(heights))
