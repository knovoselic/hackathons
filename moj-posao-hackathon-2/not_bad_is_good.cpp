#include <cstdio>
#include <cstring>
#include <ctype.h>

int main(void) {
  char line[10240];
  int i, linelen;
  bool inside_substring = false;

  fgets(line, 10240, stdin);
  linelen = strlen(line);
  for(i = 0; i < linelen; i++) {
    if (inside_substring) {
      if (tolower(line[i]) == 'd' &&
          (i - 1) > 0 && tolower(line[i - 1]) == 'a' &&
          (i - 2) > 0 && tolower(line[i - 2]) == 'b') {
        inside_substring = false;
        printf("good");
      }
    } else {
      if (tolower(line[i]) == 'n' &&
          (i + 1) < linelen && tolower(line[i + 1]) == 'o' &&
          (i + 2) < linelen && tolower(line[i + 2]) == 't') {
        inside_substring = true;
        continue;
      }
      printf("%c", line[i]);
    }
  }

  return 0;
}
