#include <cstdio>

int mem[200001];
long long int modulo = 1000000007;

int main()
{
	long int n;
	scanf("%ld", &n);
	for(long int i=0; i<n; i++)
	{
		scanf("%d", mem + i);
	}


	long long int res = 0;
	for(long long int i=0; i<n; i++)
	{
		for(long long int j=0; j<n; j++)
		{
			if((mem[i] & mem[j]) == mem[j])
			{
				res = (res + (((i - j) * (i - j)) % modulo)) % modulo;
				// printf("%lld\n", res);
			}	
		}	
	}

	printf("%lld\n", res);

	return 0;
}
