import java.io.*;
import java.util.*;

public class Main {
  public static ArrayList<Integer>[] graph = new ArrayList[4001];
  public static ArrayList<Boolean> seen = new ArrayList<Boolean>();
  public static int n;
  public static int m;
  public static int max_distance = 0;
  public static int max_vertex = 0;

  public static int bfs(int x) {
    ArrayDeque<Integer> q = new ArrayDeque<Integer>();
    q.add(x);
    q.add(0);
    Integer nseen = 0;
    seen = new ArrayList<Boolean>(Collections.nCopies(4001, false));

    while (!q.isEmpty()) {
      Integer vertex = q.poll();
      Integer distance = q.poll();

      if(seen.get(vertex)) continue;

      ++nseen;
      seen.set(vertex, true);

      if (distance > max_distance) {
        max_distance = distance;
        max_vertex = vertex;
      }

      for(Integer i = 0; i < graph[vertex].size(); i++) {
        if(!seen.get(graph[vertex].get(i))) {
          q.add(graph[vertex].get(i));
          q.add(distance + 1);
        }
      }
    }

    return nseen;
  }

  public static void add_edge(Integer a, Integer b) {
    graph[a].add(b);
    graph[b].add(a);
  }

  public static Integer sequence(Integer x) {
    if (x < 2) return 0;
    x -= 2;

    Integer current_value = 1;
    Integer current_step = 1;

    for(Integer i = 0; i < x; i += current_step) {
      ++current_value;
      current_step <<= 1;
    }

    return current_value;
  }

  public static void main(String[] args) {
    Scanner in = new Scanner(System.in);
    int a, b;
    int start;

    for(int i = 0; i < 4001; ++i) {
      graph[i] = new ArrayList<Integer>();
    }

    n = in.nextInt();
    m = in.nextInt();

    a = in.nextInt();
    b = in.nextInt();
    start = a - 1;
    add_edge(a - 1, b - 1);
    for(Integer i = 1; i < m; i++) {
      a = in.nextInt();
      b = in.nextInt();
      add_edge(a - 1, b - 1);
    }

    if (bfs(0) == n) {
      bfs(max_vertex);
      System.out.println(sequence(max_distance));
    } else {
      System.out.println(-1);
    }
  }
}
