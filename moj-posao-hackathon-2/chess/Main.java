import java.io.*;
import java.util.*;

public class Main {

  public static void main(String[] args) {
    Scanner in = new Scanner(System.in);
    ArrayList<Integer> input = new ArrayList<Integer>();
    char board[][] = new char[15][15];
    String piece_name;
    int n, k;
    int solution = 0;

    for(int row = 0; row < 15; row++) {
      for(int col = 0; col < 15; col++) {
        board[row][col] = '.';
      }
    }

    n = in.nextInt();
    k = in.nextInt();
    for(int i = 0; i < k; i++) {
      piece_name = in.next();
      int row = in.nextInt();
      int col = in.nextInt();
      board[--row][--col] = piece_name.charAt(0);
    }

    for(int row = 0; row < n; row++) {
      for(int col = 0; col < n; col++) {
        if (board[row][col] == 'r') {
          for(int r = row - 1; r >= 0; --r) {
            if (board[r][col] == 'b' || board[r][col] == 'r') break;
            board[r][col] = '#';
          }
          for(int r = row + 1; r < n; ++r) {
            if (board[r][col] == 'b' || board[r][col] == 'r') break;
            board[r][col] = '#';
          }
          for(int c = col - 1; c >= 0; --c) {
            if (board[row][c] == 'b' || board[row][c] == 'r') break;
            board[row][c] = '#';
          }
          for(int c = col + 1; c < n; ++c) {
            if (board[row][c] == 'b' || board[row][c] == 'r') break;
            board[row][c] = '#';
          }
        } else if (board[row][col] == 'b') {
          int r = row, c = col;

          while(--r >= 0 && --c >= 0) {
            if (board[r][c] == 'b' || board[r][c] == 'r') break;
            board[r][c] = '#';
          }
          r = row;
          c = col;
          while(--r >= 0 && ++c < n) {
            if (board[r][c] == 'b' || board[r][c] == 'r') break;
            board[r][c] = '#';
          }
          r = row;
          c = col;
          while(++r < n && --c >= 0) {
            if (board[r][c] == 'b' || board[r][c] == 'r') break;
            board[r][c] = '#';
          }
          r = row;
          c = col;
          while(++r < n && ++c < n) {
            if (board[r][c] == 'b' || board[r][c] == 'r') break;
            board[r][c] = '#';
          }
        }
      }
    }

    for(int row = 0; row < n; row++) {
      for(int col = 0; col < n; col++) {
        int width = 0;
        int height = 0;
        int max_width = 0;
        int max_height = 0;
        Boolean abort;

        while(col + max_width < n && board[row][col + max_width] == '.') {
          ++max_width;
        }
        while(row + max_height < n && board[row + max_height][col] == '.') ++max_height;

        abort = false;
        for(height = 1; height < max_height; height++) {
          for(int i = 0; i < max_width; i++) {
            if (board[row + height][col + i] != '.') {
              abort = true;
              break;
            }
          }
          if (abort) break;
        }
        solution = Math.max(solution, max_width * height);

        abort = false;
        for(width = 1; width < max_width; width++) {
          for(int i = 0; i < max_height; i++) {
            if (board[row + i][col + width] != '.') {
              abort = true;
              break;
            }
          }
          if (abort) break;
        }
        solution = Math.max(solution, width * max_height);
      }
    }

    System.out.println(solution);
  }
}
