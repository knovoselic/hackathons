#include <cstdio>
#include <cstdlib>
#include <cmath>
#include <cstring>

int n, k;
int indexes[5] = {-1, -1, -1, -1, -1};
int values[5] = {0};
int qm = 0;
char number[101];

bool number_divisible_by_k() {
    int i = 0;
    int temp = number[i];
    // while (temp < k) temp = temp * 10 + number[++i];

    while(i < n - 1) {
        temp = (temp % k) * 10 + number[++i];
    }

    temp %= k;

    // printf("%d\n", temp);
    return temp == 0;
}

bool has_more_values() {
    for(int i = 0; i < qm; i++) {
        if (values[i] != 9) return true;
    }
    return false;
}

void next_value() {
    ++values[0];
    for(int i = 0; i < qm; i++) {
        if (values[i] > 9) {
            values[i] -= 10;
            ++values[i + 1];
        }
    }
}

void create_new_number() {
    for(int i = 0; i < qm; i++) {
        number[indexes[qm - i - 1]] = values[i];
    }
}

void print_number() {
    for(int i = 0; i < n; i++) {
        printf("%d", number[i]);
    }
    printf("\n");
}

int main(void) {
    char temp[101];

    scanf("%d %d", &n, &k);
    scanf("%s", temp);

    int j = 0;
    for(int i = 0; i < n; i++) {
        if (temp[i] == '?') {
            ++qm;
            indexes[j++] = i;
            number[i] = -1;
        } else {
            number[i] = temp[i] - '0';
        }
    }


    // print_number();

    values[0] = -1;
    int solution = 0;

    if (number[0] == -1) {
      values[qm-1] = qm == 1 ? 0 : 1;
    }

    while(has_more_values()) {
        next_value();
        create_new_number();
        // print_number();
        if (number_divisible_by_k()) ++solution;
    }

    printf("%d\n", solution);
}
