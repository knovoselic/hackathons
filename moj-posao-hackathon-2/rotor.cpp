#include <cstdio>
#include <cstring>
#include <algorithm>
#include <vector>

using namespace std;

int n, m;
char board[510][510];

bool is_pal(int i, int j, int p, int q) {
  int xmax = p - i + 1;

  for(int r = 0; r < xmax; ++r) {
    if (board[i + r][j] != board[p - r][q]) return false;
  }

  for(int c = 1; c < xmax - 1; ++c) {
    if (board[i][j + c] != board[p][q - c]) return false;
  }

  return true;
}


int main(void) {

  int max_k;

  scanf("%d %d", &n, &m);

  getc(stdin); // chomp new line
  for(int r = 0; r < n; r++) {
    fscanf(stdin, "%s", board[r]);
    // fgets(board[r], m + 2, stdin);
  }

  int mem[500][500] = {{0}};
  int max_same = 1;
  for(int i = 0; i < n; i++) {
    mem[i][0] = 1;
  }
  for(int i = 0; i < m; i++) {
    mem[0][i] = 1;
  }
  for(int i = 1; i < n; i++) {
    for(int j = 1; j < m; j++) {
      if (board[i][j - 1] == board[i][j] &&
          board[i - 1][j - 1] == board[i][j] &&
          board[i - 1][j] == board[i][j]) {
        mem[i][j] = std::min(mem[i - 1][j], std::min(mem[i - 1][j - 1], mem[i][j - 1])) + 1;
      } else {
        mem[i][j] = 1;
      }
      if (mem[i][j] > max_same) max_same = mem[i][j];
    }
  }

  int top, left, bottom, right;
  bool good = true;

  //odd k
  int current_k = max_same;
  if (current_k % 2 == 0) --current_k;
  int nmo = n - 1;
  int mmo = m - 1;
  int cr, cc, k, r, c;

  for(cr = 1; cr < nmo; cr++) {
    for(cc = 1; cc < mmo; cc++) {
      max_k = (std::min(cr, std::min(cc, std::min(nmo - cr, mmo - cc))) << 1) + 1;
      if (max_k <= current_k) continue;
      top = cr - 1;
      left = cc - 1;
      bottom = cr + 1;
      right = cc + 1;

      good = true;
      for(k = 3; k <= max_k; k += 2) {

        // compare left and right columns
        for(r = 0; r < k; r++) {
          if (board[top + r][left] != board[bottom - r][right]) {
            good = false;
            break;
          }
          if (!good) break;
        }

        if (!good) break;

        // compare top and bottom rows if needed
        for(c = 1; c < k - 1; c++) {
          if (board[top][left + c] != board[bottom][right - c]) {
            good = false;
            break;
          }
          if (!good) break;
        }

        if (good) {
          current_k = std::max(current_k, k);
          --top;
          --left;
          ++bottom;
          ++right;
        } else {
          break;
        }
      }
    }
  }

  //even K. (cr, cc) are middle point of each square
  int last_k = current_k;
  --current_k;
  for(cr = 1; cr < n; cr++) {
    for(cc = 1; cc < m; cc++) {
      max_k = std::min(cr, std::min(cc, std::min(n - cr, m - cc))) << 1;
      if (max_k <= current_k) continue;
      top = cr - 1;
      left = cc - 1;
      bottom = cr;
      right = cc;

      good = true;
      for(k = 2; k <= max_k; k += 2) {

        // compare left and right columns
        for(r = 0; r < k; r++) {
          if (board[top + r][left] != board[bottom - r][right]) {
            good = false;
            break;
          }
          if (!good) break;
        }

        if (!good) break;

        // compare top and bottom rows if needed
        for(c = 1; c < k - 1; c++) {
          if (board[top][left + c] != board[bottom][right - c]) {
            good = false;
            break;
          }
          if (!good) break;
        }

        if (good) {
          current_k = std::max(current_k, k);
          --top;
          --left;
          ++bottom;
          ++right;
        } else {
          break;
        }
      }
    }
  }

  printf("%d\n", std::max(current_k, last_k));
  return 0;
}
