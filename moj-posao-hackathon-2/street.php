<?php
  fscanf(STDIN, "%d %d", $aa, $bb);

  $a = min($aa, $bb);
  $b = max($aa, $bb);
  $diff = $b - $a;
  if ($a % 2 == 0 && $b % 2 == 1) $diff += 2;
  if ($diff % 2 == 0) {
    fprintf(STDOUT, "%d\n", $diff / 2);
  } else {
    fprintf(STDOUT, "%d\n", ($diff + 1) / 2);
  }
?>
