#include <cstdio>
#include <cstring>
#include <algorithm>

int main(void) {
  int n, m;
  int max_k;
  bool debug = false;
  unsigned long total_iterations = 0;
  unsigned long total_checks = 0;

  scanf("%d %d", &n, &m);
  char board[n][m];
  if (n > 10) debug = false;

  getc(stdin); // chomp new line
  for(int r = 0; r < n; r++) {
    for(int c = 0; c < m; c++) {
      board[r][c] = getc(stdin);
    }
    getc(stdin);
  }

  // OLD algorithm, much slower - 10 to 25 times, depending on the case
  // int current_k = 1;
  // int last_k = 1;
  // int er, ec, mc;
  // bool good = true;
  // for(int sr = 0; sr < n; sr++) {
  //   for(int sc = 0; sc < m; sc++) {
  //     max_k = std::min(n - sr, m - sc);
  //     for(int k = current_k + 1; k <= max_k; k++) {
  //       ++total_iterations;
  //       er = sr + k;
  //       ec = sc + k;
  //       mc = k >> 1;
  //       if (k % 2 == 1) ++mc;
  //
  //       good = true;
  //       for(int r = 0; r < k; r++) {
  //         if (debug) printf("sr = %d, sc = %d, r = %d\n", sr, sc, r);
  //         for(int c = 0; c < mc; c++) {
  //           if (debug) printf("Comparing %c == %c\n", board[sr + r][sc + c], board[er - r - 1][ec - c - 1]);
  //           ++total_checks;
  //           if (board[sr + r][sc + c] != board[er - r - 1][ec - c - 1]) {
  //             good = false;
  //             break;
  //           }
  //         }
  //         if (!good) break;
  //       }
  //       if (good) {
  //         // printf("Found solution at %d, %d size %d\n", sr, sc, k);
  //         current_k = std::max(current_k, k);
  //       }
  //     }
  //   }
  // }

  int top, left, bottom, right;
  bool good = true;

  //odd k
  int current_k = 1;
  int nmo = n - 1;
  int mmo = m - 1;

  for(int cr = 1; cr < nmo; cr++) {
    for(int cc = 1; cc < mmo; cc++) {
      max_k = (std::min(cr, std::min(cc, std::min(nmo - cr, mmo - cc))) << 1) + 1;
      top = cr - 1;
      left = cc - 1;
      bottom = cr + 1;
      right = cc + 1;
      if (debug) printf("(%d, %d) max_k = %d, current_k = %d\n", cr, cc, max_k, current_k);
      if (max_k <= current_k) continue;

      good = true;
      for(int k = 3; k <= max_k; k += 2) {
        ++total_iterations;
        if (debug) printf("\tk = %d\n", max_k);

        // compare left and right columns
        for(int r = 0; r < k; r++) {
          // printf("cr = %d, cc = %d, r = %d\n", cr, cc, r);
          if (debug) printf("\tComparing %c == %c\n", board[top + r][left], board[bottom - r][right]);
          ++total_checks;
          if (board[top + r][left] != board[bottom - r][right]) {
            good = false;
            break;
          }
          if (!good) break;
        }

        if (!good) break;
        if (debug) printf("\t========\n");

        // compare top and bottom rows if needed
        for(int c = 1; c < k - 1; c++) {
          // printf("cr = %d, cc = %d, r = %d\n", cr, cc, r);
          if (debug) printf("\tComparing %c == %c\n", board[top][left + c], board[bottom][right - c]);
          ++total_checks;
          if (board[top][left + c] != board[bottom][right - c]) {
            good = false;
            break;
          }
          if (!good) break;
        }

        if (good) {
          current_k = std::max(current_k, k);
          if (debug) printf("\tFound new solution, k = %d!\n", k);
          --top;
          --left;
          ++bottom;
          ++right;
        } else {
          break;
        }
      }
    }
  }

  //even K. (cr, cc) are middle point of each square
  int last_k = current_k;
  --current_k;
  for(int cr = 1; cr < n; cr++) {
    for(int cc = 1; cc < m; cc++) {
      max_k = std::min(cr, std::min(cc, std::min(n - cr, m - cc))) << 1;
      top = cr - 1;
      left = cc - 1;
      bottom = cr;
      right = cc;
      if (debug) printf("(%d, %d) max_k = %d, current_k = %d\n", cr, cc, max_k, current_k);
      if (max_k <= current_k) continue;

      good = true;
      for(int k = 2; k <= max_k; k += 2) {
        ++total_iterations;
        if (debug) printf("\tk = %d\n", k);

        // compare left and right columns
        for(int r = 0; r < k; r++) {
          // printf("cr = %d, cc = %d, r = %d\n", cr, cc, r);
          if (debug) printf("\tComparing %c == %c\n", board[top + r][left], board[bottom - r][right]);
          ++total_checks;
          if (board[top + r][left] != board[bottom - r][right]) {
            good = false;
            break;
          }
          if (!good) break;
        }

        if (!good) break;
        if (debug) printf("\t========\n");

        // compare top and bottom rows if needed
        for(int c = 1; c < k - 1; c++) {
          // printf("cr = %d, cc = %d, r = %d\n", cr, cc, r);
          if (debug) printf("\tComparing %c == %c\n", board[top][left + c], board[bottom][right - c]);
          ++total_checks;
          if (board[top][left + c] != board[bottom][right - c]) {
            good = false;
            break;
          }
          if (!good) break;
        }
        // if (good) {
        //   // printf("Found solution at %d, %d size %d\n", cr, cc, k);
        //   current_k = std::max(current_k, k);
        // }
        if (good) {
          current_k = std::max(current_k, k);
          --top;
          --left;
          ++bottom;
          ++right;
        } else {
          break;
        }
      }
    }
  }

  if (debug)
    for(int r = 0; r < n; r++) {
      for(int c = 0; c < m; c++) {
        printf("%c ", board[r][c]);
      }
      printf("\n");
    }

  if (debug) printf("Total iterations: %lu\n", total_iterations);
  if (debug) printf("Total checks: %lu\n", total_checks);
  printf("%d\n", std::max(current_k, last_k));
  return 0;
}
