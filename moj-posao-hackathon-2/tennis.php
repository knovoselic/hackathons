<?php

$n = intval(trim(fgets(STDIN, 10240)));

$x_wins = 0;
$y_wins = 0;

for($i = 0; $i < $n; $i++) {
  $results = array_map('intval', explode(":", trim(fgets(STDIN, 10240))));
  if ($results[0] > $results[1]) {
    $x_wins = $x_wins + 1;
  } else {
    $y_wins = $y_wins + 1;
  }
}

fprintf(STDOUT, "%d:%d\n", $x_wins , $y_wins);
?>
