var MAX = 10000;
var MOD = 1000000007;

var solution = 0;

function calculate_one(va) {
  var sum = 0;
  var squares = 0;
  var temp;
  var count = va.length;
  var size = count;

  for(var i = 0; i < size; ++i) {
    temp = va[i];
    sum = (sum + temp);
    squares = (squares + temp*temp);
  }

  for(var i = 0; i < size; ++i) {
    temp = va[i];
    --count;
    sum -= temp;
    squares -= temp*temp;
    solution = (solution + count * temp * temp - (temp<<1)*sum + squares);
  }
}

function calculate(va, vb) {
  if (va.length > vb.length) {
    bigger = va;
    smaller = vb;
  } else {
    bigger = vb;
    smaller = va;
  }
  sum = 0;
  squares = 0;
  temp;
  count = bigger.length;

  current_size = count;
  for(var i = 0; i < current_size; ++i) {
    temp = bigger[i];
    sum = sum + temp;
    squares = squares + temp*temp;
  }

  current_size = smaller.length;
  for(var i = 0; i < current_size; ++i) {
    temp = smaller[i];
    solution = (solution + count * temp * temp - (temp<<1)*sum + squares);
  }
}


var n = Number(readline().trim());
var positions = [];
var numbers = readline().trim().split(" ").map(Number);
var minv = 999999;
var maxv = 0;

for(var i = 0; i < n; i++) {
  temp = numbers[i];
  if (positions[temp] === undefined) positions[temp] = [];
  positions[temp].push(i);
  if (temp > maxv) maxv = temp;
  if (temp < minv) minv = temp;
}

for(var i = minv; i <= maxv; i++) {
  for(var j = minv; j < i; j++) {
    if ((i & j) != j) continue;
    if (positions[i] == undefined) continue;
    if (positions[j] == undefined) continue;
    calculate(positions[i], positions[j]);
  }
}

for(var i = minv; i <= maxv; ++i) {
  if (positions[i] === undefined || positions[i].length == 1) continue;
  // ovo traje jako kratko, naravno da se optimizirat kak treba ako bude potrebno
  calculate_one(positions[i]);
  calculate_one(positions[i]);
}

print(solution % MOD);
