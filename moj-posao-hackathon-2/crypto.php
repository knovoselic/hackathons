<?php
$encoded = trim(fgets(STDIN, 10000000));
$n = strlen($encoded);
$decoded = str_repeat(" ", $n + 1);

$start = 0;
$index = 0;
$step = 2;

while($start < $n) {
    for($i = $start; $i < $n; $i += $step) {
        $decoded[$i] = $encoded[$index];
        $encoded[$index++] = '.';
    }
    while($decoded[$start] != ' ') ++$start;
    $step = $step << 1;
}

fprintf(STDOUT, "%s\n", substr($decoded, 0, $n));
?>
