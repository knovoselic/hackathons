<?php

function prime_factors($n) {
  $count = 0;

  while($n % 2 == 0) {
    ++$count;
    $n /= 2;
  }

  for($i = 3; $i <= sqrt($n); $i += 2) {
    while ($n%$i == 0) {
      ++$count;
      $n /= $i;
    }
  }

  if ($n > 2) ++$count;


  return $count;
}

$solution = 0;

fscanf(STDIN, "%d", $n);

$numbers = array_map('intval', explode(" ", trim(fgets(STDIN, 102400))));
for($i = 0; $i < $n; $i++) {
  $solution += prime_factors($numbers[$i]);
}

if ($solution % 2 == 0) {
  fprintf(STDOUT, "Slavko\n");
} else {
  fprintf(STDOUT, "Mirko\n");
}

fprintf(STDOUT, "%d\n", $solution);
?>
