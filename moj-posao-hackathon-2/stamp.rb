#!/usr/bin/env ruby
RANGE = 5
MAX = 20
File.open('stamp_input4', 'w') do |f|
  n = rand(RANGE)
  f.puts n
  n.times do
    x = rand(0..MAX)
    y = rand(0..MAX)
    f.puts "#{x} #{y} #{rand((x+1)..MAX)} #{rand((y+1)..MAX)}" 
  end
  n = rand(RANGE)
  f.puts n
  n.times do
    x = rand(0..MAX)
    y = rand(0..MAX)
    f.puts "#{x} #{y} #{rand((x+1)..MAX)} #{rand((y+1)..MAX)}" 
  end
end
