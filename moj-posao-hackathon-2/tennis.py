import fileinput

lines = iter(fileinput.input())
next(lines)
x_wins = 0
y_wins = 0
for line in lines:
    x, y = [int(x) for x in line.strip().split(":")]
    if x > y:
        x_wins = x_wins + 1
    else:
        y_wins = y_wins + 1
print("{}:{}".format(x_wins, y_wins))
