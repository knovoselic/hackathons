<?php

$board = array_fill(0, 15, array_fill(0, 15, '.'));

$piece_name[64];
$solution = 0;

fscanf(STDIN, "%d %d", $n, $k);
for($i = 0; $i < $k; $i++) {
  fscanf(STDIN, "%s %d %d", $piece_name, $row, $col);
  $board[--$row][--$col] = $piece_name[0];
}

for($row = 0; $row < $n; $row++) {
  for($col = 0; $col < $n; $col++) {
    if ($board[$row][$col] == 'r') {
      for($r = $row - 1; $r >= 0; --$r) {
        if ($board[$r][$col] == 'b' || $board[$r][$col] == 'r') break;
        $board[$r][$col] = '#';
      }
      for($r = $row + 1; $r < $n; ++$r) {
        if ($board[$r][$col] == 'b' || $board[$r][$col] == 'r') break;
        $board[$r][$col] = '#';
      }
      for($c = $col - 1; $c >= 0; --$c) {
        if ($board[$row][$c] == 'b' || $board[$row][$c] == 'r') break;
        $board[$row][$c] = '#';
      }
      for($c = $col + 1; $c < $n; ++$c) {
        if ($board[$row][$c] == 'b' || $board[$row][$c] == 'r') break;
        $board[$row][$c] = '#';
      }
    } else if ($board[$row][$col] == 'b') {
      $r = $row;
      $c = $col;

      while(--$r >= 0 && --$c >= 0) {
        if ($board[$r][$c] == 'b' || $board[$r][$c] == 'r') break;
        $board[$r][$c] = '#';
      }
      $r = $row;
      $c = $col;
      while(--$r >= 0 && ++$c < $n) {
        if ($board[$r][$c] == 'b' || $board[$r][$c] == 'r') break;
        $board[$r][$c] = '#';
      }
      $r = $row;
      $c = $col;
      while(++$r < $n && --$c >= 0) {
        if ($board[$r][$c] == 'b' || $board[$r][$c] == 'r') break;
        $board[$r][$c] = '#';
      }
      $r = $row;
      $c = $col;
      while(++$r < $n && ++$c < $n) {
        if ($board[$r][$c] == 'b' || $board[$r][$c] == 'r') break;
        $board[$r][$c] = '#';
      }
    }
  }
}

for($row = 0; $row < $n; $row++) {
  for($col = 0; $col < $n; $col++) {
    $width = 0;
    $height = 0;
    $max_width = 0;
    $max_height = 0;

    while($col + $max_width < $n && $board[$row][$col + $max_width] == '.') ++$max_width;
    while($row + $max_height < $n && $board[$row + $max_height][$col] == '.') ++$max_height;

    $abort = false;
    for($height = 1; $height < $max_height; $height++) {
      for($i = 0; $i < $max_width; $i++) {
        if ($board[$row + $height][$col + $i] != '.') {
          $abort = true;
          break;
        }
      }
      if ($abort) break;
    }
    $solution = max($solution, $max_width * $height);

    $abort = false;
    for($width = 1; $width < $max_width; $width++) {
      for($i = 0; $i < $max_height; $i++) {
        if ($board[$row + $i][$col + $width] != '.') {
          $abort = true;
          break;
        }
      }
      if ($abort) break;
    }
    $solution = max($solution, $width * $max_height);
  }
}
fprintf(STDOUT, "%d\n", $solution);

?>
