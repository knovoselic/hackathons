import java.io.*;
import java.util.*;

public class Main {
  public static void main(String[] args) {
    Scanner in = new Scanner(System.in);
    String input;
    Boolean caps_lock = false;
    String buffer = "";
    int num = in.nextInt();

    in.nextLine();

    for(int i = 0; i < num; i++) {
      input = in.nextLine().trim();
      if (input.equals("backspace")) {
        if (buffer.length() > 0) buffer = buffer.substring(0, buffer.length() - 1);
      } else if (input.equals("caps")) {
        caps_lock = !caps_lock;
      } else {
        buffer = buffer + (caps_lock ? Character.toUpperCase(input.charAt(0)) : input.charAt(0));
      }
    }
    System.out.println(buffer);
  }
}
