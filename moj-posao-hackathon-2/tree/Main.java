import java.io.*;
import java.util.*;

public class Main {
  public static final int MAX = 1001;
  public static char[][] lines = new char[MAX][MAX];
  public static ArrayList<Integer> a = new ArrayList<Integer>();
  public static int n;

  public static void branch(int x, int y, int dx, int dy, int level) {
    for(int i = 0; i < a.get(level); ++i) {
      x += dx;
      y += dy;
      lines[x][y] = '*';
    }

    ++level;
    if (level >= n) return;

    if (dx != 0 && dy != 0) {
      branch(x, y, dx, 0, level);
      branch(x, y, 0, dy, level);
    } else if (dx == 0) {
      branch(x, y, -1, dy, level);
      branch(x, y, 1, dy, level);
    } else {
      branch(x, y, dx, -1, level);
      branch(x, y, dx, 1, level);
    }
  }

  public static void main(String[] args) {
    Scanner in = new Scanner(System.in);
    n = in.nextInt();

    for(int i = 0; i < n; ++i) {
      a.add(in.nextInt());
    }

    for(int i = 0; i < MAX; ++i) {
      Arrays.fill(lines[i], '.');
    }

    int x = MAX, y = MAX / 2;
    for(int i = 0; i < a.get(0); ++i) {
      --x;
      lines[x][y] = '*';
    }

    branch(x, y, -1, -1, 1);
    branch(x, y, -1, 1, 1);

    int minx = MAX;
    int miny = MAX;
    int maxx = 0;
    int maxy = 0;

    for(int i = 0; i < MAX; ++i) {
      for(int j = 0; j < MAX; ++j) {
        if (lines[i][j] == '.') continue;
        minx = Math.min(minx, i);
        miny = Math.min(miny, j);
        maxx = Math.max(maxx, i);
        maxy = Math.max(maxy, j);
      }
    }

    for(int i = minx; i <= maxx; ++i) {
      for(int j = miny; j <= maxy; ++j) {
        System.out.print(lines[i][j]);
      }
      System.out.println();
    }
  }
}
