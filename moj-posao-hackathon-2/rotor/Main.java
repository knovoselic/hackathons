import java.io.*;
import java.util.*;

public class Main {
  public static Boolean is_pal(char board[][], int x, int y, int k) {
    for (int i = 0; i < k; ++i) {
      int yi = y + i;
      int yii = y - i - 1 + k;
      int xk = x + k - 1;
      for (int j = 0; j < k; ++j) {
        if (board[yi][x + j] != board[yii][xk- j]) return false;
      }
    }

    return true;
  }

  public static int solve(char board[][], int m, int n) {
    int max_k = Math.min(m, n);
    int current_k = max_k;

    while(true) {
      int mk = m - current_k;
      int nk = n - current_k;
      for (int x = 0; x <= mk; ++x) {
        for (int y = 0; y <= nk; ++y) {
          if (is_pal(board, x, y, current_k)) return current_k;
        }
      }

      --current_k;
    }
  }

  public static void main(String[] args) throws IOException {
    BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
    StringTokenizer st = new StringTokenizer(br.readLine());
    ArrayList<Integer> bottles = new ArrayList<Integer>();
    int n, m;
    int max_k;

    n = Integer.parseInt(st.nextToken());
    m = Integer.parseInt(st.nextToken());
    char board[][] = new char[n][1];

    for(int r = 0; r < n; r++) {
      board[r] = br.readLine().toCharArray();
    }

    System.out.println(solve(board, m, n));
  }
}
