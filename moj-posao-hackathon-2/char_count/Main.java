import java.io.*;
import java.util.*;

public class Main {
  private static final int NUMBER_OF_LETTERS = 26;

  public static void main(String[] args) {
    Scanner in = new Scanner(System.in);
    while(in.hasNextLine())
    {
      String input = in.nextLine();
      int[] counts = new int[NUMBER_OF_LETTERS];
      int i = 0;
      Boolean firstPrint = true;

      for(i = 0; i < input.length(); i++) {
        int index = Character.toLowerCase(input.charAt(i)) - 'a';
        if (index < 0 || index > 25) continue;
        counts[index]++;
      }


      for(i = 0; i < NUMBER_OF_LETTERS; i++) {
        if (counts[i] == 0) continue;
        if (!firstPrint) System.out.println();
        System.out.print(String.valueOf((char)(i + 'a')));
        System.out.print(":");
        System.out.print(counts[i]);
        firstPrint = false;
      }
    }
  }
}
