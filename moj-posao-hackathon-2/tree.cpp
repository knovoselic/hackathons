#include <iostream>
#include <string>
#include <vector>

#define MAX 1001

using namespace std;

vector<string> lines;
int n;

void branch(int x, int y, int dx, int dy, int level, const vector<int> &a) {
  for(int i = 0; i < a[level]; ++i) {
    x += dx;
    y += dy;
    lines[x][y] = level + 49;
  }

  ++level;
  if (level >= n) return;

  if (dx != 0 && dy != 0) {
    branch(x, y, dx, 0, level, a);
    branch(x, y, 0, dy, level, a);
  } else if (dx == 0) {
    branch(x, y, -1, dy, level, a);
    branch(x, y, 1, dy, level, a);
  } else {
    branch(x, y, dx, -1, level, a);
    branch(x, y, dx, 1, level, a);
  }
}

int main(void) {
  cin >> n;
  vector<int> a(n);

  for(int i = 0; i < n; ++i) {
    cin >> a[i];
  }

  for(int i = 0; i < MAX; ++i) {
    lines.push_back(string(MAX, '.'));
  }

  int x = MAX, y = MAX / 2;

  for(int i = 0; i < a[0]; ++i) {
    --x;
    lines[x][y] = 49;
  }

  branch(x, y, -1, -1, 1, a);
  branch(x, y, -1, 1, 1, a);

  int minx = MAX;
  int miny = MAX;
  int maxx = 0;
  int maxy = 0;

  for(int i = 0; i < MAX; ++i) {
    for(int j = 0; j < MAX; ++j) {
      if (lines[i][j] == '.') continue;
      minx = min(minx, i);
      miny = min(miny, j);
      maxx = max(maxx, i);
      maxy = max(maxy, j);
    }
  }

  for(int i = minx; i <= maxx; ++i) {
    for(int j = miny; j <= maxy; ++j) {
      if (lines[i][j] == '.') {
        cout << '.';
      } else {
        cout << '*';
      }
    }
    cout << endl;
  }

  return 0;
}
