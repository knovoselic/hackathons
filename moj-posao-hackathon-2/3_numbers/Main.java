import java.io.*;
import java.util.*;

public class Main {

  public static void main(String[] args) {
    for(int a = 0; a < 100; a++) {
      for(int b = 0; b < 100; b++) {
        for(int c = 0; c < 100; c++) {
          if (a + b + c == 194 && a * b * c == 229824) {
            System.out.format("[%d, %d, %d]\n", a, b, c);
            return;
          }
        }
      }
    }
  }
}
