<?php


$ci = $cj = $ni = $nj = 0;

function find_next($needle) {
  global $ci;
  global $cj;
  global $ni;
  global $nj;
    $numpad = array(
        array('1', '2', '3'),
        array('4', '5', '6'),
        array('7', '8', '9'),
        array('\0', '0', 'C')
    );

    for($i = 0; $i < 4; $i++) {
        for($j = 0; $j < 3; $j++) {
            if ($numpad[$i][$j] == $needle) {
                $ni = $i;
                $nj = $j;
                return;
            }
        }
    }
}

$number = trim(fgets(STDIN, 102400)) . 'C';


$number_length = strlen($number);
$solution = 0;

find_next('0');
$ci = $ni; $cj = $nj;
// printf("%d %d\n", $ci, $cj);
for($c = 0; $c < $number_length; ++$c) {
    find_next($number[$c]);
    $distance = abs($ci - $ni) + abs($cj - $nj) + 1;
    $solution += $distance;
    // printf("%d\n", distance);
    $ci = $ni; $cj = $nj;
}


fprintf(STDOUT, "%d\n", $solution);

?>
