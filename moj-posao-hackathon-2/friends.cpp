#include <iostream>
#include <queue>
#include <vector>

using namespace std;

vector<int> graph[4001];
vector<bool> seen;
int n, m;

int max_distance = 0;
int max_vertex = 0;

int bfs(int x) {
  queue<int> q;
  q.push(x);
  q.push(0);
  int nseen = 0;
  seen.assign(4001, false);

  while (!q.empty()) {
    int vertex = q.front();
    q.pop();
    int distance = q.front();
    q.pop();

    if(seen[vertex]) continue;

    ++nseen;
    seen[vertex] = true;

    if (distance > max_distance) {
      max_distance = distance;
      max_vertex = vertex;
    }

    for(unsigned long i = 0; i < graph[vertex].size(); i++) {
      if(!seen[graph[vertex][i]]) {
        q.push(graph[vertex][i]);
        q.push(distance + 1);
      }
    }
  }

  return nseen;
}

void add_edge(int a, int b) {
  graph[a].push_back(b);
  graph[b].push_back(a);
}

int sequence(int x) {
  if (x < 2) return 0;
  x -= 2;

  int current_value = 1;
  int current_step = 1;

  for(int i = 0; i < x; i += current_step) {
    ++current_value;
    current_step <<= 1;
  }

  return current_value;
}

int main(void) {
  int a, b;
  int start;

  cin >> n >> m;

  cin >> a >> b;
  start = a - 1;
  add_edge(a - 1, b - 1);
  for(int i = 1; i < m; i++) {
    cin >> a >> b;
    add_edge(a - 1, b - 1);
  }

  if (bfs(0) == n) {
    bfs(max_vertex);
    cout << sequence(max_distance) << endl;
  } else {
    cout << -1 << endl;
  }

  return 0;
}
