var board = Array(15);
for(var i = 0; i < 15; i++) {
  board[i] = ['.', '.', '.', '.', '.', '.', '.', '.', '.', '.', '.', '.', '.', '.', '.'];
}
var piece_name;
var n, k, row, col;
var solution = 0;

[n, k] = readline().trim().split(" ").map(Number);
for(var i = 0; i < k; i++) {
  [piece_name, row, col] = readline().trim().split(" ");
  [row, col] = [row, col].map(Number);
  board[--row][--col] = piece_name[0];
}

for(var row = 0; row < n; row++) {
  for(var col = 0; col < n; col++) {
    if (board[row][col] == 'r') {
      for(var r = row - 1; r >= 0; --r) {
        if (board[r][col] == 'b' || board[r][col] == 'r') break;
        board[r][col] = '#';
      }
      for(var r = row + 1; r < n; ++r) {
        if (board[r][col] == 'b' || board[r][col] == 'r') break;
        board[r][col] = '#';
      }
      for(var c = col - 1; c >= 0; --c) {
        if (board[row][c] == 'b' || board[row][c] == 'r') break;
        board[row][c] = '#';
      }
      for(var c = col + 1; c < n; ++c) {
        if (board[row][c] == 'b' || board[row][c] == 'r') break;
        board[row][c] = '#';
      }
    } else if (board[row][col] == 'b') {
      var r = row, c = col;

      while(--r >= 0 && --c >= 0) {
        if (board[r][c] == 'b' || board[r][c] == 'r') break;
        board[r][c] = '#';
      }
      r = row;
      c = col;
      while(--r >= 0 && ++c < n) {
        if (board[r][c] == 'b' || board[r][c] == 'r') break;
        board[r][c] = '#';
      }
      r = row;
      c = col;
      while(++r < n && --c >= 0) {
        if (board[r][c] == 'b' || board[r][c] == 'r') break;
        board[r][c] = '#';
      }
      r = row;
      c = col;
      while(++r < n && ++c < n) {
        if (board[r][c] == 'b' || board[r][c] == 'r') break;
        board[r][c] = '#';
      }
    }
  }
}

for(var row = 0; row < n; row++) {
  for(var col = 0; col < n; col++) {
    var width = 0;
    var height = 0;
    var max_width = 0;
    var max_height = 0;
    var abort;

    while(col + max_width < n && board[row][col + max_width] == '.') ++max_width;
    while(row + max_height < n && board[row + max_height][col] == '.') ++max_height;

    abort = false;
    for(height = 1; height < max_height; height++) {
      for(var i = 0; i < max_width; i++) {
        if (board[row + height][col + i] != '.') {
          abort = true;
          break;
        }
      }
      if (abort) break;
    }
    solution = Math.max(solution, max_width * height);

    abort = false;
    for(width = 1; width < max_width; width++) {
      for(var i = 0; i < max_height; i++) {
        if (board[row + i][col + width] != '.') {
          abort = true;
          break
        }
      }
      if (abort) break;
    }
    solution = Math.max(solution, width * max_height);
  }
}
// print();
// for(var i = 0; i < n; i++) {
//   for(var j = 0; j < n; j++) {
//     putstr(board[i][j] + ' ');
//   }
//   print();
// }

print(solution);
