var n;

function find_solution(pattern) {
  mem = [0, 0, 0, 0, 0, 0];

  for(var i = 0; i < n; i++) {
    var current = pattern[i % 3];
    if (input_string[i] == 'A' && current == 'B') {
      ++mem[0];
    } else if (input_string[i] == 'A' && current == 'C') {
      ++mem[1];
    } else if (input_string[i] == 'B' && current == 'A') {
      ++mem[2];
    } else if (input_string[i] == 'B' && current == 'C') {
      ++mem[3];
    } else if (input_string[i] == 'C' && current == 'A') {
      ++mem[4];
    } else if (input_string[i] == 'C' && current == 'B') {
      ++mem[5];
    }
  }

  return Math.min(mem[0], mem[2]) +
         Math.min(mem[1], mem[4]) +
         Math.min(mem[3], mem[5]) +
         Math.abs(mem[0] - mem[2]) * 2;
}

n = Number(readline().trim());
input_string = readline().trim();

n *= 3;
var minimum = n + 10;

minimum = Math.min(minimum, find_solution("ABC"));
minimum = Math.min(minimum, find_solution("ACB"));
minimum = Math.min(minimum, find_solution("BAC"));
minimum = Math.min(minimum, find_solution("BCA"));
minimum = Math.min(minimum, find_solution("CAB"));
minimum = Math.min(minimum, find_solution("CBA"));

print(minimum);
