var MAX = 1000;

var lines = [];
var n;
var a;

function branch(x, y, dx, dy, level) {
  for(var i = 0; i < a[level]; ++i) {
    x += dx;
    y += dy;
    lines[x][y] = '*';
  }

  ++level;
  if (level >= n) return;

  if (dx != 0 && dy != 0) {
    branch(x, y, dx, 0, level, a);
    branch(x, y, 0, dy, level, a);
  } else if (dx == 0) {
    branch(x, y, -1, dy, level, a);
    branch(x, y, 1, dy, level, a);
  } else {
    branch(x, y, dx, -1, level, a);
    branch(x, y, dx, 1, level, a);
  }
}

n = Number(readline().trim());
a = readline().trim().split(" ").map(Number);


for(var i = 0; i < MAX; ++i) {
  lines[i] = Array(MAX + 1).join('.').split('');
}

var x = MAX, y = MAX / 2;

for(var i = 0; i < a[0]; ++i) {
  --x;
  lines[x][y] = '*';
}

branch(x, y, -1, -1, 1);
branch(x, y, -1, 1, 1);

var minx = MAX;
var miny = MAX;
var maxx = 0;
var maxy = 0;

for(var i = 0; i < MAX; ++i) {
  for(var j = 0; j < MAX; ++j) {
    if (lines[i][j] == '.') continue;
    minx = Math.min(minx, i);
    miny = Math.min(miny, j);
    maxx = Math.max(maxx, i);
    maxy = Math.max(maxy, j);
  }
}

for(var i = minx; i <= maxx; ++i) {
  for(var j = miny; j <= maxy; ++j) {
    putstr(lines[i][j]);
  }
  print();
}
