<?php

$graph = array();
for($i = 0; $i < 4001; ++$i) {
  array_push($graph, array());
}
$max_distance = 0;
$max_vertex = 0;

function bfs($x) {
  global $max_distance;
  global $max_vertex;
  global $graph;

  $q = array();
  array_push($q, $x);
  array_push($q, 0);
  $nseen = 0;
  $seen = array();
  for($i = 0; $i < 4001; ++$i) {
    array_push($seen, false);
  }

  while (sizeof($q) > 0) {
    $vertex = array_shift($q);
    $distance = array_shift($q);

    if($seen[$vertex]) continue;

    ++$nseen;
    $seen[$vertex] = true;

    if ($distance > $max_distance) {
      $max_distance = $distance;
      $max_vertex = $vertex;
    }

    for($i = 0; $i < sizeof($graph[$vertex]); $i++) {
      if(!$seen[$graph[$vertex][$i]]) {
        array_push($q, $graph[$vertex][$i]);
        array_push($q, $distance + 1);
      }
    }
  }

  return $nseen;
}

function add_edge($a, $b) {
  global $graph;

  array_push($graph[$a], $b);
  array_push($graph[$b], $a);
}

function sequence($x) {
  if ($x < 2) return 0;
  $x -= 2;

  $current_value = 1;
  $current_step = 1;

  for($i = 0; $i < $x; $i += $current_step) {
    ++$current_value;
    $current_step <<= 1;
  }

  return $current_value;
}

fscanf(STDIN, "%d %d", $n, $m);

for($i = 0; $i < $m; $i++) {
  fscanf(STDIN, "%d %d", $a, $b);
  add_edge($a - 1, $b - 1);
}

if (bfs(0) == $n) {
  bfs($max_vertex);
  fprintf(STDOUT, "%d\n", sequence($max_distance));
} else {
  fprintf(STDOUT, "-1\n");
}
