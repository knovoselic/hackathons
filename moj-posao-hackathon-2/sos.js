var numpad = [
    ['1', '2', '3'],
    ['4', '5', '6'],
    ['7', '8', '9'],
    ['\0', '0', 'C']
];

var ci, cj, ni, nj;

function find_next(needle) {
    for(var i = 0; i < 4; i++) {
        for(var j = 0; j < 3; j++) {
            if (numpad[i][j] == needle) {
                ni = i;
                nj = j;
                return;
            }
        }
    }
}

number = readline().trim() + 'C';
var number_length = number.length;
var solution = 0;

find_next('0');
ci = ni; cj = nj;
// printf("%d %d\n", ci, cj);
for(var c = 0; c < number_length; ++c) {
    find_next(number[c]);
    var distance = Math.abs(ci - ni) + Math.abs(cj - nj) + 1;
    solution += distance;
    // printf("%d\n", distance);
    ci = ni; cj = nj;
}


print(solution);
