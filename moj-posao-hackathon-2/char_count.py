import fileinput

NUMBER_OF_LETTERS = 26
counts = [0] * NUMBER_OF_LETTERS
for line in fileinput.input():
    line = line.lower()
    for c in line:
        index = ord(c) - 97
        if (index < 0 or index > 25): continue;
        counts[index] += 1

    firstPrint = True;
    for i in range(0, NUMBER_OF_LETTERS):
        if (counts[i] == 0): continue
        if (not firstPrint): print()
        print(chr(i + 97), end = '')
        print(':', end = '')
        print(counts[i], end = '')
        firstPrint = False
