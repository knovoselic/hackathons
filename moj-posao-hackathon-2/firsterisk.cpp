#include <cstdio>
#include <string.h>

int main(void) {
  char input[10240];

  scanf("%s", input);
  printf("%c", input[0]);
  for(int i = 1; i < strlen(input); i++) {
    if(input[i] == input[0]) {
      printf("*");
    } else {
      printf("%c", input[i]);
    }
  }
  printf("\n");

  return 0;
}
