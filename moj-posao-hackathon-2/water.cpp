#include <cstdio>
#include <cstdlib>

int bottles_comparator(const void *pa, const void *pb) {
    int a = *(int *)pa;
    int b = *(int *)pb;
    if (a > b) {
        return 1;
    } else if (a == b) {
        return 0;
    } else {
        return -1;
    }
}

int main(void) {
    int n, x, i;
    int bottles[100] = {0};
    int solution = 0;

    scanf("%d %d", &n, &x);
    for(i = 0; i < n; i++) {
        scanf("%d", bottles + i);
    }

    qsort(bottles, n, sizeof(bottles[0]), bottles_comparator);
    i = 0;
    while (x > 0 && i < n) {
        x -= bottles[i];
        if (x < 0 ) break;
        ++solution;
        ++i;
    }
    printf("%d\n", solution);
}
