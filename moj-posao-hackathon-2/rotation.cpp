#include <iostream>

using namespace std;

int mat[51][51] = {{0}};
int n;

void pprint() {
  for(int i = 0; i < n; i++) {
    for (int j = 0; j < n; j++) {
      cout << mat[i][j];
      if (j < n - 1) cout << ' ';
    }
    cout << endl;
  }
  cout << endl;
}
void rotate(int N, int x, int y) {
  // cout << "Called\n";
  for (int i = 0; i < N / 2; ++i) {
    for (int j = i; j < N - i - 1; ++j) {
        // store current cell in temp variable
        int temp = mat[i + x][j + y];
        // cout << "Current " << temp << endl;

        // move values from right to top
        mat[i + x][j + y] = mat[j + x][N - 1 - i + y];
        // cout << "Right to top " << mat[i + x][j + y] << endl;

        // move values from bottom to right
        mat[j + x][N - 1 - i + y] = mat[N - 1 - i + x][N - 1 - j + y];

        // move values from left to bottom
        mat[N - 1 - i + x][N - 1 - j + y] = mat[N - 1 - j + x][i + y];

        // assign temp to left
        mat[N - 1 - j + x][i + y] = temp;
        // pprint();
    }
  }
}

int main(void) {
  int q;
  // d[0] = i, d[1] = j, d[2] = A, d[3] = K
  int d[50][4] = {{0}};

  cin >> n;

  for(int i = 0; i < n; i++) {
    for (int j = 0; j < n; j++) {
      cin >> mat[i][j];
    }
  }

  cin >> q;

  for(int i = 0; i < q; i++) {
    for (int j = 0; j < 4; j++) {
      cin >> d[i][j];
    }
  }

  for(int i = q - 1; i >= 0; --i) {
    // pprint();

    int x = d[i][0] - 1;
    int y = d[i][1] - 1;
    int a = d[i][2];
    int k = d[i][3] % 4;

    // cout << x << endl;
    // cout << y << endl;
    // cout << a << endl;
    // cout << k << endl;

    while(k--) rotate(a, x, y);
  }

  pprint();
}
