function print_padded(line, alignment, w) {
  if (alignment == 'right') {
    putstr(Array(w - line.length + 1).join('.'));
    print(line);
  } else if (alignment == 'left') {
    putstr(line);
    print(Array(w - line.length + 1).join('.'));
  } else {
    var diff = w - line.length;
    var spaces = Math.floor(diff / 2) + 1;
    putstr(Array(spaces).join('.'));
    putstr(line);
    putstr(Array(spaces).join('.'));
    if (diff % 2 == 1) putstr('.');
    print();
  }
}

var w = Number(readline());
var alignment = readline().trim();
var words = readline().trim().split(" ");

var current_width = 0
var line = '';

while (words.length > 0) {
    if (line == '') {
      line = words.shift();
    } else if ((line.length + words[0].length + 1) <= w) {
      line = line + '.' + words.shift();
    } else {
      print_padded(line, alignment, w)
      line = '';
    }
}

if (line != '') print_padded(line, alignment, w);
