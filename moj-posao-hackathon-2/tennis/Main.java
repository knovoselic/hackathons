import java.io.*;
import java.util.*;
import java.util.regex.Pattern;

public class Main {

  public static void main(String[] args) {
    Scanner in = new Scanner(System.in);
    int x_wins = 0;
    int y_wins = 0;
    int x, y;
    int n = in.nextInt();
    in.nextLine();
    in.useDelimiter(Pattern.compile(":|\n"));

    for(int i = 0; i < n; i++) {
      x = in.nextInt();
      y = in.nextInt();
      if (x > y) {
        x_wins += 1;
      } else {
        y_wins += 1;
      }
    }
    System.out.println(x_wins + ":" + y_wins);
  }
}
