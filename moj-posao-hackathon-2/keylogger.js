readline();
buffer = [];
var caps_lock = false;
while (line = readline()) {
  line = line.trim();
  if (line == 'backspace') {
    if (buffer.length > 0) buffer.pop();
  } else if (line == 'caps') {
    caps_lock = !caps_lock;
  } else {
    var char = caps_lock ? line[0].toUpperCase() : line[0].toLowerCase();
    buffer.push(char)
  }
}
print(buffer.join(''));
