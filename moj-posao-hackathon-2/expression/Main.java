import java.io.*;
import java.util.*;

public class Main {

  public static void burek(int x) {
    if (x < 0) {
      System.out.print('-');
    } else {
      System.out.print('+');
    }
  }

  public static void main(String[] args) {
    Scanner in = new Scanner(System.in);

    in.nextInt();
    burek(in.nextInt());
    burek(in.nextInt());
    burek(in.nextInt());

    System.out.println();
  }
}
