var primes = {};

function prime_factors(n) {
  while(n % 2 == 0) {
    if (!(2 in primes)) primes[2] = 0;
    primes[2]++;
    n /= 2;
  }

  for(var i = 3; i <= Math.sqrt(n); i += 2) {
    while (n%i == 0) {
      if (!(i in primes)) primes[i] = 0;
      primes[i]++;
      n /= i;
    }
  }

  if (n > 1) {
    if (!(n in primes)) primes[n] = 0;
    primes[n]++;
  }
}

var n;
var count = 1;

n = Number(readline());
prime_factors(n);

for (var property in primes) {
  count *= (primes[property] + 1);
}

print(count);
