<?php

function print_padded($line, $alignment, $w) {
  if ($alignment == 'right') {
    echo(str_repeat('.', $w - strlen($line)));
    echo($line);
    echo("\n");
  } else if ($alignment == 'left') {
    echo($line);
    echo(str_repeat('.', $w - strlen($line)));
    echo("\n");
  } else {
    $diff = $w - strlen($line);
    $spaces = floor($diff / 2);
    echo(str_repeat('.', $spaces));
    echo($line);
    echo(str_repeat('.', $spaces));
    if ($diff % 2 == 1) echo('.');
    echo("\n");
  }
}

fscanf(STDIN, "%d", $w);
$alignment = trim(fgets(STDIN, 102400));
$words = explode(" ", trim(fgets(STDIN, 102400)));

$current_width = 0;
$line = '';

while (sizeof($words) > 0) {
    if ($line == '') {
      $line = array_shift($words);
    } else if ((strlen($line) + strlen($words[0]) + 1) <= $w) {
      $line = $line . '.' . array_shift($words);
    } else {
      print_padded($line, $alignment, $w);
      $line = '';
    }
}

if ($line != '') print_padded($line, $alignment, $w);
