import fileinput

for line in fileinput.input():
    input = [int(x) for x in line.strip().split(" ")]
    len = len(input)
    i = 0
    while i < len:
        current = input[i]
        print("%d " % current, end="")
        i += 1
        while i < len and input[i] == current: i += 1
    print()
