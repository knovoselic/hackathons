#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <algorithm>

using namespace std;

char input_string[300001];
int n, minimum;

int find_solution(const char pattern[3]) {
  int mem[6] = {0};

  for(int i = 0; i < n; i++) {
    char current = pattern[i % 3];
    if (input_string[i] == 'A' && current == 'B') {
      ++mem[0];
    } else if (input_string[i] == 'A' && current == 'C') {
      ++mem[1];
    } else if (input_string[i] == 'B' && current == 'A') {
      ++mem[2];
    } else if (input_string[i] == 'B' && current == 'C') {
      ++mem[3];
    } else if (input_string[i] == 'C' && current == 'A') {
      ++mem[4];
    } else if (input_string[i] == 'C' && current == 'B') {
      ++mem[5];
    }
  }

  return min(mem[0], mem[2]) + min(mem[1], mem[4]) + min(mem[3], mem[5]) + abs(mem[0] - mem[2]) * 2;
}

int main(void) {
    scanf("%d %s", &n, input_string);

    n *= 3;
    minimum = n + 10;

    minimum = min(minimum, find_solution("ABC"));
    minimum = min(minimum, find_solution("ACB"));
    minimum = min(minimum, find_solution("BAC"));
    minimum = min(minimum, find_solution("BCA"));
    minimum = min(minimum, find_solution("CAB"));
    minimum = min(minimum, find_solution("CBA"));

    printf("%d\n", minimum);

    return 0;
}
