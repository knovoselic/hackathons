#include <cstdio>

int main(void) {
  int n, x, temp;

  scanf("%d %d", &n, &x);
  while(--n) {
    scanf("%d", &temp);
    x -= temp;
  }

  printf("%d\n", x);
  return 0;
}
