<?php
$n = 0;
$area = 0.0;
$rects = array();

fscanf(STDIN, "%d", $n);

for($i = 0; $i < $n; $i++) {
  fscanf(STDIN, "%d %d %d %d", $x1, $y1, $x2, $y2);
  $rects[$i] = [$x1, $y1, $x2, $y2];
  $area += ($x2 - $x1) * ($y2 - $y1);
}

fscanf(STDIN, "%d", $m);
for($i = 0; $i < $m; $i++) {
  fscanf(STDIN, "%d %d %d %d", $x1, $y1, $x2, $y2);
  $area += ($x2 - $x1) * ($y2 - $y1);
  for($j = 0; $j < $n; $j++) {
    $rect = $rects[$j];
    $x_overlap = max(0, min($rect[2], $x2) - max($rect[0], $x1));
    $y_overlap = max(0, min($rect[3], $y2) - max($rect[1], $y1));
    $area -= $x_overlap * $y_overlap;
  }
}

fprintf(STDOUT, "%ld\n", $area);

?>
