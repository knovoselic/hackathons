var w1 = readline().trim();
var w2 = readline().trim();

var letters = [];

for(var i = 0; i < w1.length; i++) {
  letters[w1[i]] |= 1;
}
for(var i = 0; i < w2.length; i++) {
  letters[w2[i]] |= 2;
}

for(var i = 0; i < w1.length; i++) {
  if (letters[w1[i]] == 3) {
    putstr('!');
  } else {
    putstr(w1[i]);
  }
}
print();

for(var i = 0; i < w2.length; i++) {
  if (letters[w2[i]] == 3) {
    putstr('!');
  } else {
    putstr(w2[i]);
  }
}

print();
