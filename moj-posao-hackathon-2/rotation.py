def rotate(N, x, y):
    global mat

    for i in range(N // 2):
        for j in range(i, N - i - 1):
            temp = mat[i + x][j + y]

            mat[i + x][j + y] = mat[j + x][N - 1 - i + y]
            mat[j + x][N - 1 - i + y] = mat[N - 1 - i + x][N - 1 - j + y]
            mat[N - 1 - i + x][N - 1 - j + y] = mat[N - 1 - j + x][i + y]
            mat[N - 1 - j + x][i + y] = temp


n = int(input().strip())
mat = [0] * n

for i in range(n):
    mat[i] = [int(x) for x in input().strip().split(" ")]

q = int(input().strip())
d = [0] * q

for i in range(q):
    d[i] = [int(x) for x in input().strip().split(" ")]

for i in range(q - 1, -1, -1):
    x = d[i][0] - 1
    y = d[i][1] - 1
    a = d[i][2]
    k = d[i][3] % 4

    while (k > 0):
        k -= 1
        rotate(a, x, y)

for i in range(n):
    for j in range(n):
        print(mat[i][j], end = '')
        if j < n - 1: print(' ', end = '')
    print()

print()
