import fileinput

buffer = []
caps_lock = False
first = True
for line in fileinput.input():
    line = line.lower().strip()
    if first:
        first = False
        continue
    if line == 'backspace':
        if len(buffer) > 0: buffer.pop()
    elif line == 'caps':
        caps_lock = not caps_lock
    else:
        char = line[0].upper() if caps_lock else line[0].lower()
        buffer.append(char)

print(''.join(buffer))
