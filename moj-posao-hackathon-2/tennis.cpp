#include <cstdio>
#include <cstring>
#include <ctype.h>

int main(void) {
  int n, x, y;
  int x_wins = 0;
  int y_wins = 0;

  scanf("%d", &n);
  for(int i = 0; i < n; i++) {
    scanf("%d:%d", &x, &y);
    if (x > y) {
      x_wins = x_wins + 1;
    } else {
      y_wins = y_wins + 1;
    }
  }

  printf("%d:%d\n", x_wins, y_wins);

  return 0;
}
