import fileinput

area = 0
rects = []
file = iter(fileinput.input())


n = int(next(file).strip())

for i in range(0, n):
    x1, y1, x2, y2 = [int(x) for x in next(file).strip().split(" ")]
    rects.append([x1, y1, x2, y2])
    area += (x2 - x1) * (y2 - y1)

m = int(next(file).strip())
for i in range(0, m):
    x1, y1, x2, y2 = [int(x) for x in next(file).strip().split(" ")]
    area += (x2 - x1) * (y2 - y1)
    for j in range(0, n):
        rect = rects[j]
        x_overlap = max(0, min(rect[2], x2) - max(rect[0], x1));
        y_overlap = max(0, min(rect[3], y2) - max(rect[1], y1));
        area -= x_overlap * y_overlap;

print(area);
