<?php

function is_pal($board, $x, $y, $k) {
  for ($i = 0; $i < $k; $i++) {
    $yi = $y + $i;
    $yii = $y - $i - 1 + $k;
    $xk = $x + $k - 1;
    for ($j = 0; $j < $k; $j++) {
      if ($board[$yi][$x + $j] != $board[$yii][$xk- $j]) return false;
    }
  }

  return true;
}

function solve($board, $m, $n) {
  $current_k = $max_k = min($m, $n);
  while(true) {
    for ($x = 0; $x + $current_k <= $m; $x++) {
      for ($y = 0; $y + $current_k <= $n; $y++) {
        if (is_pal($board, $x, $y, $current_k)) return $current_k;
      }
    }

    --$current_k;
  }

  return 1;
}

fscanf(STDIN, "%d %d", $n, $m);
$board = array();

for($r = 0; $r < $n; $r++) {
  $board[$r] = str_split(trim(fgets(STDIN, $m + 1)));
  fgetc(STDIN);
}

fprintf(STDOUT, "%d\n", solve($board, $m, $n));
