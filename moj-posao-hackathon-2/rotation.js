function rotate(N, x, y) {
  for (var i = 0; i < N / 2; ++i) {
    for (var j = i; j < N - i - 1; ++j) {
        var temp = mat[i + x][j + y];

        mat[i + x][j + y] = mat[j + x][N - 1 - i + y];
        mat[j + x][N - 1 - i + y] = mat[N - 1 - i + x][N - 1 - j + y];
        mat[N - 1 - i + x][N - 1 - j + y] = mat[N - 1 - j + x][i + y];
        mat[N - 1 - j + x][i + y] = temp;
    }
  }
}

var d = [];
var mat = [];

var n = Number(readline().trim());

for(var i = 0; i < n; i++) {
  mat[i] = readline().trim().split(" ").map(Number);
}

var q = Number(readline().trim());

for(var i = 0; i < q; i++) {
  d[i] = readline().trim().split(" ").map(Number);
}

for(var i = q - 1; i >= 0; --i) {
  var x = d[i][0] - 1;
  var y = d[i][1] - 1;
  var a = d[i][2];
  var k = d[i][3] % 4;

  while(k--) rotate(a, x, y);
}

for(var i = 0; i < n; i++) {
  for (var j = 0; j < n; j++) {
    putstr(mat[i][j]);
    if (j < n - 1) putstr(' ');
  }
  print();
}
print();
