var encoded = readline().trim().slice('');

var n = encoded.length;
var start = 0;
var index = 0;
var step = 2;
var decoded = [];

for(var i = 0; i < n+1; i++) decoded[i] = ' ';

while(start < n) {
    for(var i = start; i < n; i += step) {
        decoded[i] = encoded[index];
        encoded[index++] = '.';
    }
    while(decoded[start] != ' ') ++start;
    step = step << 1;
}
decoded.splice(-1, 1);

print(decoded.join(''));
