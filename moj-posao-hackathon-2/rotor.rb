#!/usr/bin/env ruby
n = 500
m = 500

CHARS = "a".split('')

File.open('rotor_input7', 'w') do |f|
  f.puts "#{n} #{m}"
  
  n.times do
    m.times do
      f.write CHARS.sample
    end
    f.puts
  end
end
