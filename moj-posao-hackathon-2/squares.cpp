#include <cstdio>
#include <cstring>
#include <ctype.h>

int main(void) {
  int world[10];
  int len = 0;
  int min_amount = 32768;

  while(scanf("%d", &world[len++]) != EOF) {}
  --len;

  for(int i = 0; i < 6; i++) {
    int min = 5;
    for(int j = 0; j < 5; j++) {
      if (world[i + j] < min) min = world[i + j];
    }
    int amount = 0;
    for(int j = 0; j < 5; j++) {
      amount += world[i + j] - min;
    }
    if (amount < min_amount) min_amount = amount;
  }
  printf("%d\n", min_amount);
  return 0;
}
