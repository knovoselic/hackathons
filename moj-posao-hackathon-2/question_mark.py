import fileinput
debug = False
# debug = True

input = fileinput.input()
n, k = map(int, input.readline().strip().split(" "))
temp = input.readline().strip()
number = list(temp.replace('?', '0'))
# number = temp.replace('?', '0')

if debug: print(temp)
if debug: print(number)
indexes = [i for i, char in enumerate(temp) if char == '?']
qm = len(indexes)
if debug: print(indexes)
values = [0] * qm
values[0] = -1
max_sum = qm * 9
solution = 0

if debug: print(values)
if debug: print(qm)
if temp[0] == '?':
    if qm == 1:
        values[qm - 1] = 0
    else:
        values[qm - 1] = 1;
        number[0] = '1'

if debug: print(values)

while sum(values) < max_sum:
    updated_indexes = [0]
    values[0] += 1
    for i in range(qm):
        if values[i] > 9:
            values[i] -= 10;
            values[i + 1] += 1
            updated_indexes.append(i + 1)
    if debug: print(updated_indexes)
    for i in updated_indexes:
        # index = indexes[qm - i -1]
        # number = number[:index] + chr(values[i] + 48) + number[index + 1:]
        number[indexes[qm - i - 1]] = chr(values[i] + 48)
    if debug: print(''.join(number))
    if int(''.join(number)) % k == 0: solution += 1

print(solution)
