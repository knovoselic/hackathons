#include <cstdio>
#include <algorithm>

int main(void) {
  int a, b, aa, bb;

  scanf("%d %d", &aa, &bb);

  a = std::min(aa, bb);
  b = std::max(aa, bb);
  int diff = b - a;
  if (a % 2 == 0 && b % 2 == 1) diff += 2;
  if (diff % 2 == 0) {
    printf("%d\n", diff / 2);
  } else {
    printf("%d\n", (diff + 1) / 2);
  }

  return 0;
}
