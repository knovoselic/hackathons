var input = readline().trim().split(" ").map(Number);

var a = Math.min(input[0], input[1]);
var b = Math.max(input[0], input[1]);
var diff = b - a;
if (a % 2 == 0 && b % 2 == 1) diff += 2;
if (diff % 2 == 0) {
  print(diff / 2);
} else {
  print((diff + 1) / 2);
}
