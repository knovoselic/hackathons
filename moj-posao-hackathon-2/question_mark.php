<?php

function has_more_values() {
  global $values;
  global $qm;
  global $n;

    for($i = 0; $i < $qm; $i++) {
        if ($values[$i] != 9) return true;
    }
    return false;
}

function next_value() {
  global $values;
  global $qm;
  global $n;

    ++$values[0];
    for($i = 0; $i < $qm; $i++) {
        if ($values[$i] > 9) {
            $values[$i] -= 10;
            ++$values[$i + 1];
        }
    }
}

function create_new_number() {
  global $values;
  global $qm;
  global $n;
  global $number;
  global $indexes;

    for($i = 0; $i < $qm; $i++) {
        $number[$indexes[$qm - $i - 1]] = $values[$i];
    }
}

function number_divisible_by_k() {
  global $values;
  global $qm;
  global $n;
  global $k;
  global $number;
  global $indexes;

    $i = 0;
    $temp = $number[$i];

    while($i < $n - 1) {
        $temp = ($temp % $k) * 10 + $number[++$i];
    }

    $temp %= $k;

    return $temp == 0;
}

fscanf(STDIN, "%d %d", $n, $k);
fscanf(STDIN, "%s", $temp);

$number = array();
$qm = 0;
$indexes = array();
$j = 0;

for($i = 0; $i < $n; ++$i) {
  if ($temp[$i] == '?') {
    ++$qm;
    $indexes[$j++] = $i;
    $number[$i] = -1;
  } else {
    $number[$i] = $temp[$i] - '0';
  }
}

$values[0] = -1;
$solution = 0;

if ($number[0] == -1) {
  $values[$qm-1] = $qm == 1 ? 0 : 1;
}

while(has_more_values()) {
    next_value();
    create_new_number();
    if (number_divisible_by_k()) ++$solution;
}

fprintf(STDOUT, "%d\n", $solution);
?>
