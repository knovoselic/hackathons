import java.io.*;
import java.util.*;

public class Main {
  private static final int NUMBER_OF_LETTERS = 26;

  public static void main(String[] args) {
    Scanner in = new Scanner(System.in);
    while(in.hasNextLine())
    {
      String line = in.nextLine();
      System.out.println(line.replaceAll("not.*?bad", "good"));
    }
  }
}
