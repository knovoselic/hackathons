import java.io.*;
import java.util.*;

public class Main {
  public static int prime_factors(int n) {
    char count = 0;

    while(n % 2 == 0) {
      ++count;
      n /= 2;
    }

    for(int i = 3; i <= Math.sqrt(n); i += 2) {
      while (n%i == 0) {
        ++count;
        n /= i;
      }
    }

    if (n > 2) ++count;

    return count;
  }

  public static void main(String[] args) {
    Scanner in = new Scanner(System.in);
    int n, count = 0;
    int number;

    n = in.nextInt();

    for(int i = 0; i < n; i++) {
      number = in.nextInt();
      count += prime_factors(number);
    }

    if (count % 2 == 0) {
      System.out.println("Slavko");
    } else {
      System.out.println("Mirko");
    }
    System.out.println(count);
  }
}
