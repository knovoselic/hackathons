#include <cstdio>
#include <cstring>
#include <ctype.h>

int main(void) {
  char input[10240];
  int i;

  fgets(input, 10240, stdin);

  for(i = 0; i < strlen(input); i++) {
    if(isalpha(input[i])) {
      printf("-");
    } else if (isdigit(input[i])) {
      printf("*");
    } else {
      printf("?");
    }
  }
  printf("\n");

  return 0;
}
