<?php

$world = array_map('intval', explode(" ", trim(fgets(STDIN, 10240))));
$min_amount = 32768;

for($i = 0; $i < 6; $i++) {
  $min = 5;
  for($j = 0; $j < 5; $j++) {
    if ($world[$i + $j] < $min) $min = $world[$i + $j];
  }
  $amount = 0;
  for($j = 0; $j < 5; $j++) {
    $amount += $world[$i + $j] - $min;
  }
  if ($amount < $min_amount) $min_amount = $amount;
}
fprintf(STDOUT, "%d\n", $min_amount);
?>
