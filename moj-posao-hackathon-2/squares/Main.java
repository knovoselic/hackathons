import java.io.*;
import java.util.*;

public class Main {

  public static void main(String[] args) {
    Scanner in = new Scanner(System.in);
    ArrayList<Integer> world = new ArrayList<Integer>();
    int min_amount = 32768;

    while(in.hasNextInt())
    {
      world.add(in.nextInt());
    }

    for(int i = 0; i < 6; i++) {
      int min = 5;
      for(int j = 0; j < 5; j++) {
        if (world.get(i + j) < min) min = world.get(i + j);
      }
      int amount = 0;
      for(int j = 0; j < 5; j++) {
        amount += world.get(i + j) - min;
      }
      if (amount < min_amount) min_amount = amount;
    }

    System.out.println(min_amount);
  }
}
