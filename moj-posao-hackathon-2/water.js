var n, x, i;
var solution = 0;

[n, x] = readline().trim().split(" ").map(Number);

var bottles = readline().trim().split(" ").map(Number);

bottles.sort(function(a, b) { return a - b; });

i = 0;
while (x > 0 && i < n) {
    x -= bottles[i];
    if (x < 0 ) break;
    ++solution;
    ++i;
}
print(solution);
