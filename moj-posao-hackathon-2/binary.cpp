#include <cstdlib>
#include <cstdio>
#include <iterator>
#include <vector>

#define MAX 10000
#define MOD 1000000007
#define DEBUG(x, ...)
// #define DEBUG(x, ...) printf(x, ##__VA_ARGS__)

typedef std::vector<unsigned long long int> vint_t;
unsigned long long int solution = 0;

void calculate(const vint_t &va) {
  unsigned long long int sum = 0;
  unsigned long long int squares = 0;
  unsigned long long int temp;
  unsigned long long int count = va.size();
  int size = va.size();

  for(int i = 0; i < size; ++i) {
    temp = va[i];
    sum = (sum + temp);
    squares = (squares + temp*temp);
  }

  for(int i = 0; i < size; ++i) {
    temp = va[i];
    --count;
    sum -= temp;
    squares -= temp*temp;
    solution = (solution + count * temp * temp - (temp<<1)*sum + squares);
  }
}

void calculate(const vint_t &va, const vint_t &vb) {
  const vint_t *bigger, *smaller;
  if (va.size() > vb.size()) {
    bigger = &va;
    smaller = &vb;
  } else {
    bigger = &vb;
    smaller = &va;
  }
  unsigned long long int sum = 0;
  unsigned long long int squares = 0;
  unsigned long long int temp;
  unsigned long long int count = bigger->size();

  int current_size = bigger->size();
  for(int i = 0; i < current_size; ++i) {
    temp = (*bigger)[i];
    sum = sum + temp;
    squares = squares + temp*temp;
  }

  current_size = smaller->size();
  for(int i = 0; i < current_size; ++i) {
    temp = (*smaller)[i];
    solution = (solution + count * temp * temp - (temp<<1)*sum + squares);
  }
}

int main(void) {
  int n;
  scanf("%d", &n);
  int temp;
  vint_t *positions[10001] = {NULL};

  for(int i = 0; i < n; i++) {
    scanf("%d", &temp);
    if (positions[temp] == NULL) positions[temp] = new vint_t();
    positions[temp]->push_back(i);
  }

  DEBUG("=============\n");

  for(int i = 1; i <= MAX; i++) {
    for(int j = 1; j < i; j++) {
      if ((i & j) != j) continue;
      // DEBUG("Checking %d, %d\n", i, j);
      vint_t *vi = positions[i];
      if (!vi) continue;
      vint_t *vj = positions[j];
      if (!vj) continue;
      calculate(*vi, *vj);
    }
  }

  for(int i = 1; i <= MAX; ++i) {
    if (!positions[i] || positions[i]->size() == 1) continue;
    // ovo traje jako kratko, naravno da se optimizirat kak treba ako bude potrebno
    calculate(*positions[i]);
    calculate(*positions[i]);
  }

  // solution = 18446744073702214796u;
  printf("%lld\n", solution % MOD);

  return 0;
}
