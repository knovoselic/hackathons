#include <iostream>
#include <map>
#include <cmath>

std::map<int, int> primes;

void prime_factors(unsigned long long n) {
  while(n % 2 == 0) {
    primes[2]++;
    n /= 2;
  }

  for(int i = 3; i <= sqrt(n); i += 2) {
    while (n%i == 0) {
      primes[i]++;
      n /= i;
    }
  }

  if (n > 1) primes[n]++;
}

int main(void) {
  unsigned long long n;
  unsigned long long count = 1;

  std::cin >> n;

  prime_factors(n);

  for(std::map<int,int>::iterator it = primes.begin(); it != primes.end(); ++it) {
    count *= (it->second + 1);
  }

  std::cout << count << std::endl;

  return 0;
}
