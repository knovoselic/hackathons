<?php

$NUMBER_OF_LETTERS = 26;
$counts = array();
$input = strtolower(fgets(STDIN, 10240));

for($i = 0; $i < strlen($input); $i++) {
  if (!ctype_alpha($input[$i])) continue;
  $counts[ord($input[$i]) - ord('a')]++;
}

$first_print = true;
for($i = 0; $i < $NUMBER_OF_LETTERS; $i++) {
  if ($counts[$i] == 0) continue;
  if (!$first_print) printf("\n");
  fprintf(STDOUT, "%s:%d", chr($i + ord('a')), $counts[$i]);
  $first_print = false;
}
?>
