/*
S 2
M 9
S 426
S 380
M 393
M 409
S 384
*/
#include <cstdio>
#include <cstring>
#include <cmath>
#include <algorithm>

char prime_factors(unsigned int n) {
  char count = 0;

  while(n % 2 == 0) {
    ++count;
    n /= 2;
  }

  for(int i = 3; i <= sqrt(n); i += 2) {
    while (n%i == 0) {
      ++count;
      n /= i;
    }
  }

  if (n > 2) ++count;

  return count;
}

int main(void) {
  int n;
  int count = 0;
  unsigned int number;

  scanf("%d", &n);

  for(int i = 0; i < n; i++) {
    scanf("%u", &number);
    count += prime_factors(number);
  }

  if (count % 2 == 0) {
    printf("Slavko\n");
  } else {
    printf("Mirko\n");
  }
  printf("%d\n", count);
  return 0;
}
