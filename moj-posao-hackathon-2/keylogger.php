<?php

$num = intval(fgets(STDIN, 10));
$caps_lock = false;
$buffer = array();

for($i = 0; $i < $num; $i++) {
  $line = trim(fgets(STDIN, 10240));
  if ($line == 'backspace') {
    if (count($buffer) > 0) array_pop($buffer);
  } else if ($line == 'caps') {
    $caps_lock = !$caps_lock;
  } else {
    $char = $caps_lock ? strtoupper($line[0]) : strtolower($line[0]);
    array_push($buffer, $char);
  }
}

fprintf(STDOUT, "%s\n", implode('', $buffer));
?>
