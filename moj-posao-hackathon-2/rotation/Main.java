import java.io.*;
import java.util.*;

public class Main {
  public static int[][] mat = new int[51][51];
  public static int n;

  public static void rotate(int N, int x, int y) {
    for (int i = 0; i < N / 2; ++i) {
      for (int j = i; j < N - i - 1; ++j) {
          int temp = mat[i + x][j + y];

          mat[i + x][j + y] = mat[j + x][N - 1 - i + y];

          mat[j + x][N - 1 - i + y] = mat[N - 1 - i + x][N - 1 - j + y];

          mat[N - 1 - i + x][N - 1 - j + y] = mat[N - 1 - j + x][i + y];

          mat[N - 1 - j + x][i + y] = temp;
      }
    }
  }

  public static void main(String[] args) {
    Scanner in = new Scanner(System.in);
    int d[][] = new int[50][4];
    int q;

    n = in.nextInt();

    for(int i = 0; i < n; i++) {
      for (int j = 0; j < n; j++) {
        mat[i][j] = in.nextInt();
      }
    }

    q = in.nextInt();

    for(int i = 0; i < q; i++) {
      for (int j = 0; j < 4; j++) {
        d[i][j] = in.nextInt();
      }
    }

    for(int i = q - 1; i >= 0; --i) {
      int x = d[i][0] - 1;
      int y = d[i][1] - 1;
      int a = d[i][2];
      int k = d[i][3] % 4;

      while(k-- > 0) rotate(a, x, y);
    }

    for(int i = 0; i < n; i++) {
      for (int j = 0; j < n; j++) {
        System.out.print(mat[i][j]);
        if (j < n - 1) System.out.print(' ');
      }
      System.out.println();
    }
    System.out.println();
  }
}
