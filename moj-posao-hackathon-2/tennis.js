var n = Number(readline().trim())
var x_wins = 0;
var y_wins = 0;

for(var i = 0; i < n; i++) {
  var results = readline().trim().split(":").map(Number);
  if (results[0] > results[1]) {
    x_wins = x_wins + 1;
  } else {
    y_wins = y_wins + 1;
  }
}

print(x_wins + ":" + y_wins);
