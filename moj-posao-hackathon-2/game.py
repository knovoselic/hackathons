import fileinput
import math

file = iter(fileinput.input())

def prime_factors(n):
    count = 0;
    while(n % 2 == 0):
        count += 1
        n = n / 2

    i = 3
    while(i <= math.sqrt(n)):
        while (n%i == 0):
            count += 1
            n = n / i
        i += 2

    if (n > 2): count += 1

    return count

count = 0

n = int(next(file))
numbers = [int(x) for x in next(file).strip().split(" ")]

for i in range(n):
    count = count + prime_factors(numbers[i])

if (count % 2 == 0):
    print("Slavko")
else:
    print("Mirko")

print(count)
