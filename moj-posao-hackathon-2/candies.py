import math

primes = {}

def prime_factors(n):
    global primes

    while(n % 2 == 0):
        if not 2 in primes: primes[2] = 0
        primes[2] += 1
        n = n / 2

    i = 3
    while(i <= math.sqrt(n)):
        while (n%i == 0):
            if not i in primes: primes[i] = 0
            primes[i] += 1
            n = n / i
        i += 2

    if (n > 2):
        if not n in primes: primes[n] = 1

count = 1

n = int(input())
prime_factors(n)

for v in primes.values():
    count *= (v + 1)

print(count)
