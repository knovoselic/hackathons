var n, k;
var indexes = [-1, -1, -1, -1, -1];
var values = [0, 0, 0, 0, 0];
var qm = 0;
var number = [];

function number_divisible_by_k() {
    var i = 0;
    var temp = number[i];

    while(i < n - 1) {
        temp = (temp % k) * 10 + number[++i];
    }

    temp %= k;

    return temp == 0;
}

function has_more_values() {
    for(var i = 0; i < qm; i++) {
        if (values[i] != 9) return true;
    }
    return false;
}

function next_value() {
    ++values[0];
    for(var i = 0; i < qm; i++) {
        if (values[i] > 9) {
            values[i] -= 10;
            ++values[i + 1];
        }
    }
}

function create_new_number() {
    for(var i = 0; i < qm; i++) {
        number[indexes[qm - i - 1]] = values[i];
    }
}

var temp;

n = readline().trim().split(" ").map(Number);
k = n[1];
n = n[0];

temp = readline().trim();

var j = 0;
for(var i = 0; i < n; i++) {
    if (temp[i] == '?') {
        ++qm;
        indexes[j++] = i;
        number[i] = -1;
    } else {
        number[i] = temp.charCodeAt(i) - 48;
    }
}


// print_number();

values[0] = -1;
var solution = 0;

if (number[0] == -1) {
  values[qm-1] = qm == 1 ? 0 : 1;
}

while(has_more_values()) {
    next_value();
    create_new_number();
    // print_number();
    if (number_divisible_by_k()) ++solution;
}

print(solution);
