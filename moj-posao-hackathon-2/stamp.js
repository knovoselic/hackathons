var n = 0;
var area = 0;
var rects = [];

n = Number(readline().trim());

for(var i = 0; i < n; i++) {
  var rect = readline().trim().split(" ").map(Number);
  rects[i] = rect;
  area += (rect[2] - rect[0]) * (rect[3] - rect[1]);
}

m = Number(readline().trim());
for(i = 0; i < m; i++) {
  var cr = readline().trim().split(" ").map(Number);
  area += (cr[2] - cr[0]) * (cr[3] - cr[1]);
  for(j = 0; j < n; j++) {
    rect = rects[j];
    x_overlap = Math.max(0, Math.min(rect[2], cr[2]) - Math.max(rect[0], cr[0]));
    y_overlap = Math.max(0, Math.min(rect[3], cr[3]) - Math.max(rect[1], cr[1]));
    area -= x_overlap * y_overlap;
  }
}

print(area);
