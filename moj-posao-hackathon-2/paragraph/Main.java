import java.io.*;
import java.util.*;

public class Main {
  public static void print(String str, int n) {
    while(n-- > 0) System.out.print(str);
  }
  public static void print_padded(String line, String alignment, int w) {
    if (alignment.equals("right")) {
      print(".", w - line.length());
      System.out.println(line);
    } else if (alignment.equals("left")) {
      System.out.print(line);
      print(".", w - line.length());
      System.out.println();
    } else {
      int diff = w - line.length();
      int spaces = diff / 2;
      print(".", spaces);
      System.out.print(line);
      print(".", spaces);
      if (diff % 2 == 1) System.out.print(".");
      System.out.println();
    }
  }

  public static void main(String[] args) {
    Scanner in = new Scanner(System.in);
    int w = in.nextInt();
    String alignment = in.next().trim();
    ArrayList<String> words = new ArrayList<String>();

    while(in.hasNext()) {
      words.add(in.next().trim());
    }

    int current_width = 0;
    String line = "";

    while(words.size() > 0) {
      if (line.equals("")) {
        line = words.remove(0);
      } else if (line.length() + words.get(0).length() + 1 <= w) {
        line = line + "." + words.remove(0);
      } else {
        print_padded(line, alignment, w);
        line = "";
      }
    }

    if (!line.equals("")) print_padded(line, alignment, w);
  }
}
