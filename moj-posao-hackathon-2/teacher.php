<?php

fscanf(STDIN, "%d %d", $n, $x);
$heights = array_map('intval', explode(" ", trim(fgets(STDIN, 102400))));

while(--$n) {
  $x -= $heights[$n - 1];
}

printf("%d\n", $x);

?>
