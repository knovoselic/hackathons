#include <iostream>
#include <map>
#include <vector>

#define MAX 10001
#define MOD 1000000007
#define DEBUG(x, ...)
// #define DEBUG(x, ...) printf(x, ##__VA_ARGS__)

typedef std::vector<long long int> vint_t;
typedef std::map<int, vint_t> pos_t;

bool mem[MAX][MAX] = {{0}};
pos_t positions;
long long int solution = 0;

void pretty_print_mem() {
  if (MAX > 100) {
    DEBUG("MAX = %d is too high, will not print.\n", MAX);
    return;
  }
  for(int i = 0; i < MAX; i++) {
    for(int j = 0; j < MAX; j++) {
      DEBUG("%d ", mem[i][j]);
      if (j % 4 == 3) DEBUG("| ");
    }
    DEBUG("\n");
    if (i % 4 == 3) {
      for(int j = 0; j < MAX*1.23; j++ ) DEBUG("--");
      DEBUG("\n");
    }
  }
}

void pretty_print_positions() {
  for(pos_t::iterator it = positions.begin(); it != positions.end(); ++it) {
    DEBUG("%d: ", it->first);
    for(vint_t::iterator vit = it->second.begin(); vit != it->second.end(); ++vit) {
      DEBUG("%lld ", *vit);
    }
    DEBUG("\n");
  }
}

// void calculate(const vint_t &va) {
//   for(vint_t::const_iterator a = va.begin(); a != va.end(); ++a) {
//     for(vint_t::const_iterator b = std::next(a); b != va.end(); ++b) {
//       solution = (solution + (((*a - *b) * (*a - *b)) % MOD)) % MOD;
//       DEBUG("-- %d %d %d\n", *a, *b, result);
//     }
//   }
// }

// void calculate(const vint_t &va, const vint_t &vb) {
//   for(vint_t::const_iterator a = va.begin(); a != va.end(); ++a) {
//     for(vint_t::const_iterator b = vb.begin(); b != vb.end(); ++b) {
//       solution = (solution + (((*a - *b) * (*a - *b)) % MOD)) % MOD;
//       DEBUG("-- %d %d %d\n", *a, *b, result);
//     }
//   }
// }

void calculate(const vint_t &va) {
  for(vint_t::const_iterator a = va.begin(); a != va.end(); ++a) {
    for(vint_t::const_iterator b = std::next(a); b != va.end(); ++b) {
      solution = (solution + (((*a - *b) * (*a - *b)) % MOD)) % MOD;
      DEBUG("-- %lld %lld %lld\n", *a, *b, solution);
    }
  }
}

void calculate(const vint_t &va, const vint_t &vb) {
  const vint_t *bigger, *smaller;
  if (va.size() > vb.size()) {
    bigger = &va;
    smaller = &vb;
  } else {
    bigger = &vb;
    smaller = &va;
  }
  long long int sum = 0;
  long long int squares = 0;
  long long int temp;
  long long int count = bigger->size();

  for(vint_t::const_iterator a = bigger->begin(); a != bigger->end(); ++a) {
    temp = *a;
    sum = (sum + temp) % MOD;
    squares = (squares + (temp*temp % MOD)) % MOD;
  }

  for(vint_t::const_iterator a = smaller->begin(); a != smaller->end(); ++a) {
    temp = *a;
    solution = (solution + count * temp * temp - (temp<<1)*sum + squares) % MOD;
  }
}

int main(void) {
  int n;
  std::cin >> n;
  int temp;

  for(int i = 0; i < MAX; i++) {
    for(int j = 0; j <= i; j++) {
      mem[i][j] = (i & j) == j;
    }
  }

  for(int i = 0; i < n; i++) {
    std::cin >> temp;
    positions[temp].push_back(i);
  }

  // pretty_print_mem();
  // pretty_print_positions();
  DEBUG("=============\n");

  int a, b;
  for(pos_t::iterator i = positions.begin(); i != positions.end(); ++i) {
    a = i->first;

    for(pos_t::iterator j = i; j != positions.end(); ++j) {
      if (i == j && j->second.size() == 1) continue;
      b = j->first;

      // printf("Checking %d, %d\n", a, b);
      if (mem[a][b]) {
        if (i == j) {
          calculate(i->second);
        } else {
          calculate(i->second, j->second);
        }
        DEBUG("%d, %d; %lld\n", a, b, solution);
      }
      if (mem[b][a]) {
        if (i == j) {
          calculate(i->second);
        } else {
          calculate(i->second, j->second);
        }
        DEBUG("%d, %d; %lld\n", b, a, solution);
      }
    }
  }


  printf("%lld\n", solution);

  return 0;
}
