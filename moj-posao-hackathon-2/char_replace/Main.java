import java.io.*;
import java.util.*;

public class Main {
  private static final int NUMBER_OF_LETTERS = 26;

  public static void main(String[] args) {
    Scanner in = new Scanner(System.in);
    while(in.hasNextLine())
    {
      String input = in.nextLine();
      int i;

      for(i = 0; i < input.length(); i++) {
        if(Character.isLetter(input.charAt(i))) {
          System.out.print("-");
        } else if (Character.isDigit(input.charAt(i))) {
          System.out.print("*");
        } else {
          System.out.print("?");
        }
      }
      System.out.println();
    }
  }
}
