import fileinput

for line in fileinput.input():
    input = [int(x) for x in line.strip().split(" ")]
    a = min(input)
    b = max(input)
    diff = b - a;
    if (a % 2 == 0 and b % 2 == 1): diff = diff + 2
    if (diff % 2 == 0):
        print(int(diff / 2));
    else:
        print(int((diff + 1) / 2));
