#include <cstdio>
#include <cstring>
#include <ctype.h>

#define NUMBER_OF_LETTERS 26
int main(void) {
  int input[10240];
  int len = 0;

  while(scanf("%d", &input[len++]) != EOF) {}
  --len;
  for(int i = 0; i < len; ) {
    int current = input[i];
    printf("%d ", input[i]);
    i++;
    for(; i < len && input[i] == current; i++) {}
  }
  printf("\n");
  return 0;
}
