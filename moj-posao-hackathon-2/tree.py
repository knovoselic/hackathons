MAX = 1000

def branch(x, y, dx, dy, level):
    for i in range(a[level]):
        x += dx;
        y += dy;
        lines[x][y] = '*';

    level += 1
    if level >= n: return

    if dx != 0 and dy != 0:
        branch(x, y, dx, 0, level);
        branch(x, y, 0, dy, level);
    elif dx == 0:
        branch(x, y, -1, dy, level);
        branch(x, y, 1, dy, level);
    else:
        branch(x, y, dx, -1, level);
        branch(x, y, dx, 1, level);

n = int(input().strip())
a = [int(x) for x in input().strip().split(" ")]

lines = [['.' for x in range(MAX)] for y in range(MAX)]

x = MAX
y = MAX // 2

for i in range(a[0]):
    x -= 1
    lines[x][y] = '*'

branch(x, y, -1, -1, 1);
branch(x, y, -1, 1, 1);

minx = MAX
miny = MAX
maxx = 0
maxy = 0

for i in range(MAX):
    for j in range(MAX):
        if lines[i][j] == '.': continue
        minx = min(minx, i)
        miny = min(miny, j)
        maxx = max(maxx, i)
        maxy = max(maxy, j)

for i in range(minx, maxx + 1):
    for j in range(miny, maxy + 1):
        print(lines[i][j], end='')
    print()
