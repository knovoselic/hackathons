# frozen_string_literal: true

require 'guard/compat/plugin'
require 'benchmark'
require 'pry'

module ::Guard
  class Base < Plugin
    def run_on_modifications(paths)
      paths.each do |path|
        @compiled = false
        max_duration = 0
        clean_output_file
        filtered_input_files(path).each do |input|
          compile_command, run = generate_command(path, input)
          run += " | tee -a #{output_file}"
          puts compile_command if compile_command
          puts run
          puts '=' * 80
          break unless compile_if_needed compile_command
          duration = Benchmark.realtime { system run }
          max_duration = [max_duration, duration].max
          puts '+' * 80
          puts "Total duration: #{duration}"
          puts '=' * 80
        end
        puts "Max duration: #{max_duration}"
      end
    end

    def input_files(path)
      file_name = File.basename(path, File.extname(path))
      Dir["#{file_name}_input*"].sort
    end

    def generate_command(_path, _input)
      raise 'Please implement this'
    end

    private

    def output_file
      'current_output'
    end

    def clean_output_file
      File.delete output_file if File.exist? output_file
    end

    def filtered_input_files(path)
      files = input_files(path)
      return files unless options[:input_filters]
      files.select do |input|
        options[:input_filters].any? { |filter| input =~ Regexp.new(filter) }
      end
    end

    def compile_if_needed(compile)
      return true unless compile && !@compiled
      system(compile).tap do |successful|
        @compiled = true if successful
      end
    end
  end
  class Cpp < Base
    def generate_command(path, input)
      ["g++ -Wall -Wextra -Warray-bounds #{path}", "./a.out < #{input}"]
    end
  end
  class Java < Base
    def generate_command(path, input)
      directory = File.dirname path
      ["javac #{path}", "java -cp #{directory} Main < #{input}"]
    end

    def input_files(path)
      file_name = File.dirname path
      Dir["#{file_name}_input*"].sort
    end
  end
  class Php < Base
    def generate_command(path, input)
      [nil, "php -d error_reporting=22527 #{path} < #{input}"]
    end
  end
  class Js < Base
    def generate_command(path, input)
      [nil, "js #{path} < #{input}"]
    end
  end
  class Python < Base
    def generate_command(path, input)
      [nil, "python3 #{path} < #{input}"]
    end
  end
end
guard :cpp do
  watch(/^.*\.cpp$/)
end
guard :java do
  watch(/^.*\.java$/)
end
guard :php do
  watch(/^.*\.php$/)
end
guard :js do
  watch(/^.*\.js$/)
end
guard :python do
  watch(/^.*\.py$/)
end

Pry::Commands.block_command 'only', 'Run only specific test cases' do |*numbers|
  if numbers.empty?
    print 'Input filter: '
    puts Guard.state.session.plugins.all.first.options[:input_filters]
  else
    Guard.state.session.plugins.all.each do |plugin|
      plugin.options[:input_filters] = numbers
    end
  end
end

Pry::Commands.block_command 'tc_all', 'Run all test cases' do
  Guard.state.session.plugins.all.each do |plugin|
    plugin.options[:input_filters] = false
  end
end
