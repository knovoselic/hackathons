FROM i386/ubuntu:latest

ENV DEBIAN_FRONTEND=noninteractive

RUN apt-get update
RUN apt-get upgrade -y
RUN apt-get install -y build-essential
RUN apt-get install -y ruby ruby-dev
RUN apt-get install -y python3
RUN apt-get install -y php php-bcmath
RUN apt-get install -y openjdk-8-jdk
RUN apt-get install -y libmozjs-52-0 libmozjs-52-dev

RUN gem install guard

WORKDIR /app
