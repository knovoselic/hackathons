# FLOOD_FILL_MATRIX = [[1, 0], [0, 1]]
FLOOD_FILL_MATRIX = [[1, 0], [0, 1], [-1, 0], [0, -1]]
SPACES_TO_LETTER = {
    0 => 'F',
    1 => 'O',
    2 => '8'
}

$n, $m = gets.chomp.split(' ').map(&:to_i)
$field = Array.new($n) { gets.chomp }

def print_field
    $n.times do |y|
        $m.times do |x|
            print $field[y][x]
        end
        puts
    end
end

def flood_fill(x, y, from, to)
    return if x >= $m || y >= $n
    return if x < 0 || y < 0
    return unless $field[y][x] == from
    $field[y][x] = to
    FLOOD_FILL_MATRIX.each do |dx, dy|
        flood_fill x + dx, y + dy, from, to
    end
end

def flood_fill_boundaries(originx, originy, dx = 0, dy = 0)
    # puts "#{x}, #{y}, #{current_width}, #{current_height}"
    x = originx + dx
    y = originy + dy
    return if x >= $m || y >= $n
    return if x < 0 || y < 0
    return unless $field[y][x] == '1'
    $field[y][x] = '$'
    max_x = dx + 1
    max_y = dy + 1
    FLOOD_FILL_MATRIX.each do |stepx, stepy|
        sx, sy = flood_fill_boundaries(originx, originy, dx + stepx, dy + stepy)
        next unless sx || sy
        max_x = sx if sx > max_x
        max_y = sy if sy > max_y
    end
    return max_x, max_y
end
letters = []
bg_removed = false
$n.times do |y|
    $m.times do |x|
        if $field[y][x] == '.'
            # puts 'Removing background ', x, y
            bg_removed = true
            flood_fill x, y, '.', ' '
        end
        break if bg_removed
    end
    break if bg_removed
end
# print_field

$n.times do |y|
    $m.times do |x|
        if $field[y][x] == '1'
            width, height = flood_fill_boundaries x, y
            empty_spaces = 0
            height.times do |yy|
                width.times do |xx|
                    if $field[y + yy][x + xx] == '.'
                        empty_spaces += 1
                        flood_fill x + xx, y + yy, '.', '+'
                    end
                end
            end
            letters << SPACES_TO_LETTER[empty_spaces]
        end
    end
end

# print_field
if letters.empty?
    puts 'N'
else
  puts letters.sort.join
end