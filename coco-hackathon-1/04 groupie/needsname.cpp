#include<stdio.h>
int main()
{
	long long int t,n,a[20],i,j,product,k,negproduct,max,min;
	scanf("%lld",&t);
	while(t--)
	{
		scanf("%lld",&n);
		for(i=0;i<n;i++)
			scanf("%lld",&a[i]);
		for(i=0;i<n;i++)
			for(j=i;j<n;j++)
				if(a[i]>a[j])
				{
					k=a[i];
					a[i]=a[j];
					a[j]=k;
				}
		i=0;
		while((a[i]<0)&&(i<n))
			i++;
		j=i;
		while((a[j]==0)&&(j<n))
			j++;
		product=1;
		for(k=j;k<n;k++)
			product=product*a[k];
		if(n==1)
			printf("%lld %lld\n",a[0],a[0]);
		else if((j-i)==n)
			printf("0 0\n");
		else if((i==0)&&(j!=n))
			printf("%lld %lld\n",product,a[0]);
		else if((j==n)&&(i==1))
			printf("0 %lld\n",a[0]);
		else
		{
			negproduct=1;
			for(k=0;k<i;k++)
				negproduct=negproduct*a[k];
			if(negproduct>0)
			{
				max=negproduct*product;
				min=negproduct*product;
				min=min/a[i-1];
				printf("%lld %lld\n",max,min);
			}
			else if(negproduct<0)
			{
				min=negproduct*product;
				max=negproduct*product;
				max=max/a[i-1];
				printf("%lld %lld\n",max,min);
			}
		}
	}
	return 0;
}
