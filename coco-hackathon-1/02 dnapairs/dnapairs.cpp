#include <stdio.h>
#define max 1000000

int main() {
  char a[max];
  long t, n, i;
  char error;
  scanf("%ld", &t);
  while (t--) {
    error = 0;
    scanf("%ld", &n);
    scanf("%s", a);
    for (i = 0; i < n; i++) {
      if (a[i] == 'A')
        a[i] = 'T';
      else if (a[i] == 'T')
        a[i] = 'A';
      else if (a[i] == 'C')
        a[i] = 'G';
      else if (a[i] == 'G')
        a[i] = 'C';
      else {
        printf("Bravo, Braco! Nasao si ne DNA par!\n");
        error = 1;
        break;
      }
    }
    if (!error)
      printf("%s\n", a);
  }
  return 0;
}
