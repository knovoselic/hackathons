// C++
#include <algorithm>
#include <bitset>
#include <complex>
#include <deque>
#include <exception>
#include <fstream>
#include <functional>
#include <iomanip>
#include <ios>
#include <iosfwd>
#include <iostream>
#include <istream>
#include <iterator>
#include <limits>
#include <list>
#include <locale>
#include <map>
#include <memory>
#include <new>
#include <numeric>
#include <ostream>
#include <queue>
#include <set>
#include <sstream>
#include <stack>
#include <stdexcept>
#include <streambuf>
#include <string>
#include <typeinfo>
#include <utility>
#include <valarray>
#include <vector>

#if __cplusplus >= 201103L
#include <array>
#include <atomic>
#include <chrono>
#include <condition_variable>
#include <forward_list>
#include <future>
#include <initializer_list>
#include <mutex>
#include <random>
#include <ratio>
#include <regex>
#include <scoped_allocator>
#include <system_error>
#include <thread>
#include <tuple>
#include <typeindex>
#include <type_traits>
#include <unordered_map>
#include <unordered_set>
#endif
using namespace std;

typedef long long LL;

const int N = 64;
const char Icons[] = "FOP";
const int dr[] = {-1, 0, 1, 0};
const int dc[] = {0, 1, 0, -1};

int n, m;
char g[N][N];

void flood(int i, int j, vector<int>& vx, vector<int>& vy) {
    if (i < 0 || i >= n || j < 0 || j >= m || '1' != g[i][j]) return;
    vx.push_back(i);
    vy.push_back(j);
    g[i][j] = '*';
    for (int d = 0; d < 4; ++d) flood(i + dr[d], j + dc[d], vx, vy);
}

char ocr(int i, int j) {
    vector<int> vx, vy;
    flood(i, j, vx, vy);
    // for (int i = 0; i < n; ++i) cout << g[i] << endl;
    int min_x = vx[0], min_y = vy[0];
    for (int i = 0; i < vx.size(); ++i) {
        min_x = min(min_x, vx[i]);
        min_y = min(min_y, vy[i]);
    }
    for (int i = 0; i < vx.size(); ++i) vx[i] -= min_x, vy[i] -= min_y;
    LL by_row[N] = {0};
    for (int i = 0; i < vx.size(); ++i) by_row[vx[i]] |= 1LL << vy[i];
    int h = unique(by_row, by_row + N) - by_row;
    if (h > 0 && 0 == by_row[h - 1]) --h;
    // for (int i = 0; i < h; ++i) cout << by_row[i] << endl;
    LL by_col[N] = {0};
    for (int j = 0; j < N; ++j) for (int i = 0; i < h; ++i) {
        if (by_row[i] & (1LL << j)) by_col[j] |= 1LL << i;
    }
    int w = unique(by_col, by_col + N) - by_col;
    if (w > 0 && 0 == by_col[w - 1]) --w;
    // for (int i = 0; i < w; ++i) cout << by_col[i] << endl;
    if (3 == w && 7 == by_col[0] && 5 == by_col[1] && 7 == by_col[2]) return 'O';
    if (3 == w && 31 == by_col[0] && 21 == by_col[1] && 31 == by_col[2]) return 'P';
    if (w >= 2 && 15 == by_col[0] && 5 == by_col[1]) return 'F';
    return 'X';
}

int main(void) {
    ios_base::sync_with_stdio(false);
    cin.tie(NULL);

    cin >> n >> m;
    for (int i = 0; i < n; ++i) cin >> g[i];
    int occ[128] = {0};
    for (int i = 0; i < n; ++i) for (int j = 0; j < m; ++j) if ('1' == g[i][j]) {
        ++occ[ocr(i, j)];
    }
    bool any = false;
    for (const char* p = Icons; *p; ++p) for (int i = occ[*p]; i-- > 0; ) cout << *p, any = true;
    if (!any) cout << 'N';
    cout << '\n';

    return 0;
}
