#include <cstdio>

int main(void) {
  int n;

  scanf("%d", &n);
  --n;
  for(int i = 0; i <= n; i++) {
    for(int j = 0; j <= n; j++) {
      if (i == 0 || i == n || j == 0 || j == n || i - j == 0 || i + j == n) {
        printf("#");
      } else {
        printf(".");
      }
      if (j < n) printf(".");
    }
    printf("\n");
  }
  return 0;
}
