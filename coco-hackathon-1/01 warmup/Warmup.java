import java.util.Scanner;

public class Warmup {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int n = scanner.nextInt() - 1;
        StringBuilder sb = new StringBuilder(n * n * 2);

        for(int i = 0; i <= n; i++) {
          for(int j = 0; j <= n; j++) {
            if (i == 0 || i == n || j == 0 || j == n || i - j == 0 || i + j == n) {
              sb.append("#");
            } else {
              sb.append(".");
            }
            if (j < n) sb.append(".");
          }
          sb.append("\n");
        }
        System.out.print(sb.toString());
    }
}
