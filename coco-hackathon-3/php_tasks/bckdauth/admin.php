<?php
  $logged_in = '';

  if ($_COOKIE["session_loggedin"] == "true") {
    $logged_in = 'user';
  }
  if ($_COOKIE["session_admin"] == "true") {
    $logged_in = 'admin';
  }

  if ($_POST["username"] == "user" && $_POST["password"] == "1234") {
    setcookie("session_loggedin", "true", time() + (86400 * 30), "/");
    setcookie("session_admin", "false", time() + (86400 * 30), "/");
    $logged_in = 'user';
  } else if ($_POST["username"] == "admin" && $_POST["password"] == "9hjGDRwMK") {
    setcookie("session_loggedin", "true", time() + (86400 * 30), "/");
    setcookie("session_admin", "true", time() + (86400 * 30), "/");
    $logged_in = 'admin';
  }

?>

<!doctype html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="author" content="Kristijan Novoselić">
    <title>CoCo Hackathon 2021</title>
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.1/dist/css/bootstrap.min.css" crossorigin="anonymous">
    <style>
      .bd-placeholder-img {
        font-size: 1.125rem;
        text-anchor: middle;
        -webkit-user-select: none;
        -moz-user-select: none;
        user-select: none;
      }

      @media (min-width: 768px) {
        .bd-placeholder-img-lg {
          font-size: 3.5rem;
        }
      }
    </style>


    <!-- Custom styles for this template -->
    <link href="navbar-top-fixed.css" rel="stylesheet">
  </head>
  <body>

<nav class="navbar navbar-expand-md navbar-dark fixed-top bg-dark">
  <div class="container-fluid">
    <a class="navbar-brand" href="#">CoCo Hackathon 2021</a>
    <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarCollapse" aria-controls="navbarCollapse" aria-expanded="false" aria-label="Toggle navigation">
      <span class="navbar-toggler-icon"></span>
    </button>
    <div class="collapse navbar-collapse" id="navbarCollapse">
      <ul class="navbar-nav me-auto mb-2 mb-md-0">
        <li class="nav-item">
          <a class="nav-link" href="/">Home</a>
        </li>
        <li class="nav-item">
          <a class="nav-link active" href="admin.php">Admin</a>
        </li>
      </ul>
      <form class="d-flex" action="logout.php">
        <button class="btn btn-outline-success" type="submit">Log out</button>
      </form>
    </div>
  </div>
</nav>

<main class="container">
  <div class="bg-light p-5 rounded">
    <?php if ($logged_in == 'user') : ?>
      <h1>Go away, you are not worthy of my flag!</h1>
    <?php elseif($logged_in == 'admin') : ?>
      <h1>Yo admin, here's your flag:</h1>
      <h2>3xTWZ5u4kh4KNewZGXrjHyPbMUEWrZGZ</h2>
    <?php else : ?>
    <h1>You want the flag?</h1>
    <form action="" method="POST">
      <div class="mb-3">
        <label for="username" class="form-label">Username</label>
        <input type="text" class="form-control" name="username" id="username" placeholder="hahaha, you have no idea">
      </div>
      <div class="mb-3">
        <label for="password" class="form-label">Password</label>
        <input type="password" class="form-control" name="password" id="password" placeholder="the password is 128 characters long">
      </div>
      <button type="submit" class="btn btn-primary">Login</button>
    </form>
  <?php endif; ?>
  </div>
</main>

  <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.1/dist/js/bootstrap.bundle.min.js" crossorigin="anonymous"></script>
  </body>
</html>
