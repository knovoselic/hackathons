COCO tim često koristi Google Shits. COCO tim često spamma, trolla i umeće neprimjeren sadržaj u Google Shits.

Menadžment je odlučio tome stati na kraj (točnije Happiness Control Management) i treba pomoć u prepoznavanju zločestog sadržaja.

Prihvatljiv sadržaj je onaj koji:

sadrži najmanje 3 samoglasnika (`aeiou`), poput `aei`, `xazegov` ili `aeiouaeiouaeiou`.

sadrži najmanje jedno slovo koje se pojavljuje dva puta u istome redu, poput `xx`, `abcdde` (`dd`) ili `aabbccdd` (`aa`, `bb`, `cc` ili `dd`).

ne sadrži stringove (znakovne nizove) `ab`, `cd`, `pq`, ili `xy` čak i kada su dio nekog od ostalih pravila.

Na primjer:

`ugknbfddgicrmopn` je prihvatljiv zato što ima najmanje 3 samoglasnika (u...i...o...), dvostruko slovo (...dd...) i nema nedozvoljene substringove (znakovne podnizove).

`aaa` je prihvatljiv zato što ima najmanje 3 samoglasnika i dvostruko slovo, iako se slova spomenuta u oba pravila preklapaju.

`jchzalrnumimnmhp` je zločest zato što nema dvostruko slovo.

`haegwjzuvuyypxyu` je zločest zato što ima substring (znakovni podniz) `xy`.

`dvszwmarrgswjxmb` je zločest zato što ima samo 1 samoglasnik.


Menadžment je uvidio svoje pogreške, i nakon implementacije i deploya na produkcijski environment odlučio razmisliti i osmisliti normalne i potpune requiremente. Recimo.
Nijedno od prijašnjih pravila više ne vrijedi.

Sada, prihvatljiv sadržaj je onaj koji:

sadrži par bilo koja dva slova koji se pojavljuje najmanje dvaput u stringu (znakovnome nizu) bez preklapanja, poput `xyxy` (`xy`) ili `aabcdefgaa` (`aa`), ali ne poput `aaa` (`aa`, ali se preklapa).

sadrži najmanje jedno slovo koje se ponavlja s točno jednim slovom između, poput `xyx`, `abcdefeghi` (`efe`) ili čak `aaa`.

Na primjer:

`qjhvhtzxzqqjkmpb` je prihvatljiv zato što ima par koji se pojavljuje dvaput (`qj`) i slovo koje se ponavlja s točno jednim slovom između (`zxz`).

`xxyxx` je prihvatljiv zato što ima par koji se pojavljuje dvaput i slovo koje se ponavlja s točno jednim slovom između, iako se slova preklapaju u različitim pravilima.

`uurcxstgmygtbstg` je zločest zato što ima par (`tg`) ali nema slovo koje se ponavlja s točno jednim slovom između.

`ieodomkazucvgmuy` je zločest zato što ima slovo koje se ponavlja s točno jednim slovom između (`odo`), ali nema par koji se pojavljuje dvaput.


Koliko stringova je prihvatljivo po novim pravilima?

##INSERT_SAMPLE_TEST_CASES_HERE##
