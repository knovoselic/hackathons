import sys

def parse_op(op):
    parts = op.split(" ")

    if parts[0] == "toggle":
        first = parts[1].split(",")
        second = parts[3].split(",")
        return int(first[0]), int(first[1]), int(second[0]), int(second[1]), 2

    if parts[1] == "on":
        first = parts[2].split(",")
        second = parts[4].split(",")
        return int(first[0]), int(first[1]), int(second[0]), int(second[1]), 1

    first = parts[2].split(",")
    second = parts[4].split(",")
    return int(first[0]), int(first[1]), int(second[0]), int(second[1]), -1

input_str = sys.stdin.read()
lines = input_str.split("\n")

n = 1001
mem = [[0]*n for i in range(n)]

for line in lines:
    op = parse_op(line)
    for i in range(op[0], op[2]+1):
        for j in range(op[1], op[3]+1):
            mem[i][j] += op[4]
            if mem[i][j] < 0:
                mem[i][j] = 0

print(sum([sum(row) for row in mem]))
